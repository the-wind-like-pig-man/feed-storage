//3_7_exercises.c---读取一个浮点数，然后以不同的形式打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	float input = 0;

	printf("Please enter a floating-point value: ");

	scanf("%f", &input);

	printf("fixed-point notation: %f \n", input);//打印普通形式，转换说明符用%f

	printf("exponential natation: %e \n", input);//打印指数形式，转换说明符用%e

	printf("p notation: %a \n", input);//打印p计数法形式，转换说明符用%a



	return 0;

}

//output:
//Please enter a floating - point value : 3.1415926
//fixed - point notation : 3.141593
//exponential natation : 3.141593e+00
//p notation : 0x1.921fb40000000p+1

//output1:
//Please enter a floating - point value : 64.25
//fixed - point notation : 64.250000
//exponential natation : 6.425000e+01
//p notation : 0x1.0100000000000p+6
