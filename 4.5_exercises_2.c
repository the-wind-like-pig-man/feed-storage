//4.5_exercises_2.c---用不同的格式打印名字

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	char name[40] = { 0 };

	int width = 0;

	printf("Please input your name: ");

	scanf("%s", name);

	width = printf("\"%s\"\n", name);//打印用户输入的名字，使用转义序列\"打印双引号，并通过printf()函数的返回值获取名字的字符长度

	width -= 4;//printf()函数的返回值为打印字符数，因此需要排除两个引号、一个换行符、一个句号，或者直接使用width = strlen(name);

	printf("\"%20s\".\n", name);//在宽度为20的字段右边打印名字，使用转义序列\"打印双引号

	printf("\"%-20s\".\n", name);

	printf("\"%*s\".", (width + 3), name);//使用修饰符*指定宽度参数，打印名字字符串




	return 0;

}
//
//output:
//Please input your name : C Primer Plus
//"C"
//"                   C".
//"C                   ".
//"  C".

//output:
//Please input your name : JhonSmith
//"JhonSmith"
//"           JhonSmith".
//"JhonSmith           ".
//"  JhonSmith".