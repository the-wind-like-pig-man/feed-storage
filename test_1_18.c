﻿#include <stdbool.h>

#include <math.h>//少了数学库函数，会打印偶数

#include <stdio.h>


//写一个函数，实现一个整形有序数组的二分查找
//
//int binary_search(int arr[], int k, int sz)
//{
//	int left = 0;
//
//	int right = sz - 1;
//	while (left <= right)
//	{
//
//		int mid = left + (right - left) / 2;
//
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//
//		}
//
//		else
//
//		{
//			return mid;//找到了，返回下标
//		}
//
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 ,10};
//
//	int k = 7;//找到了返回下标
//    
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//找不到，返回-1
//
//	int ret = binary_search(arr, k, sz);
//
//	
//	if (ret == -1)
//	{
//
//		printf("can't find it:\n");
//
//	}
//
//	else
//	{
//		printf("find it, the subscript is:%d\n", ret);
//	}
//
//
//	return 0;
//}

//形参和实参的名字可以相同，也可以不同

//数组传参实际上传递的是数组首元素的地址，而不是整个数组

//所以在一个函数内部计算一个函数参数部分的数组的元素个数是不靠谱的

//形参arr看上去是数组，本质是指针变量
//
//bool is_prime(int n)//打印出来的由偶数？？
//{
//	int j = 0;
//
//	for (j = 2; j <= sqrt(n); j++)
//	{
//		if (n % j == 0)
//		{
//			return false;
//		}
//	}
//
//	return true;
//}
//
//int main()
//{
//	int i = 0;
//
//	int count = 0;
//
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i))
//		{
//			printf("%d ", i);
//
//			count++;
//
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//
//	printf("%d\n", sizeof(bool));
//
//
//	return 0;
//}

//写一个函数，每调用一次这个函数，就会将num的值增加1
//
//void Add(int* p)
//{
//	(*p)++;
//}

int Add(int n)
{
	return ++n;
}


//int main()
//{
//	int num = 0;
//
//	Add(&num);
//
//	printf("%d\n", num);//num=1
//
//	Add(&num);
//
//	printf("%d\n", num);//num=2
//
//	Add(&num);
//
//	printf("%d\n", num);//num=3
//
//	Add(&num);
//
//	printf("%d\n", num);// num = 4
//
//		return 0;
//
//}
//

int main()
{
	int num = 0;

	num = Add(num);

	printf("%d\n", num);//num=1

	num = Add(num);

	printf("%d\n", num);//num=2

	num = Add(num);


	printf("%d\n", num);//num=3

    num = Add(num);


	printf("%d\n", num);// num = 4

	return 0;

}