//3_7_exercises_1.c---整数上溢，浮点数上溢、下溢

#include <stdio.h>

#include <float.h>

#include <limits.h>

int main()
{

	int big_int = 2147483647;//有符号整数最大值2的31次方减1

	float big_float = 3.4E38;//浮点数最大值

	float small_float = 10.0 / 3;//浮点数的有效数据为6位

	printf("The big int data is %d\n", big_int + 1);//整数最大值加1，越界

	printf("The big flaot data is %f\n", big_float * 10);//浮点数最大值乘10，越界

	printf("The small float data is %f\n", small_float);//打印3.333333精度损失

	printf("The MAX float data is %f\n", FLT_MAX);

	printf("The MAX int data is %d\n", INT_MAX);




	return 0;

}

//output:
//The big int data is - 2147483648
//The big flaot data is inf
//The small float data is 3.333333
//The MAX float data is 340282346638528859811704183484516925440.000000
//The MAX int data is 2147483647