//when.c---何时退出循环

#include <stdio.h>

int main()
{

	int n = 3;

	while (n < 7)//第7行
	{

		printf("n = %d\n", n);

		n++;//第10行

		printf("Now n = %d\n", n);//第11行

	}

	printf("The loop has finished.\n");



	return 0;

}

//output:---n=5
//n = 5
//Now n = 6
//n = 6
//Now n = 7
//The loop has finished.

//output:---n=3
//n = 3
//Now n = 4
//n = 4
//Now n = 5
//n = 5
//Now n = 6
//n = 6
//Now n = 7
//The loop has finished.