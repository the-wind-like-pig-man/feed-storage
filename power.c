//power.c---计算数的整数幂

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

double power(double n, int p);//ANSI函数原型

int main()
{

	double x = 0, xpow = 0;

	int exp = 0;

	printf("Please enter a number and the positive integer power");

	printf(" to which\nthe number be raised. Enter q");

	printf(" to quit.\n");

	while (scanf("%lf%d", &x, &exp) == 2)
	{

		xpow = power(x, exp);//函数调用

		printf("%.3g to the power %d is %.5g\n", x, exp, xpow);

		printf("Please enter the next pair of numbers or q to qiut.\n");

	}

	printf("Hope you nejoyed this power tirp --- goodbye!\n");


	return 0;

}

double power(double n, int p)
{

	double pow = 1;

	int i = 0;

	for (i = 1; i <= p; i++)
	{

		pow *= n;

	}

	return pow;//返回pow的值
}


//output：
//Enter a number and the positive integer power to which
//the number be raised.Enter q to quit.
//1.2 12
//1.2 to the power 12 is 8.9161
//Enter the next pair of numbers or q to qiut.
//2 16
//2 to the power 16 is 65536
//Enter the next pair of numbers or q to qiut.
//3 3
//3 to the power 3 is 27
//Enter the next pair of numbers or q to qiut.
//1 2
//1 to the power 2 is 1
//Enter the next pair of numbers or q to qiut.
//10 3
//10 to the power 3 is 1000
//Enter the next pair of numbers or q to qiut.
//q
//Hope you nejoyed this power tirp-- - goodbye!