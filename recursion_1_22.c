﻿#define  _CRT_SECURE_NO_WARNINGS 0

#include <string.h>

#include <stdio.h>

//接收一个整型值（无符号），按顺序打印它的每一位。
//例如：输入---1234，输出---1 2 3 4 ，

//%d是打印有符号的整数（会有正负数）
//%u是打印无符号的整数

//int main()
//{
//	unsigned int num = 0;
//
//	scanf("%u", &num);//1234
//
//	while (num)
//	{
//		printf("%d ", num % 10);
//
//		num = num / 10;
//
//	}
//
//	//1 2 3 4
//	printf("%u\n", num);
//	return 0;
//}

////递归的实现
//
//void print(unsigned int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);//123
//
//	}
//
//	printf("%d ", n % 10);//4
//
//}
//
//int main()
//{
//	unsigned int num = 0;
//
//	scanf("%u", &num);
//
//	print(num);
//
//
//	return 0;
//}

//print(1234)
//print(123) 4
//print(12) 3 4
//print(1) 2 3 4
//1 2 3 4 

//编写函数不允许创建临时变量，求字的符串长度
// 模拟实现strlen
//int my_strlen(char str[])//参数部分写成数组的形式
//
//int my_strlen(char* str)//参数部分写成指针形式，与上同意
//{
//	int count = 0;
//
//	while (*str != '\0')
//	{
//		count++;
//
//		str++;//找下一个字符
//	}
//
//	return count;
//
//}

//递归求解

//my_strlen("abc")
//1+my_strlen("bc")
//1+1+my_strlen("c")
//1+1+1+my_strlen("")
//1+1+1+0
//
//int my_strlen(char* str)
//{
//	if (*str != '\0')
//	
//		return 1 + my_strlen(str + 1);//++str虽可以得到结果，但str的值改变了。
//	//str++无法得到相应的结果
//
//	else
//
//		return 0;
//
//}
//
//int main()
//{
//	char arr[] = "abcdef";//[abcdef\0]
//	//char*
//
//	int len = my_strlen("abcefg");
//
//	printf("%d\n", len);
//	
//	return 0;
//}

//求n的阶乘
//int fac(int n)//递归实现
//{
//	if (n <= 1)
//
//		return 1;
//
//	else
//
//		return n * fac(n - 1);
//
//}

//迭代的方式实现---非递归
//int fac(int n)
//{
//	int i = 0;
//
//	int ret = 1;
//
//		for (i = 1; i <= n; i++)//for循环实现
//		{
//			ret *= i;
//		}
//
//	return ret;
//}
//
//int main()
//{
//	int n = 0;
//
//	scanf("%d", &n);
//
//	int ret = fac(n);
//
//	printf("ret = %d\n", ret);
//
//
//	return 0;
//}

//求第n个斐波那契数列(不考虑溢出)
//斐波那契数数列
//1 1 2 3 5 8 13 21 34 55...

//int count = 0;
//
//int Fib(int n)
//{
//	if (n == 3)
//		count++;
//
//	if (n <= 2)
//
//		return 1;
//
//	else
//
//		return Fib(n - 1) + Fib(n - 2);//递归实现时，当n较大时，计算速度会很慢，比如超过50
//
//}
//
////          40
////     39         38
////  38    37    37   36
////37 36 36 35 36 35 35 34...
//
//int main()
//{
//	int n = 0;
//
//	scanf("%d", &n);
//
//	int ret = Fib(n);
//
//	printf("%d\n", ret);
//
//	printf("%d\n", count);
//
//	return 0;
//}
//
//int count = 0;
//
//int Fib(int n)
//{
//	int a = 1;
//	
//	int b = 1;
//
//	int c = 0;
//
//
//
//	while (n >= 3)
//	{
//
//
//		c = a + b;
//
//		a = b;
//		
//		b = c;
//
//		n--;
//
//		count++;
//
//
//
//	}
//
//
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//
//	scanf("%d", &n);
//
//	int ret = Fib(n);
//
//	printf("%d\n", ret);
//
//	printf("%d\n", count);
//
//	return 0;
//}
//
//void test(int n)//Stack overflow---栈溢出
//{
//
//	if (n < 10000)
//
//		test(n + 1);
//
//}
//
//int main()
//{
//	test(1);
//
//	return 0;
//}