//exercises_6.c---定义一个变量，然后打印该变量、它的2倍和它的平方

#include <stdio.h>

int main()
{

	int toes = 10;//定义整形变量toes，并初始化为10

	printf("The variable toes = %d.\n", toes);

	printf("double toes = %d.\n", 2 * toes);//计算并打印toes的两倍，也可以先计算再打印
	//例如：int d_toes = 2*toes;
	//printf("double toes = %d.\n", d_toes);
	//但不要写成 toes= 2*toes；这样回修改toes的值，影响到后面的计算

	printf("toes' square = %d.\n", toes * toes);


	return 0;

}

//output:
//The variable toes = 10.
//double toes = 20.
//toes' square = 100.