﻿
#define _CRT_SECURE_NO_WARNINGS 0


#include "game.h"//把定义ROW,COL参数的头文件包含进来

void InitBoard(char board[ROW][COL], int row, int col)//此处不能用ROW,COL代替3，3
{
	int i = 0;

	int j = 0;

	for (i = 0; i < row; i++)//第i行，第j列初始化为空格
	{
		
		for (j = 0; j < col; j++)
		{
			
			board[i][j] = ' ';

		}

	}

}
//
////第一版本---行是变动的，而列是固定的
//void DisplayBoard(char board[ROW][COL], int row, int col)
//{
//
//	int i = 0;
//
//	for (i = 0; i < row; i++)
//	{
//		//先打印数据
//		printf(" %c | %c | %c \n",board[i][0],board[i][1],board[i][2]);//行是变动的，列是固定的
//
//		//后打印分割行信息
//
//		if(i < row - 1)//用if语句进行过滤
//
//		   printf("---|---|---\n");//若无if语句第三行的分割行信息也会打印出来
//
//	}
//
//}


//
void DisplayBoard(char board[ROW][COL], int row, int col)
{

	int i = 0;

	for (i = 0; i < row; i++)//外循环决定打印几行
	{
		//先打印数据
		//printf(" %c | %c | %c \n",board[i][0],board[i][1],board[i][2]);//行是变动的，列是固定的

		int j = 0;

		for (j = 0; j < col; j++)//内循环决定打印几列
		{
			
			printf(" %c ", board[i][j]);

			if (j < col - 1)//最后一列的竖杠不打印

				printf("|");

		}

		printf("\n");//不要忘记换行
		
		//后打印分割行信息

		if (i < row - 1)//用if语句进行过滤
		{
		
			//printf("---|---|---\n");//若无if语句第三行的分割行信息也会打印出来

			int j = 0;

			for (j = 0; j < col; j++)
			{
			
				printf("---");

				if (j < col - 1)

					printf("|");

			}
			printf("\n");

		}

	}

}

void PlayerMove(char board[ROW][COL], int row, int col)//玩家下棋
{

	int x = 0;

	int y = 0;

	printf("the Player moves:> ");

	while (1)
	{
		printf("please input the coordinate:>");

		scanf("%d %d", &x, &y);

		//合法坐标位置的判断
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{

			if (board[x - 1][y - 1] == ' ')//坐标的修改，下标0对应玩家坐标1
			{
				
				board[x - 1][y - 1] = '*';

				break;
			}

			else
			{

				printf("the coordinate has been placed,please choose other coordinate\n");

				//break;

			}

		}

		else
		{

			printf("wrong coordinate,please input again\n");

			//break;
		}
	}
}


void ComputerMove(char board[ROW][COL], int row, int col)//电脑下棋
{

	printf("the Computer moves:>\n");

	int x = 0;

	int y = 0;

	while (1)
	{

		x = rand() % row;//0~5---rand的范围特别大 0~32767，模了以后范围大大缩小

		y = rand() % col;//0~5

		//if (board[x][y] == ' ')
		if(board[x][y] == ' ')

		{

			board[x][y] = '#';

			break;

		}


	}

}

//每个棋盘满了 返回1
//   棋盘不满 返回0
int IsFull(char board[ROW][COL], int row, int col)
{

	int i = 0;

	int j = 0;

	for (i = 0; i < row; i++)
	{

		for (j = 0; j < col; j++)
		{

			if (board[i][j] == ' ')
			{

				return 0;
			
			}

		}

	}

	return 1;

}

char IsWin(char board[ROW][COL], int row, int col)//IsWin()函数的实现
{
	//判断行是否都是*/#/空格
	int i = 0;

	for (i = 0; i < row; i++)
	{

		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][1] != ' ')
		{

			return board[i][1];//返回行其中一个元素

		}

	}
	int j = 0;
	//判断列是否都是*/#/空格
	for (j = 0; j < col; j++)
	{

		if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[1][j] != ' ')
		{

			return board[1][j];//返回列其中一个元素

		}

	}

	//判断对角线是否都是*/#/空格
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
	{

		return board[1][1];

	}

	if (board[2][0] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{

		return board[1][1];

	}

	//没有赢家，判断平局

	if (IsFull(board, row, col))
	{

		return 'Q';

	}

	//游戏继续
	return 'C';

}