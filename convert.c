//convert.c---自动类型转换

#include <stdio.h>

int main()
{

	char ch;

	int i;

	float f1;

	f1 = i = ch = 'C';//第九行

	printf("ch = %c, i = %d, f1 = %2.2f\n", ch, i, f1);//第十行

	ch = ch + 1;//第十一行

	i = f1 + 2 * ch;//第十二行

	f1 = 2.0 * ch + i;//第十三行

	printf("ch = %c, i = %d, f1 = %2.2f\n", ch, i, f1);//第十四行

	ch = 1107;//第十五行

	printf("Now ch = %c\n", ch);//第十六行

	ch = 80.89;//第十七行

	printf("Now ch = %c\n", ch);//第十八行


	return 0;

}

//output:
//ch = C, i = 67, f1 = 67.00
//ch = D, i = 203, f1 = 339.00
//Now ch = S
//Now ch = P