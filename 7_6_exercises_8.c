//exercises_8.c---给出一个可供选择的工资菜单，使用switch语句完成工资等级选择

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define EXTRA_HOUR 1.5

#define BASE_TAX 0.15

#define EXTRA_TAX 0.2

#define EXCEED_TAX 0.25

void show_menu(void);//显示基本工资的菜单函数

float get_hours(void);//读取用户输入工作时长的函数

void calc_salary(float base_salary, float hours);
//依据基本工资和工作时长计算工资、税金、净收入的函数

int main()
{

	float hours = 0; 
	
	char selected = 0;

	do
	{

		show_menu();

		//scanf("%c", &selected);

		//scanf("%c", &selected);

		selected = getchar();

		switch (selected)
		{

		case '1':

			printf("Hello, you select $8.75 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(8.75, hours);

			break;
			//选择基本工资，读取用户输入的工作时长，计算工资、税金及净收入

		case '2':

			printf("Hello, you select $9.33 / hr. Enter thre work hours: ");

			scanf("%f", &hours);

			calc_salary(9.33, hours);

			break;

		case '3':

			printf("Hello, you select #10.00 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(10.00, hours);

			break;

		case '4':

			printf("Hello, you select $11.2 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(11.2, hours);

			break;

		case '5':

			break;

		default:

			printf("Error selected! Please retry!\n");

			//getchar();

			break;

		}

	} while (selected != '5');

	printf("Done!\n");


	return 0;

}


void show_menu()
{

	//显示提取菜单

	char s1[] = "1) $8.75 / hr";

	char s2[] = "2) $9.33 / hr";

	char s3[] = "3) $10.00 / hr";

	char s4[] = "4) $11.20 / hr";

	char s5[] = "5) quit";

	printf("*********************************************************\n");

	printf("Enter the number corresponding to the desired pay rate or action\n");

	printf("%-40s", s1);

	printf("%-40s\n", s2);

	printf("%-40s", s3);

	printf("%-40s\n", s4);

	printf("%-40s\n", s5);

	printf("***********************************************************\n");
}

//void cacl_salary(float base_salary, float hours);

void calc_salary(float base_salary, float hours)

{

	float salary = 0, tax = 0, taxed_salary = 0;

	if (hours <= 30)
	{

		//工作时长小于30小时的情况

		salary = hours * base_salary;

		tax = salary * BASE_TAX;

		taxed_salary = salary - tax;

	}

	else if (hours <= 40)
	{
		//工作时长大于40小时的情况

		salary = hours * base_salary;

		tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		taxed_salary = salary - tax;

	}

	else
	{
		//其他工作时长条件下的税收计算

		salary = (40 + (hours - 40) * EXTRA_HOUR) * base_salary;

		if (salary <= 450)
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		}

		else
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX + (salary - 450) * EXCEED_TAX;

			taxed_salary = salary - tax;

		}


	}

	printf("Your salary before tax is %.2f, tax is %.2f, salary after tax is %.2f\n.",
		salary, tax, taxed_salary);

	printf("\ncountinue...\n");

}

//output:
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//1
//Hello, you select $8.75 / hr.Enter the work hours : 30
//Your salary before tax is 262.50, tax is 39.38, salary after tax is 223.12
//.
//countinue...
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//Error selected!Please retry!
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//2
//Hello, you select $9.33 / hr.Enter thre work hours : 30
//Your salary before tax is 279.90, tax is 41.99, salary after tax is 237.91
//.
//countinue...
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//Error selected!Please retry!
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//3
//Hello, you select #10.00 / hr.Enter the work hours : 40
//Your salary before tax is 400.00, tax is 65.00, salary after tax is 335.00
//.
//countinue...
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//Error selected!Please retry!
//*********************************************************
//Enter the number corresponding to the desired pay rate or action
//1) $8.75 / hr                           2) $9.33 / hr
//3) $10.00 / hr                          4) $11.20 / hr
//5) quit
//***********************************************************
//5
//Done!