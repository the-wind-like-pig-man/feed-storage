//for_cube.c---使用for循环创建一个立方表

#include <stdio.h>

int main()
{

	int num = 0;

	printf("      n     n cubed\n");

	for (num = 1; num <= 9; num++)
	{

		printf("%7d %7d\n", num, num * num * num);

	}


	return 0;

}
//
//output:
//n     n cubed
//1       1
//2       8
//3      27
//4      64
//5     125
//6     216
//7     343
//8     512
//9     729
