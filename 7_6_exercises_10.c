//exerises_10.c---1998年美联邦税收方案，编程实现

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define SINGLE 17850

#define HOLDER 23900

#define MARRY 29750

#define DIVORCE 14875

#define BASE_TAX 0.15

#define EXTRA_TAX 0.28
//定义相关的常量数据

int main()
{

	char type = 0;

	float salary = 0;

	float tax = 0, salary_taxed = 0;

	do {
	
		printf("Please select tax type. There are for type: \n");

		printf("1)Single    2)House Holder    3)Married    4)Divorced    5)Quit:\n");

		scanf("%c", &type);//选择纳税类型，switch语句对不同类型分别计算

		switch (type)
		{

		case '1'://signle类型

			printf("Please enter your salary: ");

			scanf("%f", &salary);

			if (salary <= SINGLE)
			{

				tax = salary * BASE_TAX;

				salary_taxed = salary - tax;

			}

			else
			{

				tax = salary * BASE_TAX + (salary - SINGLE) * EXTRA_TAX;

				salary_taxed = salary - tax;

			}

			printf("Hi, your salary is %.2f, tax is %.2f, after tax salary is %.2f\b",
				salary, tax, salary_taxed);

			break;

		case '2'://house holder类型

			printf("Please enter your salary: ");

			scanf("%f", &salary);

			if (salary <= HOLDER)
			{

				tax = salary * BASE_TAX;

				salary_taxed = salary - tax;

			}

			else
			{

				tax = salary * BASE_TAX + (salary - HOLDER) * EXTRA_TAX;

				salary_taxed = salary - tax;

			}

			printf("Hi, your salary is %.2f, tax is %.2f, salary after tax is %.2f\t",
				salary, tax, salary_taxed);

			break;

		case '3'://MARRY类型

			printf("Please enter your salary: ");

			scanf("%f", &salary);

			if (salary <= MARRY)
			{

				tax = salary * BASE_TAX;

				salary_taxed = salary - tax;

			}

			else
			{

				tax = salary * BASE_TAX + (salary - MARRY) * EXTRA_TAX;

				salary_taxed = salary - tax;

			}

			printf("Hi, your salary is %.2f, tax is %.2f, salary after tax is %.2f\n",
				salary, tax, salary_taxed);

			break;

		case '4'://DIVORCE类型

			printf("Please enter your salary: ");

			scanf("%f", &salary);

			if (salary <= DIVORCE)
			{

				tax = salary * BASE_TAX;

				salary_taxed = salary - tax;

			}

			else
			{

				tax = salary * BASE_TAX + (salary - DIVORCE) * EXTRA_TAX;

				salary_taxed = salary - tax;

			}

			printf("Hi, your salary is %.2f, tax is %.2f, salary after tax is %.2f\n",
				salary, tax, salary_taxed);

			break;

		case '5':

			printf("Quit!\n");

			break;

		default:

			printf("Wrong type.Please retry.\n");


		}
	
	} while (type != '5');

	printf("Well Done!\n");


	return 0;

}

//output:
//Please select tax type.There are for type :
//	1)Single    2)House Holder    3)Married    4)Divorced    5)Quit: 1
//	Please enter your salary : 27850
//	Hi, your salary is 27850.00, tax is 6977.50, after tax salary is 20872.5Please select tax type.There are for type :
//	1)Single    2)House Holder    3)Married    4)Divorced    5)Quit: Wrong type.Please retry.
//	Please select tax type.There are for type :
//	1)Single    2)House Holder    3)Married    4)Divorced    5)Quit:
