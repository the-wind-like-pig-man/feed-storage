//t_and_f.c---C中的真和假的值

#include <stdio.h>

int main()
{

	int true_val, false_val;

	true_val = (10 > 2);//关系为真的值---(表达式)---求表达式的值

	false_val = (10 == 2);//关系为假的值

	printf("true_val = %d; false_val = %d\n", true_val, false_val);


	return 0;

}

//output:
//true_val = 1; false_val = 0