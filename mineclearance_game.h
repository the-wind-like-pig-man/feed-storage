﻿#pragma once

#include <stdio.h>

#include <time.h>

#include <stdlib.h>

#define ROW 9

#define COL 9

#define ROWS ROW + 2

#define COLS COL + 2

#define EASY_COUNT 10 //简单雷---10个

void InitBoard(char board[ROWS][COLS],int rows,int cols,char set);

void DisplayBoard(char board[ROWS][COLS],int row,int col);//此处不是ROW,COL,因数组仍然是11*11的

//只是打印的是9*9,即只用到9*9大小的数组

void SetMine(char board[ROWS][COLS],int row,int col);
