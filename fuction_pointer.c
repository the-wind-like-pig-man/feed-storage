﻿
#define  _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>


//函数指针的用途

//实现一个计算器：加法、减法、乘法、除法

//void menu()
//{
//
//
//	printf("*********************************\n");
//
//
//	printf("******  1.Add      2.Sub   ******\n");
//
//
//	printf("******  3.Mul      4.Div   ******\n");
//
//
//	printf("******        0.Exit       ******\n");
//
//
//	printf("*********************************\n");
//
//
//}
//
//int Add(int x, int y)
//{
//
//	return x + y;
//
//}
//
//int Sub(int x, int y)
//{
//
//	return x - y;
//
//}
//
//int Mul(int x, int y)
//{
//
//	return x * y;
//
//}
//
//int Div(int x, int y)
//{
//
//	return x / y;
//
//}
//
//
//int main()
//{
//	int input = 0;
//
//	int x = 0;
//
//	int y = 0;
//
//	int ret = 0;
//
//	do
//	{
//
//		menu();
//
//		printf("Please choose:>");
//
//		scanf("%d", &input);
//
//		//printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//		//scanf("%d %d", &x, &y);
//
//		switch (input)
//		{
//
//		case 1:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Add(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 2:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Sub(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 3:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Mul(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 4:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Div(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 0:
//
//			printf("Exit!\n");
//
//			break;
//
//		default:
//
//			printf("Choose wrong!\n");
//
//			break;
//
//
//		}
//
//
//	} while (input);
//
//
//	return 0;
//
//}

void menu()
{


	printf("*********************************\n");


	printf("******  1.Add      2.Sub   ******\n");


	printf("******  3.Mul      4.Div   ******\n");


	printf("******        0.Exit       ******\n");


	printf("*********************************\n");


}

int Add(int x, int y)
{

	return x + y;

}

int Sub(int x, int y)
{

	return x - y;

}

int Mul(int x, int y)
{

	return x * y;

}

int Div(int x, int y)
{

	return x / y;

}

void calc(int(*pf)(int x, int y))//封装calc---计算器函数
{

	int x = 0;

	int y = 0;

	int ret = 0;

	printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

	scanf("%d %d", &x, &y);

	ret = pf(x, y);//函数体部分冗余

	printf("%d\n", ret);

}

int main()
{
	int input = 0;

	

	do
	{

		menu();

		printf("Please choose:>");

		scanf("%d", &input);

		//printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

		//scanf("%d %d", &x, &y);

		switch (input)
		{

		case 1:

			calc(Add);

			break;

		case 2:

			calc(Sub);

			break;

		case 3:

			calc(Mul);

			break;

		case 4:

			calc(Div);

			break;

		case 0:

			printf("Exit!\n");

			break;

		default:

			printf("Choose wrong!\n");

			break;


		}


	} while (input);


	return 0;

}