//12_rain_fuction.c---重新写程序清单10.7中的rain.c，把main中的主要任务都使用函数来完成

#include <stdio.h>

#define MONTHS 12//一年的月份数

#define YEARS 5//年数


//void year_average(int years, int months, const float t[years][months]);
//
//void month_average(int years, int months, const float t[years][months]);
////函数使用变长数组作为参数

void year_average(int years, const float t[][MONTHS]);

void month_average(int years, const float t[][MONTHS]);
//函数使用传统数组作为参数

int main()
{

	const float rain[YEARS][MONTHS] = {

		{4.3, 4.3, 4.3, 3.0, 2.0, 1.2, 0.2, 0.2, 0.4, 2.4, 3.5, 6.6},

	{8.5, 8.2, 1.2, 1.6, 2.4, 0.0, 5.2, 0.9, 0.3, 0.9, 1.4, 7.3},

		{9.1, 8.5, 6.7, 4.3, 2.1, 0.8, 0.2, 0.2, 1.1, 2.3, 6.1, 8.4},

		{7.2, 9.9, 8.4, 3.3, 1.2, 0.8, 0.4, 0.0, 0.6, 1.7, 4.3, 6.2},

		{7.6, 5.6, 3.8, 2.8, 3.8, 0.2, 0.0, 0.0, 0.0, 1.3, 2.6, 5.2},
	
	};

	//year_average(YEARS, MONTHS, rain);

	//month_average(YEARS, MONTHS, rain);
	////函数调用

	year_average(YEARS, rain);

	month_average(YEARS, rain);
	//函数调用
	printf("\n");



	return 0;

}

//void year_average(int years, int months, const float t[years][months])
//{
//
//	float subtot = 0, total = 0;
//
//	int month, year;
//
//	printf(" YEAR    RAINFALL.  (inches)\n");
//
//	for (year = 0, total = 0; year < years; year++)
//	{
//
//		for (month = 0, subtot = 0; month < months; month++)
//		{
//
//			subtot += t[year][month];
//
//			printf("%5d %15.1f\n", 2010 + year, subtot);
//
//
//		}
//
//		total += subtot;
//
//	}
//
//	printf("\nThe yearly average is %.1f inches.\n\n", total / YEARS);
//	//计算年平均降水量，使用内层循环计算每年的平均降水量，使用外层循环计算五年的平均降水量
//
//}

void year_average(int years,const float t[][MONTHS])
{

	float subtot = 0, total = 0;

	int month, year;

	printf(" YEAR    RAINFALL.  (inches)\n");

	for (year = 0, total = 0; year < YEARS; year++)
	{

		for (month = 0, subtot = 0; month < MONTHS; month++)
		{

			subtot += t[year][month];

			//printf("%5d %15.1f\n", 2010 + year, subtot);

		}

		printf("%5d %15.1f\n", 2010 + year, subtot);

		total += subtot;

	}

	printf("\nThe yearly average is %.1f inches.\n\n", total / YEARS);

	//计算年平均降水量，使用内层循环计算每年的平均降水量，使用外层循环计算五年的平均降水量

}

//void month_average(int years, int months, const float t[years][months])
//{
//
//	float subtot = 0;
//
//	int month = 0, year = 0;
//
//	printf("MONTHLY AVERAGE:\n\n");
//
//	printf(" Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct ");
//
//	printf("Nov  Dec\n");
//
//	for (month = 0; month < months; month++)
//	{
//
//		for (year = 0; subtot = 0; year < years; year++)
//		{
//
//			subtot += t[year][month];
//
//			printf("%4.1f ", subtot / years);
//
//		}
//
//	}
//
//}

void month_average(int years, const float t[][MONTHS])
{

	float subtot = 0;

	int month = 0, year = 0;

	printf("MONTHLY AVERAGE:\n\n");

	printf(" Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct ");

	printf("Nov  Dec\n");

	for (month = 0; month < MONTHS; month++)
	{

		for (year = 0, subtot = 0; year < YEARS; year++)
		{

			subtot += t[year][month];

			//printf("%4.1f ", subtot / years);

		}

		printf("%4.1f ", subtot / years);

	}

	//printf("%4.1f ", subtot / years);

}

//
//计算月平均降水量，对于二维数组，外层循环控制月份值的变换，内层循环控制年份变化（主数组）
//和年均降水量的循环嵌套略有不同

//output:
//2013            44.0
//2014             7.6
//2014            13.2
//2014            17.0
//2014            19.8
//2014            23.6
//2014            23.8
//2014            23.8
//2014            23.8
//2014            23.8
//2014            25.1
//2014            27.7
//2014            32.9
//
//The yearly average is 39.4 inches.
//
//MONTHLY AVERAGE :
//
//Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct Nov  Dec
//0.9  2.6  4.4  5.8  7.3  0.9  2.5  4.2  6.2  7.3  0.9  1.1  2.4  4.1  4.9  0.6  0.9  1.8  2.4  3.0  0.4  0.9  1.3  1.5  2.3  0.2  0.2  0.4  0.6  0.6  0.0  1.1  1.1  1.2  1.2  0.0  0.2  0.3  0.3  0.3  0.1  0.1  0.4  0.5  0.5  0.5  0.7  1.1  1.5  1.7  0.7  1.0  2.2  3.1  3.6  1.3  2.8  4.5  5.7  6.7
