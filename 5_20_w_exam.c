﻿
#include <string.h>

#include <stdio.h>

//int main()
//{
//
//	//char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
//
//	char* p = "abcdef";
//
//	//[a b c d e f \0]
//
//	printf("%d\n", strlen(p));//---6
//
//	printf("%d\n", strlen(p + 1));//---5
//
//	//printf("%d\n", srelen(*p));//---error
//
//
//	//printf("%d\n", strlen(p[0]));//---error
//
//
//	printf("%d\n", strlen(&p));//---输出6? 不应该是随机值吗？  
//
//	printf("%d\n", strlen(&p + 1));//---随机值 30
//
//	printf("%d\n", strlen(&p[0] + 1));//---5
//
//
//	return 0;
//
//
//}


//int main()
//{
//
//	int a[3][4] = { 0 };
//
//	printf("%d\n", sizeof(a));
//
//	printf("%d\n", sizeof(a[0][0]));
//	
//	printf("%d\n", sizeof(a[0]));
//
//	//a[0]是第一行这个一维数组的数组名，单独放在sizeof内部，a[0]表示第一个整个这个一维数组
//	//sizeof(a[0])计算的就是第一行的大小
//	
//	printf("%d\n", sizeof(a[0] + 1));
//	//a[0]并没有单独放在sizeof内部，也没有取地址，a[0]就表示首元素的地址
//	//就是第一行这个一维数组的第一个元素的地址，a[0]+1 就是第一行第二个元素的地址 
//	
//	printf("%d\n", sizeof(*(a[0] + 1)));
//
//	//a[0]+1就是第一行第二个元素的地址 
//	//*(a[0]+1)就是第一行第二个元素
//
//	printf("%d\n", sizeof(a + 1));//4/8
//	//a虽然是二维数组的地址，但是并没有单独放在sizeof内部，也没有取地址
//	//a表示首元素的地址，二维数组的首元素是它的第一行，a是第一行的地址   
//	//a+1就是跳过第一行，表示第二行的地址
//
//	printf("%d\n", sizeof(*(a + 1)));//16
//	//*(a+1)是对第二行地址解引用，拿到的是第二行
//	//*(a+1)--->a[1] 
//	//sizeof(*(a+1))--->sizeof(a[1])
//
//	printf("%d\n", sizeof(&a[0] + 1));//4/8
//	//&a[0]对第一行的数组名取地址，拿出的是第一行的地址
//	//&a[0]+1得到的是第二行的地址
//	//
//
//	printf("%d\n", sizeof(*(&a[0] + 1)));//16
//
//	printf("%d\n", sizeof(*a));//16
//	//a表示首元素的地址，就是第一行的地址
//	//*a就是对第一行地址的解引用，拿到的就是第一行
//
//	printf("%d\n", sizeof(a[3]));//16---未越界，不会真的访问第四行，
//
//	printf("%d\n", sizeof(a[0]));//16---未越界
//
//	//int a = 10;
//	//sizeof(int)
//	//sizeof(a)
//
//
//	return 0;
//
//}

//int main()
//{
//
//	int a[5] = { 1, 2, 3, 4, 5 };
//
//	int* ptr = (int*)(&a + 1);
// //ptr取决于它自身的类型，不取决于它接受的数据类型
//
//	printf("%d %d", *(a + 1), *(ptr - 1));//2  5
//
//
//	return 0;
//
//}

//struct Test
//{
//
//	int Num;
//
//	char* pcName;
//
//	short sDate;
//
//	char cha[2];
//
//	short sBa[4];
//
//
//
//}*p = (struct Test*)0x100000;//强制类型转换为地址
//
////假设p的值为0x1000000,如下表达式的值分别是多少？
////已知，结构体Test类型表变量大小是20个字节
////x86环境下才可以运行
//
//int main()
//{
//
//	printf("%p\n", p + 0x1);//结构体指针加1跳过一个结构体大小(20个字节)
//	//0x100000 + 20 --->0x100014
//
//	printf("%p\n", (unsigned long)p + 0x1);//强制转换为无符号长整型
//	//1,048,576+1 --->1,048,577
//	//0x100001
//
//	printf("%p\n", (unsigned int*)p + 0x1);//强制类型转换为整型指针，加1等于加4个字节
//	//0x100000+4 --->0x100004
//
//	//输出：
//	//00100014
//	//00100001
//    //00100004
//
//
//	return 0;
//
//}


int main()
{
	//a=0x0012ff40
	//a+1--->0x0012ff44
	//(int)a+1--->0x0012ff41
	int a[4] = { 1, 2, 3, 4 };

	int* ptr1 = (int*)(&a + 1);

	int* ptr2 = (int*)((int)a + 1);

	printf("%x,%x", ptr1[-1], *ptr2);


	return 0;

}
