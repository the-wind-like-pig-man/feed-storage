//proto.c---使用函数原型

#include <stdio.h>

int imax(int, int);//函数原型

int main()
{

	printf("The maxninum of %d and %d is %d.\n",
		3, 5, imax(3,5));

	printf("The maxinum of %d and %d is %d.\n",
		3, 5, imax(3, 5));



	return 0;

}


int imax(int n, int m)
{

	return (n > m ? n : m);

}

//output:
//The maxninum of 3 and 5 is 5.
//The maxinum of 3 and 5 is 5.