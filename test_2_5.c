﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include "mineclearance_game.h"

void menu()
{

	printf("*************************\n");

	printf("******* 1. play *********\n");

	printf("******* 0. exit *********\n");

	printf("*************************\n");


}

void game()
{

	char mine[ROWS][COLS] = { 0 };//此数组用来存放布置好的雷的信息，11*11

	char show[ROWS][COLS] = { 0 };//此数组用来存放排查出的雷的信息，11*11
	//初始化数组的内容为指定的内容
	//mine数组在没有布置雷的时候都是'0'
	
	InitBoard(mine, ROWS, COLS,'0');

	//show数组在没有排查雷的时候，都是'*'

	InitBoard(show, ROWS, COLS,'*');

	//DisplayBoard(mine, ROW, COL);

	//DisplayBoard(show, ROW, COL);//此处不是ROW,COL,因数组仍然是11*11的

//只是打印的是9*9

	//设置雷

	SetMine(mine,ROW,COL);

	DisplayBoard(show, ROW, COL);

	//DisplayBoard(mine, ROW, COL);//要想打印设置雷，需要将此函数放到SetMine()函数后面

	//排查雷


}

int main()
{
	
	int input = 0;

	//设置随机数的生成起点

	srand((unsigned int)time(NULL));

	do
	{

		menu();

		//printf("please choose:>");//没有换行符，输出很惊喜

		printf("please choose:>");//可以没有换行符


		scanf("%d", &input);

		switch (input)
		{

		case 1:

			printf("mine clearance\n");
			
			game();//少了此句无法调用game()函数

			break;

		case 0:

			printf("exit the game\n");

			break;

		default:

			printf("choose wrong\n");

			break;

		}

	} while (input);


	return 0;

}