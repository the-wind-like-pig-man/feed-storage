//exercises_8.c---编写一个程序，显示一个提供加法、减法、乘法、除法的菜单

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void show_menu(void);//显示菜单输出

float get_number(void);//读取用户输入的运算数据

int main()
{

	char operate;

	float first = 0, second = 0;

	do {
	
		show_menu();

		operate = getchar();

		while (getchar() != '\n')
		{

			continue;//operate变量保存用户选择的菜单项，并通过while循环清除非字符输入

		}

		switch (operate)
		{

		case 'a':

			printf("Please enter the first number: ");

			first = get_number();

			printf("Please enter the second number: ");

			second = get_number();

			printf("%g + %g = %g\n", first, second, first + second);

			break;

		case 's':

			printf("Please enter the first number: ");

			first = get_number();

			printf("Please enter the second number: ");

			second = get_number();

			printf("%g - %g = %g\n", first, second, first + second);

			break;

		case 'm':

			printf("Please enter the first number: ");

			first = get_number();

			printf("Please enter the second number: ");

			second = get_number();

			printf("%g * %g = %g\n", first, second, first * second);

			break;

		case 'd':

			printf("Please enter the first number: ");

			first = get_number();

			printf("Please enter the second number: ");

			second = get_number();

			printf("Please enter the second number: ");

			while ((second = get_number()) == 0)//二次输入second---bug
			{

				printf("Please enter a number other than 0: ");

			}//判断除法操作数，对错误数据给出错误提示，直到获得正确的输入数据


			printf("%g / %g = %g\n", first, second, first / second);

			break;

		case 'q':

			break;

		default:

			printf("Please enter a char, such as a, s, m, d, and q: \n");

			while (getchar() != '\n');

			break;

		}

		while (getchar() != '\n');

	
	} while (operate != 'q');

	printf("Bye!\n");

	return 0;

}

void show_menu()
{

	printf("Please enter the operation of your choice: \n");

	printf("a. add                        s. substract \n");

	printf("m. multiply                   d. divide\n");

	printf("q.quit\n");

}

float get_number()
{

	float f = 0;

	char c = 0;

	while (scanf("%g", &f) != 1)
	{

		while ((c = getchar()) != '\n')
		{

			putchar(c);

		}

		printf(" is not a number.\n");

		printf("Please enter a number, such as 2.5, -1, 78e8, or 3: ");
	}//判断输入数据的格式，对错误数据给出错误提示，直到获得正确的输入格式

	return f;

}

//output:
//m.multiply                   d.divide
//q.quit
//a
//Please enter the first number : 22.4
//Please enter the second number : one
//one is not a number.
//Please enter a number, such as 2.5, -1, 78e8, or 3 : 2
//22.4 + 2 = 24.4
//Please enter the operation of your choice :
//a.add                        s.substract
//m.multiply                   d.divide
//q.quit
//d
//Please enter the first number : 18.4
//Please enter the second number : 0
//Please enter the second number : 0
//Please enter a number other than 0 : 0.2
//18.4 / 0.2 = 92
//Please enter the operation of your choice :
//a.add                        s.substract
//m.multiply                   d.divide
//q.quit
//q
//
//Bye!
