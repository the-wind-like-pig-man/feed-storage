//把分钟表示的时间换成用小时和分钟表示的时间

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define MIN_PER_HOU 60//每小时有60分钟

int main(int argc, char* argv[])
{

	int hours = 0, minutes = 0, input = 0;

	printf(" CONVERT MINUTES TO HOURS!\n");

	printf("PLEASE INPUT THE NUMBER OF MINUTES ( <= 0 TO QIUT): ");

	scanf("%d", &input);

	while (input > 0)
	{

		hours = input / MIN_PER_HOU;

		minutes = input % MIN_PER_HOU;

		//printf("CONVERT TO %d HOURS AND %d MINUTES.\N", hours, minutes);

		printf("CONVERT TO %d HOURS AND %d MINUTES.\n", hours, minutes);


		printf("PLEASE CONTINUE INPUT NUMBER OF MINUTES ( <= 0 TO QUIT): ");

		scanf("%d", &input);

	}

	printf("Program exit!\n");



	return 0;

}

//output:
//CONVERT MINUTES TO HOURS!
//PLEASE INPUT THE NUMBER OF MINUTES(<= 0 TO QIUT) : 100
//CONVERT TO 1 HOURS AND 40 MINUTES.
//PLEASE CONTINUE INPUT NUMBER OF MINUTES(<= 0 TO QUIT) : 120
//CONVERT TO 2 HOURS AND 0 MINUTES.
//PLEASE CONTINUE INPUT NUMBER OF MINUTES(<= 0 TO QUIT) : 210
//CONVERT TO 3 HOURS AND 30 MINUTES.
//PLEASE CONTINUE INPUT NUMBER OF MINUTES(<= 0 TO QUIT) : 150
//CONVERT TO 2 HOURS AND 30 MINUTES.
//PLEASE CONTINUE INPUT NUMBER OF MINUTES(<= 0 TO QUIT) : 0
//Program exit!
