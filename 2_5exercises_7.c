//exercises_7.c---打印smile

int smile(void);

#include <stdio.h>

//int main()
//{
//
//	smile(); smile(); smile();
//
//	printf("\n");//打印第一行，3个“Smile!”
//
//	smile(); smile();
//
//	printf("\n");//打印第二行，2个“Smile!”
//
//	smile();
//
//
//
//	return 0;
//
//}
//

int main()
{

	int i = 0;
	
	int j = 0;

	int k = 0;

	while (i < 7)
	{

		smile();

		i++;

	}

	printf("\n");

	while (j < 5)
	{

		smile();

		j++;

	}

	printf("\n");

	while (k < 3)
	{

		smile();

		k++;

	}

	printf("\nDone!");


	return 0;

}


int smile()
{

	printf("Smile!");

	return 0;

}

//output:
//Smile!Smile!Smile!
//Smile!Smile!
//Smile!

//output1:
//Smile!Smile!Smile!Smile!Smile!Smile!Smile!
//Smile!Smile!Smile!Smile!Smile!
//Smile!Smile!Smile!
//Done!