//2_5_exercixes_1.c---根据要求打印名和姓

#include <stdio.h>

#define NAME "Gustav"

#define SURNAME "Mahler"

int main()
{

	printf("%s %s\n", NAME, SURNAME);

	printf("%s\n%s\n", NAME, SURNAME);

	printf("%s ", NAME);

	printf("%s\n", SURNAME);


	return 0;

}

//output:
//Gustav Mahler
//Gustav
//Mahler
//GustavMahler

//output1:
//Gustav Mahler
//Gustav
//Mahler
//Gustav Mahler