#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int length = 0;

	double sum = 0.0;

	printf("Enter the limit length: ");

	scanf("%d", &length);

	while (length > 0)
	{

		sum = 0.0;

		for (int i = 1; i <= length; i++)
		{

			sum = sum + 1.0 / i;

		}

		printf("The sum for 1.0 + ... + 1.0 / %d is %lf\n", length, sum);

		sum = 0.0;

		for (int i = 1; i <= length; i++)
		{

			if (i % 2 == 0)
			{

				sum = sum - 1.0 / i;

			}

			else

				sum = sum + 1.0 / i;

		}

		printf("The sum for 1.0 - ... +1.0 / %d is %lf\n", length, sum);

		sum = 0.0;

		for (int i = 1; i <= length; i++)
		{

			if (i % 2 != 0)
			{

				sum = sum + (2 * 1.0) / i;

			}


		}

		printf("The sum for 1.0 + 1.0 + 2.0 / 3.0 +... + 2.0 / %d.0 is %lf\n", length, sum);

		printf("Enter the limit length: ");

		//scanf("%d", length);//---此句无法正常读取长度值

		scanf("%d", &length);


	}

	printf("\nDone!\n");


	return 0;

}

//output:
//Enter the limit length : 20
//The sum for 1.0 + ... + 1.0 / 20 is 3.597740
//The sum for 1.0 - ... + 1.0 / 20 is 0.668771
//The sum for 1.0 + 1.0 + 2.0 / 3.0 + ... + 2.0 / 20.0 is 4.266511
//Enter the limit length : 30
//The sum for 1.0 + ... + 1.0 / 30 is 3.994987
//The sum for 1.0 - ... + 1.0 / 30 is 0.676758
//The sum for 1.0 + 1.0 + 2.0 / 3.0 + ... + 2.0 / 30.0 is 4.671745
//Enter the limit length : 100
//The sum for 1.0 + ... + 1.0 / 100 is 5.187378
//The sum for 1.0 - ... + 1.0 / 100 is 0.688172
//The sum for 1.0 + 1.0 + 2.0 / 3.0 + ... + 2.0 / 100.0 is 5.875550
//Enter the limit length : 1000
//The sum for 1.0 + ... + 1.0 / 1000 is 7.485471
//The sum for 1.0 - ... + 1.0 / 1000 is 0.692647
//The sum for 1.0 + 1.0 + 2.0 / 3.0 + ... + 2.0 / 1000.0 is 8.178118
//Enter the limit length : 10000
//The sum for 1.0 + ... + 1.0 / 10000 is 9.787606
//The sum for 1.0 - ... + 1.0 / 10000 is 0.693097
//The sum for 1.0 + 1.0 + 2.0 / 3.0 + ... + 2.0 / 10000.0 is 10.480703
//Enter the limit length : 0
//
//Done!