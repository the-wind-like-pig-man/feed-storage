﻿

#include <stdio.h>

#include <string.h>


int main()
{

	char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };

	//printf("%d\n", strlen(arr));//输出随机值--->42  0xC0000005: Access violation reading location 0x0000000000000061.

	//printf("%d\n", strlen(arr + 0));//输出随机值---42 0xC0000005: Access violation reading location 0x0000000000000061.

	//printf("%d\n", strlen(*arr));//--->strlen('a'); --->strlen('97');//野指针--- : 0xC0000005: Access violation reading location 0x0000000000000062.


	//printf("%d\n", strlen(arr[1]));//--->strlen('b'); --->strlen('98');//野指针---: 0xC0000005: Access violation reading location 0x0000000000000062.


	printf("%d\n", strlen(&arr));//随机值---42

	printf("%d\n", strlen(&arr + 1));//随机值-6 ---36

	printf("%d\n", strlen(&arr[0] + 1));//随机值-1 ---41


	return 0;

}
