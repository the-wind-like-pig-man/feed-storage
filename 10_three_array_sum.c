//three_array_sum.c---编写一个函数，把两个数组中相对应的元素相加，然后把结果存储到第三个数组中
//也就是说，如果数组1中包含的数值是2、4、5、8，数组2中包含的数值是1、0、4、6，那么该函数把3、4、9、14赋值给第三个数组
//函数接受三个数组名和一个数组大小。在一个简单的程序中测试该函数。

#include <stdio.h>

#define INDEX 4

//void add_array(int n, int t[n], const int s1[n], const int s2[n]);

void add_array(int n, int t[], const int s1[], const int s2[]);

//数组加法函数的的三个形参分别表示数组长度、求和结果和两个加数数组
//程序使用变长数组形式分别表示3个数组的形参。s1和s2数组设置为const，
//可以保证在函数内不会修改两个源数组的值

int main()
{

	int sum[INDEX], s1[INDEX] = { 2, 4, 5, 8 }, s2[INDEX] = { 1, 0, 4, 6 };

	add_array(INDEX, sum, s1, s2);

	for (int i = 0; i < INDEX; i++)
	{

		printf("%d ", sum[i]);

	}



	return 0;

}


void add_array(int n, int t[], const int s1[], const int s2[])
{

	for (int i = 0; i < n; i++)
	{

		t[i] = s1[i] + s2[i];

	}
	//相加算法中由于3个数组有共同的长度和对应的下标，因此只需简单相加并且赋值
}

//output:
//3 4 9 14