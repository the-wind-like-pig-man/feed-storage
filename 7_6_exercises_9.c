//7_6_exercises_9.c---判断一个正整数是否是素数，然后显示小于或等于该数的素数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int datum = 0;

	do {
	
		printf("Please enter a number (0 to quit): ");

		scanf("%d", &datum);

		if (datum < 2)
		{

			if (datum == 0) break;

			printf("%d is out of range, retry.\n", datum);

			continue;

		}

		//读取用户输入的正整数

		printf("You input %d, so the prime from %d to 2 is: ", datum, datum);

		for (int i = datum; i > 1; i--)
		{
			//输入数据到2的循环，判断区间内的每一个数是否是素数
			
			int is_prime = 1;

			for (int j = 2; j <= i / 2; j++)
			{

				if (i % j == 0)
				{

					is_prime = 0;

					break;

				}//可以被1或其本身之外的整数整数，表示is_prime为0，退出素数判断循环

			}

			if (is_prime == 1)
			{

				printf("%d, ", i);
				//依据素数标记，判断是否打印区间内的素数

			}


		}

		printf("\n");

	
	} while (datum != 0);

	printf("Well done!Bye!");

	return 0;

}

//output:
//Please enter a number(0 to quit) : 6
//You input 6, so the prime from 6 to 2 is : 532
//Please enter a number(0 to quit) : 8
//You input 8, so the prime from 8 to 2 is : 7532
//Please enter a number(0 to quit) : 4
//You input 4, so the prime from 4 to 2 is : 32
//Please enter a number(0 to quit) : 1
//1 is out of range, retry.
//Please enter a number(0 to quit) : 0
//Well done!Bye!

//output:
//Please enter a number(0 to quit) : 6
//You input 6, so the prime from 6 to 2 is : 5, 3, 2,
//Please enter a number(0 to quit) : 8
//You input 8, so the prime from 8 to 2 is : 7, 5, 3, 2,
//Please enter a number(0 to quit) : 1
//1 is out of range, retry.
//Please enter a number(0 to quit) : 0
//Well done!Bye!