﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <assert.h>

#include <string.h>

#include <stdio.h>

#include <errno.h>



//
//int main()
//{
//
//	int arr1[] = { 1, 2, 3, 4, 5 };//内存中存储：01 00 00 00 02 00 00 00 03 00 00 00 04 00 00 00 05 00 00 00
//
//	int arr2[] = { 1, 4, 5 };//01 00 00 00 04 00 00 00 05 00 00 00
//
//	int ret = memcmp(arr1, arr2, 12);
//
//	printf("%d\n", ret);//-1，小于
//
//	return 0;
//
//}

//int main()
//{
//
//	//char arr[] = "hello world!";
//
//	////memset(arr, 'x', 5);
//
//	//memset(arr + 6, 'x', 6);//hello xxxxxx
//
//
//	////printf("%s\n", arr);//xxxxx world!
//
//	//printf("%s\n", arr);//
//
//	int arr[10] = { 0 };
//	//把该数组初始化为全1
//
//	memset(arr, 1, 40);
//
//	int i = 0;
//
//	for (i = 0; i < 10; i++)
//	{
//
//		printf("%d\n", arr[i]);//该函数对相应数据按字节初始化
//		//输出：
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//		//16843009
//
//	}
//
//
//	return 0;
//
//}

//小乐乐改数字，小乐乐喜欢数字，尤其喜欢0和1，他现在得到了一个数，想把每位的数变成0或1
//如果某一位是奇数，就把它变成1，如果是偶数，就把它变成0，请回答他最后得到的数是多少。
//输入描述：输入包含一个数n(0<=n<=10^9)
//输出描述：输出一个数，即小乐乐修改后的数字
//例如输入：22222
//输出：0
//输入：123 
//输出：101
//  


int main()
{

	int input = 0;

	int sum = 0;

	//输入数字

	scanf("%d", &input);//123

	int i = 0;

	while (input)
	{

		int bit = input % 10;

		if (bit % 2 == 1)
		{

			sum += 1 * pow(10, i);

			i++;

		}

		else
		{

			sum += 0 * pow(10, i);

			i++;

		}

		input /= 10;

	}

	printf("%d\n", sum);


	return 0;

}