//addemup.c---几种常见的语句

#include <stdio.h>

int main()//计算前20个整数的和
{

	int count, sum;//声明

	count = 0;//表达式语句

	sum = 0;//表达式语句

	while (count++ < 20)
	{

		sum = sum + count;//迭代语句

		printf("sum = %d\n", sum);//表达式语句

	}


	return 0;

}

//output:
//sum = 1
//sum = 3
//sum = 6
//sum = 10
//sum = 15
//sum = 21
//sum = 28
//sum = 36
//sum = 45
//sum = 55
//sum = 66
//sum = 78
//sum = 91
//sum = 105
//sum = 120
//sum = 136
//sum = 153
//sum = 171
//sum = 190
//sum = 210
