//exercises_4.c---统计一串字符流中单词的字母数

#include <stdio.h>

#include <ctype.h>

int main()
{

	int words = 0;

	int letter = 0;

	char ch = 0;

	//while ((ch = getchar()) != EOF)

		while ((ch = getchar()) != '\n')

	{

		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))

			letter++;//除了可以使用字母在ASCII码表中连续的方法判断字母数量之外，还可以使用
		//isalpha()函数进行判断，若ch是字母，返回非零值。if(isalpha(ch)) letter++;

		if (ch == ' ' || ch == ',' || ch == '.' || ch == '\n')
		{

			words++;
			//可以利用标点符号进行判断，上面的判断不够完整，括号、问号、感叹号等未包括
			//也可以利用ASCII码中符号的连续区间进行判断。使用ispunct()函数更加简便。但是，该函数无法判断空格，因此需要取‘或’
			//if(ispunct(ch) != 0 || ch = ' ') words++;
		}
	}

	printf("There are %d words, and %d character, %.2f C/W!\n", words, letter, 1.0 *
		letter / words);


	return 0;
}

//output:
//I come from China, I am a Chinese people.Ilove China.
//There are 12 words, and 41 character, / W!

//output1:
//I come from China, I am a Chinese people.I love China.
//There are 13 words, and 41 character, 3.15 C / W!