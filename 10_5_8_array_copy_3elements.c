//array_copy_elements.c---使用编程练习2中的复制函数，把一个内含7个元素的数组中的第3~5个元素复制
//到内含三个元素的数组中。该函数本身不需要修改，只需要选择合适的实参（不需要数组名和数组大小，只需要
//数组元素的地址和待处理元素的个数）

#include <stdio.h>

#include <stdlib.h>

void copy_ptr(double* t, double* s, int n);
//只保留以指针作为形参的函数

int main()
{

	double scr[] = { 1, 2, 3, 4, 5, 6, 7 };

	double targ[3];

	copy_ptr(targ, scr + 2, 3);
    //函数的实参为目标数组地址、源数组中第三个元素的地址以及复制的元素个数

	printf("Now show th scr array:\n");

	for (int i = 0; i < 7; i++)
	{

		printf("%.01f ", scr[i]);

	}

	printf("\nNow show the dest array:\n");

	for (int i = 0; i < 3; i++)
	{

		printf("%.01f ", targ[i]);

	}



	return 0;

}

void copy_ptr(double* t, double* s, int n)
{

	for (int i = 0; i < n; i++)
	{

		*(t + i) = *(s + i);
		//使用指针作为函数的参数，需要标明指针的访问范围，不能越界，并使用
		//星号形式进行数据的赋值

	}

}

//output:
//Now show th scr array :
//	1.0 2.0 3.0 4.0 5.0
//	Now show the dest array :
//	3.0 4.0 5.0

//output1:
//Now show th scr array :
//	1.0 2.0 3.0 4.0 5.0 6.0 7.0
//	Now show the dest array :
//	3.0 4.0 5.0