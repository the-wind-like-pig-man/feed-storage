//digui_power,c---编写递归函数，实现double类型数据的整数次幂

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

double power(double n, int p);
//计算double类型的数据n的p次幂，返回值是double类型

int main()
{

	double x, xpow;

	int exp;

	printf("Please enter a number and the integer power  ");

	printf(" to which\nthe number will be rasied.Enter q ");

	printf(" to quit.\n");

	while (scanf("%lf %d", &x, &exp) == 2)
	{

		xpow = power(x, exp);

		printf("%.3g to the power %d is %.5g.\n", x, exp, xpow);

		printf("Please enter the next pairs of numbers or q to quit.\n");

	}

	printf("Hope you enjoy this power trip --- Bye!\n");



	return 0;

}


double power(double n, int p)
{

	//double power = 1;//注意变量名不要与函数名重

	double pow = 1;


	int i = 0;

	if (n == 0 && p == 0)
	{

		printf("The %g to the power %c is not define, return 1.\n", n, p);

		return 1;

	}//0的0次幂单独标注

	if (n == 0)
	{

		return 0;//0的任何次幂（0除外）次幂等于0

	}

	if (p == 0)
	{

		return 1;//除0之外，任何数的0次幂等于1

	}

	if (p > 0)
	{

		return n * power(n, p - 1);//正p次幂递归

	}

	else
	{

		return power(n, p + 1) / n;//负p次幂的递归

	}

}

//output:
//Please enter a number and the integer power   to which
//the number will be rasied.Enter q  to quit.
//0.5 2
//0.5 to the power 2 is 0.25.
//Please enter the next pairs of numbers or q to quit.
//0.5 - 2
//0.5 to the power - 2 is 4.
//Please enter the next pairs of numbers or q to quit.
//0.5 3
//0.5 to the power 3 is 0.125.
//Please enter the next pairs of numbers or q to quit.
//0.5 - 3
//0.5 to the power - 3 is 8.
//Please enter the next pairs of numbers or q to quit.
//q
//Hope you enjoy this power trip-- - Bye!