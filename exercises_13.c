//exercises_13.c---创建一个8个元素的数组，每个元素是2的前8次幂，并打印

#include <stdio.h>

int main()
{

	int data[8] = {0};

	data[0] = 2;

	for (int i = 1; i < 8; i++)
	{

		data[i] = data[i - 1] * 2;

	}


	int i = 0;

	do
	{

		printf("%d ", data[i]);

		i++;

	} while (i < 8);


	printf("\nDone!\n");


	return 0;

}

//output:
//2 4 8 16 32 64 128 256
//Done!