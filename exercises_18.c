#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <string.h>

int main()
{

	int rabnud = 5;

	int weeks = 1;

	while (rabnud < 150)
	{

		printf("At %d weeks, Rubnud has %4d friends \n", weeks, rabnud);

		rabnud = 2 * (rabnud - weeks++);

	}

	printf("\nDone!\n");


	return 0;

}

//output:
//At 1 weeks, Rubnud has    5 friends
//At 2 weeks, Rubnud has    8 friends
//At 3 weeks, Rubnud has   12 friends
//At 4 weeks, Rubnud has   18 friends
//At 5 weeks, Rubnud has   28 friends
//At 6 weeks, Rubnud has   46 friends
//At 7 weeks, Rubnud has   80 friends
//At 8 weeks, Rubnud has  146 friends
//
//Done!
