//exercises_16.c---单利、复利投资示例

#include <stdio.h>

#include <string.h>

int main()
{

	float daphne = 0, deirdre = 0;

	daphne = deirdre = 100.0;

	int year = 0;

	do {
	
		daphne = daphne + 100.0 * 0.1;

		deirdre = deirdre + deirdre * 0.05;

		year++;
	
	} while((deirdre - daphne) < 0);

	//printf("%d year later.\nDaphne = %f.\nDeirdre = %f\n", year, daphne, deirdre);

	printf("%d year later.\n\nDaphne = %f. Deirdre = %f \n", year, daphne, deirdre);

	printf("\nDone!\n");


	return 0;

}
//
//output:
//27 year later.
//Daphne = 370.000000.
//Deirdre = 373.345520
//
//Done!
//
//output:
//27 year later.
//
//Daphne = 370.000000.Deirdre = 373.345520
//
//Done!
