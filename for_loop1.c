//for_geo.c---几何递增

//#include <stdio.h>
//
//int main()
//{
//
//	double debt;//debt---债务，借款，欠款；负债情况；人情债
//
//	for (debt = 100; debt < 150.0; debt = debt * 1.1)
//	{
//
//		printf("Your debt is now $%.2f.\n", debt);
//
//	}
//
//
//	return 0;
//
//}
//
//output：
//Your debt is now $100.00.
//Your debt is now $110.00.
//Your debt is now $121.00.
//Your debt is now $133.10.
//Your debt is now $146.41.

//for_wild.c---打印x及其表达式的值

#include <stdio.h>

int main()
{

	int x = 0;

	int y = 55;

	for (x = 1; y <= 75; y = (++x * 5) + 50)
	{

		printf("%10d %10d\n", x, y);

	}


	return 0;

}

//output:
//1         55
//2         60
//3         65
//4         70
//5         75
