﻿
#define _CRT_SECURE_NO_WARNINGS  0

#include <stdio.h>


//int main()
//{
//	int i = 0;
//
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)//此处不是判断，而是赋值，i=5表达式结果是5
//
//			printf("%d ", i);//5 5 5...
//
//	}
//
//	return 0;
//}

//int main()
//{
//	int n = 0;
//
//	scanf("%d", &n);
//
//	switch (n)
//	{
//
//	case 1:
//
//		printf("1\n");
//	
//		break;
//
//		case 2:
//
//			printf("2\n");
//
//			break;
//
//		default://default的位置可以任意
//
//			printf("hello world!\n");
//
//			break;
//	}
//	return 0;
//}

//int func(int a)
//{
//	int b;
//
//	switch (a)
//	{
//	case 1: b = 30;
//
//	case 2: b = 20;
//
//	case 3: b = 16;
//
//	default: b = 0;
//
//	}
//
//	return b;
//
//}
//
//int main()
//{
//	int a = 1;
//
//	int b = func(a);
//
//	printf("%d\n", b);
//
//	return 0;
//}

//int main()
//{
//	int x = 3;
//
//	int y = 3;
//
//	switch (x % 2)
//	{
//
//	case 1:
//
//		switch (y)
//
//		{
//		case 0:
//
//			printf("first ");
//
//		case 1:
//
//			printf("second ");
//
//			break;
//
//		default: printf("hello ");
//
//		}
//
//	case 2:
//
//		printf("third ");
//
//	}
//
//	return 0;
//
//}

//写一段代码，将三个整数按从大到小输出

//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	int c = 0;
//
//	int temp = 0;
//
//	//输入数字
//
//	scanf("%d %d %d", &a, &b, &c);
//
//	//排序
//
//	if (a < b)
//	{
//
//		temp = a;
//
//		a = b;
//
//		b = temp;
//	}
//
//	if (a < c)
//	{
//		temp = a;
//
//		a = c;
//
//		c = temp;
//
//	}
//
//	if (b < c)
//	{
//		temp = b;
//
//		b = c;
//
//		c = temp;
//
//	}
//
//	printf("%d %d %d\n", a, b, c);
//
//	return 0;
//}

//用函数实现Swap()

void Swap(int* px, int* py)
{
	int temp = *px;

	*px = *py;

	*py = temp;

}
//
//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	int c = 0;
//
//	//int temp = 0;
//
//	//输入数字
//
//	scanf("%d %d %d", &a, &b, &c);
//
//	//排序
//
//	if (a < b)
//	{
//
//		Swap(&a, &b);
//	}
//
//	if (a < c)
//	{
//		
//		Swap(&a, &c);
//
//	}
//
//	if (b < c)
//	{
//		
//		Swap(&b, &c);
//
//	}
//
//	//输出结果
//
//	printf("%d %d %d\n", a, b, c);
//
//	return 0;
//}


//写一段代码，打印1-100之间所有3的倍数

//int main()
//{
//	int i = 0;
//
//	/*for (i = 1; i <= 100; i++)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//
//		}
//
//	}*/
//
//	for (i = 3; i <= 100; i += 3)
//	{
//
//		printf("%d ", i);
//
//	}
//
//	return 0;
//}

//给定两个数，求其最大公约数
//
//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	int c = 0;
//
//	//输入数字
//
//	scanf("%d %d", &a, &b);
//
//	//求最大公约数
//
//	//int min = (a < b) ? a : b;//暴力求解法，效率较低
//
//	//int m = min;
//
//	//while (1)
//	//{
//
//	//	if (a % m == 0 && b % m == 0)
//	//	{
//	//		break;
//	//	}
//
//	//	m--;
//
//	//}
//
//	//printf("%d\n", m);
//
//	return 0;
//}

//辗转相除法
//
//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	int c = 0;
//
//	//输入数字
//
//	scanf("%d %d", &a, &b);
//
//	//求其最大公约数
//
//	//while (a % b)//多计算了一次a%b
//
//	while (c = a % b)//减少一次a%b的计算
//
//	{
//
//		//int c = a % b;
//
//		a = b;
//
//		b = c;
//	}
//
//	printf("%d\n", b);
//
//	return 0;
//}
//
//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	for (a = 1, b = 1; a <= 100; a++)//a = 1 2 3 4 5 6 7 8
//	{
//		if (b >= 20)break;//4 7 10 13 16 19 22
//
//		if (b % 3 == 1)
//		{
//			b = b + 3;
//
//			continue;
//		}
//
//		b = b - 5;
//
//	}
//
//	printf("%d\n", a);
//
//	return 0;
//}

//编写一段程序打印出1-100的所有整数中出现9的个数

//9 19 29 39 49 59 69 79 89 90 91 92 93 94 95 96 97 98 99

//int main()
//{
//	int i = 0;
//
//	int count = 0;//计数
//
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)//判断个位是否为9
//
//			count++;
//
//		if (i / 10 == 9)//判断十位是否为9
//
//			count++;
//
//	}
//
//	printf("%d\n", count);
//
//	return 0;
//}

//计算1/1-1/2+1/3-1/4+1/5...+1/99-1/100

int main()
{
	int i = 0;

	double sum = 0;

	int flag = 1;

	for (i = 1; i <= 100; i++)
	{

		sum = sum + flag * (1.0 / i);

		flag = - flag;

	}

	printf("%lf\n", sum);

	return 0;

}