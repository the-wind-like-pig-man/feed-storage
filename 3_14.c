﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <math.h>

//void print(int* p, int sz)
//{
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", *(p + i));
//
//	}
//
//	printf("\n");
//
//
//}
//
//int main()
//{
//
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	print(arr, sz);
//
//
//	return 0;
//
//}
//


//int main()//为何注释掉了仍然可以打印，未注释的函数却不打印？
//{
//
//	int a = 0x11223344;
//
//	char* pc = (char*)&a;//
//
//	*pc = 0;//char型指针只能改变整型数据的一个字节，内存中数据由低到高字节存放，输出：0x11223300
//
//	printf("%x\n", a);
//
//	return 0;
//
//}


//从屏幕输入一串字符（0<=n<=10000）
//逆序输出
//
//int main()
//{
//
//	char arr[10001] = { 0 };
//
//	gets(arr);
//
//	//逆序排列
//
//	int left = 0;
//
//	int right = strlen(arr) - 1;
//
//	while (left < right)
//	{
//
//		char tmp = arr[left];
//
//		arr[left] = arr[right];
//
//		arr[right] = tmp;
//
//		left++;
//
//		right--;
//
//	}
//
//	printf("%s\n", arr);
//
//
//	return 0;
//
//}

//求Sn=a+aa+aaa+aaaa+aaaaa的前五项之和，其中a是一个数字
//例如：2+22+222+2222+22222
//2
//2*10+2=22 
//22*10+2=222 
//

//int main()
//{
//
//	int a = 0;
//
//	int n = 0;
//
//	scanf("%d %d", &a, &n);//输入：2 5---输出：24690
//
//	int i = 0;
//
//	int sum = 0;
//
//	int k = 0;
//
//	for (i = 0; i < n; i++)
//	{
//
//		k = k * 10 + a;
//
//		sum += k;
//
//	}
//
//	printf("%d\n", sum);
//
//	
//	return 0;
//
//}


//求出0~10000之间的所有“水仙花数”并输出
//“水仙花数”是指一个n位数，其各位数字的n次方之和恰好等于该数本身，例如：153= 1^3+5^3+3^3,则153是一个“水仙花数”
//
//

//int main()
//{
//
//	int i = 0;
//
//	for (i = 0; i <= 10000; i++)
//	{
//
//		//判断i是否为“水仙花数”
//		//1234
//		//1.计算i是几位数-->n
//		
//		int n = 1;//任何一个数至少是一位数
//		
//		int tmp = i;//创建中间变量，防止i在循环内部被改变
//		
//		int sum = 0;
//
//		while (tmp / 10)
//		{
//
//			n++;
//
//			tmp /= 10;
//
//		}
//
//		//2.得到i的每一位，计算它的n次方之和
//		
//		tmp = i;
//
//		while (tmp)
//		{
//
//			sum += pow(tmp % 10, n);
//
//			tmp /= 10;
//
//		}
//
//		if (sum == i)
//		{
//
//			printf("%d ", i);
//
//		}
//	}
//
//
//	return 0;
//
//
//}
//
//int is_narcrissistic_number(int i)//“水仙花数”函数实现
//{
//
//	//判断i是否为“水仙花数”
//			//1234
//			//1.计算i是几位数-->n
//
//	int n = 1;//任何一个数至少是一位数
//
//	int tmp = i;//创建中间变量，防止i在循环内部被改变
//
//	int sum = 0;
//
//	while (tmp / 10)
//	{
//
//		n++;
//
//		tmp /= 10;
//
//	}
//
//	//2.得到i的每一位，计算它的n次方之和
//
//	tmp = i;
//
//	while (tmp)
//	{
//
//		sum += pow(tmp % 10, n);
//
//		tmp /= 10;
//
//	}
// 
// return sum == i;//此句等价于下面if---else语句
//
//	if (sum == i)
//	{
//
//		return 1;
//
//	}
//
//	else
//	{
//
//		return 0;
//
//	}
//
//
//}
//
//
//
//int main()
//{
//
//	int i = 0;
//
//	for (i = 0; i <= 10000; i++)
//	{
//		if (is_narcrissistic_number(i))
//		{
//
//			printf("%d ", i);
//
//		}
//
//	}
//
//	return 0;
//
//
//}

