//exercises_1.c---读取输入，读到#字符停止，然后报告读取的空格数、换行符数和其他字符的数量

#include <stdio.h>

int main()
{

	//char ch = 0;

	char ch;

	int blank = 0;

	int endline = 0;

	int others = 0;

	printf("Please input chars (# for exit) : ");

	while ((ch = getchar()) != '#')
	{

		if (ch == ' ')

			blank++;//统计空格数

		if (ch == '\n')

			endline++;//统计换行符数

		else

			others++;//统计其它字符数

	}

	printf("%d blank, %d endline, %d others", blank, endline, others);



	return 0;

}
//
//output:
//Please input chars(# for exit) : I come from China, i am a Chinese people.I love China very much!
//I come from China, i am a Chinese people.I love China very much!
//I come from China, i am a Chinese people.I love China very much!
//#
//33 blank, 3 endline, 189 others