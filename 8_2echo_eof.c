//echo_eof.c---重复输入，直到文件结尾

#include <stdio.h>

int main()
{

	int ch = 0;

	while ((ch = getchar()) != EOF)
	{

		putchar(ch);

	}


	return 0;

}

//output:
//She walks in beauty, like the night
//She walks in beauty, like the night
//Of cloudless climes and starry skies...
//Of cloudless climes and starry skies...
//Ctrl+Z

//output:
//She walks in beauty, like the night
//She walks in beauty, like the night
//Of cloudless climes and starry skies...
//Of cloudless climes and starry skies...
//^D
//
//^ Z
