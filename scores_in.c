//scores_in.c---使用循环处理数组

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define SIZE 10

#define PAR 72

int main()
{

	int index = 0;
	
	int score[SIZE] = { 0 };

	int sum = 0;

	float average = 0.0;

	printf("Enter %d golf scores:\n", SIZE);

	for (index = 0; index < SIZE; index++)
	{

		//scanf("%d", &score[SIZE]);//读取十个分数

		scanf("%d", &score[index]);//读取十个分数


	}

	printf("The scores read in are as follows:\n");


	for (index = 0; index < SIZE; index++)
	{

		printf("%5d", score[index]);//验证输入

	}

	printf("\n");

	for (index = 0; index < SIZE; index++)
	{

		sum += score[index];//求总分数

		average = (float) sum / SIZE;//求平均分

		/*printf("Sum of scores = %d, average = %.2f\n", sum, average);

		printf("That's a handicap of %.0f.\n", average - PAR);*/

	}

	printf("Sum of scores = %d, average = %.2f\n", sum, average);

	printf("That's a handicap of %.0f.\n", average - PAR);




	return 0;


}

//output:---&score[SIZE]
//Enter 10 golf scores :
//99 95 109 105 100 96 98 93 99 97 98
//The scores read in are as follows :
//97    0    0    0    0    0    0    0    0    0
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.
//Sum of scores = 97, average = 9.70
//That's a handicap of -62.


//output:---&score[index]
//Enter 10 golf scores :
//99 95 109 105 100 96 98 93 99 97 98
//The scores read in are as follows :
//99   95  109  105  100   96   98   93   99   97
//Sum of scores = 99, average = 9.90
//That's a handicap of -62.
//Sum of scores = 194, average = 19.40
//That's a handicap of -53.
//Sum of scores = 303, average = 30.30
//That's a handicap of -42.
//Sum of scores = 408, average = 40.80
//That's a handicap of -31.
//Sum of scores = 508, average = 50.80
//That's a handicap of -21.
//Sum of scores = 604, average = 60.40
//That's a handicap of -12.
//Sum of scores = 702, average = 70.20
//That's a handicap of -2.
//Sum of scores = 795, average = 79.50
//That's a handicap of 8.
//Sum of scores = 894, average = 89.40
//That's a handicap of 17.
//Sum of scores = 991, average = 99.10
//That's a handicap of 27.