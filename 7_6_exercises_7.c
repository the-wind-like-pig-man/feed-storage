//exercises_7.c---输入一周工作的小时数，然后打印工资总额、税金和总收入

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define BASE_SALARY 10.00

#define EXTRA_HOUR 1.5

#define BASE_TAX 0.15

#define EXTRA_TAX 0.2

#define EXCEED_TAX 0.25
//常量的定义

int main()
{

	float hours = 0;

	float salary = 0, tax = 0, taxed_salary = 0;
	//工资、税金、净收入

	printf("Enter the working hours a week: ");

	scanf("%f", &hours);

	if (hours <= 30)
	{

		salary = hours * BASE_SALARY;

		tax = salary * BASE_TAX;

		taxed_salary = salary - tax;
		//30小时以内没有加班，标准基础税率

	}

	else if (hours <= 40)
	{

		salary = hours * BASE_SALARY;

		tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		taxed_salary = salary - tax;
		//30~40小时无加班，额外税率

	}

	else
	{

		salary = (40 + (hours - 40) * EXTRA_HOUR) * BASE_SALARY;

		if (salary <= 450)
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		}

		else
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX + (salary - 450) * EXCEED_TAX;

		}

		taxed_salary = salary - tax;
		//40小时以上加班，税率按工资450美元的分界扣减

	}

	printf("Your salary before tax is %.2f, tax is %.2f, salary after tax is %.2f.\n",
		salary, tax, taxed_salary);

	printf("Done!\n");


	return 0;

}

//
//output:
//Enter the working hours a week : 30
//Your salary before tax is 300.00, tax is 45.00, salary after tax is 255.00.
//Done!

//output1:
//Enter the working hours a week : 40
//Your salary before tax is 400.00, tax is 65.00, salary after tax is 335.00.
//Done!

//output2:
//Enter the working hours a week : 70
//Your salary before tax is 850.00, tax is 255.00, salary after tax is 595.00.
//Done!