﻿

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>


//
//KIKI想知道已经给出的三条边a,b,c能否构成三角形，如果能构成三角形，判断三角形的类型（等边三角形、等腰三角形或普通三角形）
//输入：有多组输入数据，每一行输入三个a,b,c(0<a,b,c<1000),Z作为三角形的三个边，用空格分隔
//输出：针对每组输入数据，输出占一行，如果构成三角形，等边三角形则输出“Equilateral triangle!”。等腰三角形则
// 输出“Isosceles triangle!”。其余三角形则输出“Ordinary triangle!”。反之则输出“Not a triangle!”。
// 如输入：
// 2 3 2
// 3 3 3 
// 输出：
// Isosceles triangle!
// Equilateral triangle!
// 
// 

int main()
{

	int a = 0;

	int b = 0;

	int c = 0;

	while (scanf("%d %d %d", &a, &b, &c) == 3)
	{


		//判断是否构成三角形

		if ((a + b > c) && (a + c > b) && (b + c > a))
		{

			if (a == b && b == c)//等边三角形
			{

				printf("Equilateral triangle!\n");

			}

			else if ((a == b && b != c) || (a == c && b != c) || (b == c && a != b))//等腰三角形
			{

				printf("Isosceles triangle!\n");

			}

			else//普通三角形
			{

				printf("Ordinary triangle!\n");

			}

			
		}

		else//不是三角形
		{

			printf("Not a triangle!\n");

		}


	}


	return 0;

}
