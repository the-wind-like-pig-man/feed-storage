//summing.c---根据用户键入的整数求和

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	long num;

	long sum = 0L;//把sum初始化为0

	int status;

	printf("Please enter an integer to be summed ");

	printf("(q to quit): ");

	//status = scanf("%1d", &num);//---%ld not %1d

	status = scanf("%ld", &num);

	while (status == 1)// ==的意思是“等于”
	{

		sum = sum + num;

		printf("Please enter next integer (q to quit): ");

		status = scanf("%ld", &num);

		//status = scanf("%1d", &num);//------%ld not %1d


	}

	printf("Those integers sum to %ld.\n", sum);


	return 0;

}

//output:
//Please enter an integer to be summed(q to quit) : 44
//Please enter next integer(q to quit) : 33
//Please enter next integer(q to quit) : 88
//Please enter next integer(q to quit) : 121
//Please enter next integer(q to quit) : q
//Those integers sum to 286.