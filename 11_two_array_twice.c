//two_array_twice.c---编写一个程序，声明一个int类型的3x5二维数组，并用合适的值初始化它
//该程序打印数组中的值，然后把各值翻倍（即是原值的二倍），并显示出各元素的新值。编写一个函数
//以显示数组的内容，再写一个函数把各个元素翻倍。这两个函数都是以数组名和行数作为参数。

#include <stdio.h>

#define COLS 5

#define ROWS 3

//void show_element(int rows, int cols, const int t[rows][cols]);

//void double_element(int rows, int cols, int t[rows][cols]);

//程序内的打印函数和元素加倍函数均使用变长数组作为参数，使用传统方式定义两个函数的语句如下

//void show_element(int rows, int t[][COLS]);

void show_element(int rows, const int t[][COLS]);

void double_element(int rows, int t[][COLS]);

//两者在使用中没有区别，可以根据编译器的情况选用。打印函数不修改数组元素，需要使用const
//关键字修饰，加倍函数则不能使用const修饰二维数组的参数

int main()
{

	int arr[ROWS][COLS] = { {1, 0, 4, 6, 9},{2, 5, 6, 8, 3}, {5, 3, 21, 1, 6}, };

	/*show_element(ROWS, COLS, arr);

	double_element(ROWS, COLS, arr);

	printf("\n");

	show_element(ROWS, COLS, arr);*/
	//变长数组传参

	//传统数组传参
	show_element(ROWS, arr);

	double_element(ROWS, arr);

	printf("\n");

	show_element(ROWS, arr);



	return 0;

}

void show_element(int rows, const int t[][COLS])//传统数组打印函数
{

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < COLS; j++)
		{

			printf("%4d ", t[i][j]);

		}

	}

}

//void show_element(int rows, int cols, const int t[rows][cols])//变长数组打印函数
//{
//
//	for (int i = 0; i < rows; i++)
//	{
//
//		for (int j = 0; j < cols; j++)
//		{
//
//			printf("%4d ", t[i][j]);
//
//		}
//
//	}
//
//}


void double_element(int rows, int t[][COLS])//传统数组二倍元素值函数
{

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < COLS; j++)
		{

			t[i][j] *= 2;

			//t[i][j] = t[i][j] * 2;
			//通过循环嵌套来实现数组内容的打印和元素数据的处理。需要注意循环的终止条件判断
			//使用变长数组参数传递方式再循环控制上略有差异，如：
			//for(int i = 0; i < rows; i++)
			//{
			// for(intj = 0; j < cols; j++)
			// {printf("%4d ", t[i][j]);} 
			// } 
			
		}

	}

}

//void double_element(int rows, int cols, int t[rows][cols])//变长数组二倍元素值函数
//{
//
//	for (int i = 0; i < rows; i++)
//	{
//
//		for (int j = 0; j < cols; j++)
//		{
//
//			t[i][j] *= 2;
//
//			//t[i][j] = t[i][j] * 2;
//			//通过循环嵌套来实现数组内容的打印和元素数据的处理。需要注意循环的终止条件判断
//			//使用变长数组参数传递方式再循环控制上略有差异，如：
//			//for(int i = 0; i < rows; i++)
//			//{
//			// for(intj = 0; j < cols; j++)
//			// {printf("%4d ", t[i][j]);} 
//			// } 
//
//		}
//
//	}
//
//}

//output:
//1    0    4    6    9    2    5    6    8    3    5    3   21    1    6
//2    0    8   12   18    4   10   12   16    6   10    6   42    2   12