//paint.c---使用条件运算符*/

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define COVERAGE 350//每罐油漆可刷的面积（单位：平方英尺）

int main()
{

	int sq_feet = 0;

	int cans = 0;

	printf("Please enter numbei of square feet to be painted:\n");

	while (scanf("%d", &sq_feet) == 1)
	{

		cans = sq_feet / COVERAGE;

		cans += ((sq_feet % COVERAGE == 0)) ? 0 : 1;

		printf("You need %d %s of paint.\n", cans,
			cans == 1 ? "can" : "cans");

		printf("Please enter next value (q to quit):\n");

	}


	return 0;

}
//
//output:
//Please enter numbei of square feet to be painted :
//349
//You need 1 can of paint.
//Please enter next value(q to quit) :
//	351
//You need 2 cans of paint.
//Please enter next value(q to quit) :
//500
//You need 2 cans of paint.
//Please enter next value(q to quit) :
//q
