﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

//
//int main()
//{
//
//	int a, b, c;
//
//	a = 5;
//
//	c = ++a;//c=6 a=6
//
//	b = ++c, c++, ++a, a++;//c=8 b=7 a=8,逗号表达式，优先级最低
//
//	b += a++ + c;//a=9 b=23
//
//	printf("a = %d b = %d c = %d\n:", a, b, c);//9 23 8
//
//
//	return 0;
//
//}


//写一个函数返回参数是其二进制中1的个数

//int count_num_of_1(int n)//-1测不出来
//int count_num_of_1(unsigned int n)//-1能测出来
//
//{
//
//	int count = 0;
//
//	while (n)
//	{
//		
//		if ((n % 2) == 1)
//		{
//
//			count++;
//
//		}
//
//		n /= 2;
//
//	}
//
//	return count;
//
//}
////-1
////100000000000000000000000000000001---原码
////111111111111111111111111111111110---反码
////111111111111111111111111111111111---补码

//int count_num_of_1(int n)
//{
//
//	int i = 0;
//
//	int count = 0;
//
//	for (i = 0; i < 32; i++)
//	{
//
//		//if (((n >> 1) & 1) == 1)
//
//		if (((n >> i) & 1) == 1)//向右移动i位，后按位与1，统计二进制位中1的个数
//
//		{
//
//			count++;
//
//		}
//
//	}
//
//	return count;
//
//}

//15
//n = n&(n-1)
//1111 n
//1110 n-1
//1110 n
//1101 n-1
//1100 n
//1011 n-1
//1000 n
//0111 n-1
//0000 n 
// 
// 
//

//int count_num_of_1(int n)//思路很巧妙
//{
//
//	int count = 0;
//
//	while (n)
//	{
//
//		n = n & (n - 1);
//
//		count++;
//
//	}
//
//	return count;
//
//}
//
////如何判断一个属是不是2的n次方
////2^1
////10 
////2^2 
////100
////2^3
////1000
//// 
////   
//////
////if ((n & (n - 1)) == 0)
////{
////	//是2的n次方
////}
//
//
//int main()
//{
//	int num = 0;
//
//	scanf("%d", &num);
//
//	int n = count_num_of_1(num);
//
//	printf("%d\n", n);
//
//	return 0;
//
//}


//两个int（32位整数m和n的二进制表达式中有多少个位（bit）不同
// 
//

int count_diff_bit(int m, int n)
{

	int count = 0;

	int i = 0;

	for (i = 1; i < 32; i++)
	{

		if (((m >> i) & 1) != ((n >> i) & 1))
		{

			count++;

		}


	}

	return count;

}

//int count_diff_bit(int m, int n)
//{
//
//	int count = 0;
//
//	//^异或操作符
//	//相同为0，相异为1
//
//	int ret = m ^ n;
//
//	//统计一下ret中二进制位中有几个1
//
//	while (ret)
//	{
//
//		ret = ret & (ret - 1);
//
//		count++;
//
//	}
//
//	return count;
//
//}

//int main()
//{
//
//	int m = 0;
//
//	int n = 0;
//
//	scanf("%d %d", &m, &n);
//
//	int ret = count_diff_bit(m, n);
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}

//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印二进制序列
//10
//00000000000000000000000000001010
//
//

int main()
{

	int i = 0;

	int num = 0;

	scanf("%d", &num);

	//获取奇数位的数字

	for (i = 30; i >= 0; i -= 2)
	{

		printf("%d ", (num >> i) & 1);

	}

	printf("\n");

	//获取偶数位的数字

	for (i = 31; i >= 1; i -= 2)
	{

		printf("%d ", (num >> i) & 1);

	}



	return 0;

}