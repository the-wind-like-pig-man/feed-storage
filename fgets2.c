//fgets2.c---ʹ��fgets()��fputs()

#include <stdio.h>

#define STLEN 10

int main()
{

	char words[STLEN];

	puts("Please enter strings(empty line to quit): ");

	while (fgets(words, STLEN, stdin) != NULL && words[0] != '\n')
	{

		fputs(words, stdout);

	}

	puts("Done!");


	return 0;

}

//output:
//Please enter strings(empty line to quit) :
//	By the way, the gets() fuction
//	By the way, the gets() fuction
//	also returns a null pointer if it
//	also returns a null pointer if it
//	encounters end - of - file.
//	encounters end - of - file.
//
//	Done!