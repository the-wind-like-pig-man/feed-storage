//chline.c---设计函数，打印指定的字符j行i列

#define _CRT_SECURE_NO_WARNINGS 0

#include  <stdio.h>

void chline(char ch, int cols, int rows);//函数声明，无返回值，3个参数分别是char、int、int
//题目要求j行i列，因此需要注意chline(ch, j, i)形参的顺序和含义
//

int main()
{

	char c = 0;

	int i = 0, j = 0;

	printf("Please enter the char you want to print: ");

	scanf("%c", &c);

	printf("Please enter the cols and rows you want to print: ");
	//变量j负责打印行，i负责打印列，sacnf()函数读取输入顺序&j,&i

	scanf("%d %d", &j, &i);

	chline(c, i, j);


	return 0;

}

void chline(char ch, int cols, int rows)
{

	for (int m = 0; m < rows; m++)
	{

		for (int n = 0; n < cols; n++)
		{

			printf("%c", ch);

		}

		printf("\n");

	}

}
//在嵌套循环中，外层循环控制行数，内层循环控制列数
//外层循环的循环判断使用rows,并负责打印换行符，
//内容循环的循环控制判断使用形参cols，并打印字符，无换行

//output:
//Please enter the char you want to print : a
//Please enter the cols and rows you want to print : 3 3
//aaa
//aaa
//aaa