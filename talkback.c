//talkback.c---演示与用户交互

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>//提供strlen()函数的原型

#include <string.h>//人体密度（单位：磅/立方英尺）

#define DENSITY 62.4

int main()
{

	float weight, volume;

	int size, letters;

	char name[40] = { 0 };//name是一个可以容纳40个字符的数组

	printf("Hi! What's your first name?\n");

	scanf("%s", name);

	printf("%s, what's your weight in pounds?\n", name);

	scanf("%f", &weight);

	size = sizeof(name);

	letters = strlen(name);

	volume = weight / DENSITY;

	printf("%d\n", size);

	printf("Well, %s, ypur volume is %2.2f cubic feet.\n",

		name, volume);

	printf("Also, your first name has %d letters,\n",

		letters);

	printf("and we have %d bytes to store it.\n", size);


	return 0;

}

//output: 
//Hi!What's your first name?
//Christine
//Christine, what's your weight in pounds?
//154
//40
//Well, Christine, ypur volume is 2.47 cubic feet.
//Also, your first name has 9 letters,
//and we have 40 bytes to store it.