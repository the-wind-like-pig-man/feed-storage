//int_array_max.c---编写一个函数，返回存储在int类型数组中的最大值，并在一个简单的程序中测试该函数

#include <stdio.h>

int get_max(int number[], int n);//使用传统方式传递参数，形参n表示整形数组的长度

int main()
{

	int source[100] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	printf("The largest number in array is %d\n", get_max(source, 100));


	return 0;

}

int get_max(int number[], int n)
{

	int max = number[0];
	int i_max = 0;

	for (int i = 0; i < n; i++)
	{

		if (max < number[i])
		{

			max = number[i];

			i_max = i;

		}

	}

	//return i_max;
	return max;
	//函数返回该数组的最大值
}


//output：
//The largest number in array is 9