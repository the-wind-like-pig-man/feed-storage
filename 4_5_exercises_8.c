//4_5_exercises_8.c---输入油耗数，里程数，计算单位油耗并打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define GALLON_TO_LITRE 3.785

#define MILE_TO_KM 1.609//使用define语句定义单位之间的换算比例

int main()
{

	float range = 0, oil = 0;

	printf("Please input the range you traveled(in mile): ");

	scanf("%f", &range);//以英里为单位读取旅行里程

	printf("Please input the range you spend(in gallon): ");

	scanf("%f", &oil);//以加仑为单位读取消耗的汽油

	printf("In USA, your oil wear is %.1f M/G\n", range / oil);//美油耗

	printf("In Europe, your oil wear is %.1f L / 100KM\n", (oil * GALLON_TO_LITRE) /
		(range * MILE_TO_KM));//英油耗




	return 0;

}

//output:
//Please input the range you traveled(in mile) : 100
//Please input the range you spend(in gallon) : 100
//In USA, your oil wear is 1.0 M / GNIn Europe, your oil wear is 2.4 L / 100KM

//output:
//Please input the range you traveled(in mile) : 100
//Please input the range you spend(in gallon) : 100
//In USA, your oil wear is 1.0 M / G
//In Europe, your oil wear is 2.4 L / 100KM
