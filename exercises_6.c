//exercises_6.c---打印一个表格，输出一个整数，它的平方，及立方

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>


int main()
{

	int start = 0, end = 0;

	printf("Please enter the start number: ");

	scanf("%d", &start);

	printf("Please enter the end number: ");

	scanf("%d", &end);

	printf("    Qri:      Square:      Cubic: \n");

	for (int i = start; i <= end; i++)
	{

		//printf("%6d,%10d,%10d", i, i * i, i * i * i);

		printf("%6d %10d %12d", i, i * i, i * i * i);

		printf("\n");

	}

	//printf("\n");


	return 0;

}

//output:
//Please enter the start number : 2
//Please enter the end number : 8
//   Qri :    Square :     Cubic :
//	2          4            8
//	3          9           27
//	4         16           64
//	5         25          125
//	6         36          216
//	7         49          343
//	8         64          512