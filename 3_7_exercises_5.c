#define _CRT_SECURE_NO_WARNINGS 0

//3_7_exercises_5.c---以秒的形式打印一个人的年龄

#include <stdio.h>

#define SEC_PER_YEAR 3.156e7//通过预编译指令定义每年的秒数

int main()
{

	float second = 0, year = 0;

	printf("Please enter how many years old you are: ");

	scanf("%f", &year);

	second = year * SEC_PER_YEAR;

	printf("You are %.1f years old.\n", year);

	printf("And you are %e seconds old,too.\n", second);



	return 0;

}

//output:
//Please enter how many years old you are : 12.5
//You are 12.5 years old.
//And you are 3.945000e+08 seconds old, too.
