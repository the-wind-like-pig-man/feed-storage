//计算20天里赚多少钱

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main(int argc, char* argv[])
{

	int count = 0, sum = 0;

	printf("Please enter the number of days you work: ");

	scanf("%d", &count);

	while (count > 0)
	{

		sum = sum + count--;

	}

	printf("You earned $ %d total!\n", sum);

	printf("PROGRAM EXIT!\n");



	return 0;

}

//
//output:
//Please enter the number of days you work : 20
//You earned $ 210 total!
//PROGRAM EXIT!