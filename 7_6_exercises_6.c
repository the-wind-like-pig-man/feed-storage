//exercises_6.c---读取输入，读到#字符时停止，报告ei出现的次数

#include <stdio.h>

int main()
{

	int counter = 0;

	int halfpair = 0;
//部分匹配标记

	char ch = 0;

	printf("Please input chars (# for exit): ");

	while ((ch = getchar()) != '#')
	{

		switch (ch)
		{

		case 'e':

			halfpair = 1;

			break;//字符e的匹配标记

		case 'i':

			if (halfpair == 1)
			{

				counter++;

				halfpair = 0;

			}//若匹配标记为1，表明前一个字符e已经匹配，此时，若i匹配则计数，并清除匹配标记

			break;

		default:

			halfpair = 0;
			//无论字符e是否匹配，非e的字符和字符i均可以清空部分匹配标记
		}

		

	}

	printf("\n Totally exist %d \'ei\' in all char!\n", counter);

	printf("Done!\n");



	return 0;

}

//output:
//Please input chars(# for exit) : Receive your eieio award.#
//
//Totally exist 3 'ei' in all char!
//Done!

//output:
//Please input chars(# for exit) : Receive is better than give.Receive your eieieieieio award!#
//
//Totally exist 7 'ei' in all char!
//Done!
