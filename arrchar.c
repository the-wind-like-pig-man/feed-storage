//arrchar.c---指针数组，字符串数组

#include <stdio.h>

#define SLEN 40

#define LIM 5

int main()
{

	const char* mytalents[LIM] = {
	
		"Adding numbers swiftly",

		"Multiplying accurately", "Stashing data",

		"Following instructions to the letter",

		"Unsderstanding the C language"
	
	};

	char yourtalents[LIM][SLEN] = {
	
	
		"Walking in a straight line",

		"Sleeping", "Watching television",

		"Mailing letters", "Reading email"
	
	};

	int i = 0;

	puts("Let's compare talents.");

	printf("%-36s %-25s\n", "My Talents", "Your Talents");

	for (i = 0; i < LIM; i++)
	{

		printf("%-36s %-25s\n", mytalents[i], yourtalents[i]);

	}

	printf("\nsizeof matalents: %zd, sizeof yourtalents: %zd\n",
		sizeof(mytalents), sizeof(yourtalents));


	return 0;

}

//output:Let's compare talents.
//My Talents                           Your Talents
//Adding numbers swiftly               Walking in a straight line
//Multiplying accurately               Sleeping
//Stashing data                        Watching television
//Following instructions to the letter Mailing letters
//Unsderstanding the C language        Reading email
//
//sizeof matalents: 40, sizeof yourtalents : 200
