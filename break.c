//break.c---ʹ��break�˳�ѭ��

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	float length = 0, width = 0;

	printf("Please enter the length of the rectangle:\n");

	while (scanf("%f", &length) == 1)
	{

		printf("Length = %0.2f:\n", length);

		printf("Please enter its width:\n");

		if (scanf("%f", &width) != 1)
		{

			break;

		}

		printf("Width = %0.2f\n", width);

		printf("Area = %0.2f:\n", length * width);

		printf("Please enter the length of the rectangle:\n");
	}

	printf("Done!\n");


	return 0;

}

//output:
//Please enter the length of the rectangle :
//2
//Length = 2.00 :
//	Please enter its width :
//3
//Width = 3.00
//Area = 6.00 :
//	Please enter the length of the rectangle :
//6
//Length = 6.00 :
//	Please enter its width :
//4
//Width = 4.00
//Area = 24.00 :
//	Please enter the length of the rectangle :
//a
//Done!