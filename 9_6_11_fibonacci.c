//fibonacci.c---使用循环计算斐波那契数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void Fibonacci(int n);//计算斐波那契数列的前n项，无返回值

int main()
{

	int n = 0;

	printf("Please enter the number of Fibonacci (q to quit): ");

	while (scanf("%d", &n) == 1)
	{

		if (n >= 2)
		{

			Fibonacci(n);

			printf("Please enter the number of Fibonacci (q to quit): ");

		}

	}



	return 0;

}

void Fibonacci(int n)
{

	unsigned long f1 = 0, f2 = 0, temp = 0;
	//考虑到斐波那契数列的增长速度，使用无符号长整型，能够显示更多数据项

	f1 = 1;

	f2 = 1;

	for (int i = 0; i < n; i++)
	{

		printf("%lu ", f1);

		//temp = f1;
		
		temp = f1 + f2;

		f1 = f2;

		f2 = temp;

	}

	printf("\n");


}
//
//output:
//Please enter the number of Fibonacci(q to quit) : 9
//111111111
//Please enter the number of Fibonacci(q to quit) : 3
//111
//Please enter the number of Fibonacci(q to quit) : 6
//111111
//Please enter the number of Fibonacci(q to quit) : q

//output1:
//Please enter the number of Fibonacci(q to quit) : 3
//1 1 2
//Please enter the number of Fibonacci(q to quit) : 4
//1 1 2 3
//Please enter the number of Fibonacci(q to quit) : 5
//1 1 2 3 5
//Please enter the number of Fibonacci(q to quit) : 6
//1 1 2 3 5 8
//Please enter the number of Fibonacci(q to quit) : 7
//1 1 2 3 5 8 13
//Please enter the number of Fibonacci(q to quit) : q
