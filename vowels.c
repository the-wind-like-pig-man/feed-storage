//vowels.c---使用多重标签

#include <stdio.h>

//int main()
//{
//
//	char ch = '0';
//
//	int a_ct = 0, e_ct = 0, i_ct = 0, o_ct = 0, u_ct = 0;
//
//	printf("Please enter some text; enter # to quit.\n");
//
//	while ((ch = getchar()) != '#')
//	{
//
//		switch (ch)
//		{
//
//		case 'a':
//
//		case 'A': a_ct++;
//
//			break;
//
//	/*	case 'i':
//
//		case 'I': i_ct ++;
//
//			break;*/
//
//		case 'e':
//
//		case 'E': e_ct++;
//
//			break;
//
//		case 'i':
//
//		case 'I': i_ct++;
//
//			break;
//
//		case 'o':
//
//		case 'O': o_ct++;
//
//			break;
//
//		case 'u':
//
//		case 'U': u_ct++;
//
//			break;
//
//		default: break;
//
//
//
//		}//switch结束
//
//			//while循环结束
//	}
//
//	printf("number of vowels:   A  E   I   O   U\n");
//
//	printf("                       %4d  %4d  %4d  %4d  %4d  %4d\n",
//		a_ct, e_ct, i_ct, o_ct, u_ct);
//
//
//
//	return 0;
//
//}

//output:
//Please enter some text; enter # to quit.
//a
//I see under the overseer.#
//number of vowels : A  E   I   O   U
//1     7     1     1     1  6619256

//output:
//Please enter some text; enter # to quit.
//I see under the overseer.#
//number of vowels : A  E   I   O   U
//0     7     1     1     1  6619256

int main()
{

	char ch = '0';

	int a_ct = 0, e_ct = 0, i_ct = 0, o_ct = 0, u_ct = 0;

	printf("Please enter some text; enter # to quit.\n");

	while ((ch = getchar()) != '#')
	{

		//ch = toupper(ch);

		switch (ch)
		{

			ch = toupper(ch);

		case 'a':

		case 'A': a_ct++;

			break;

			/*	case 'i':

				case 'I': i_ct ++;

					break;*/

		case 'e':

		case 'E': e_ct++;

			break;

		case 'i':

		case 'I': i_ct++;

			break;

		case 'o':

		case 'O': o_ct++;

			break;

		case 'u':

		case 'U': u_ct++;

			break;

		default: break;



		}//switch结束

			//while循环结束
	}

	printf("number of vowels:    A    E    I    O    U\n");

	printf("\n");

	printf("                 %4d  %4d  %4d  %4d  %4d\n",
		a_ct, e_ct, i_ct, o_ct, u_ct);



	return 0;

}
