//pound.c---定义一个带一个参数的函数

#include <stdio.h>

void pound(int n);//ANSI函数原型声明

int main()
{

	int times = 5;

	char ch = '!';//ASCII码是33

	float f = 6.0f;

	pound(times);//int类型的参数

	pound(ch);//和pound((int)ch);函数相同

	pound(f);//和pound((int)f);函数相同


	return 0;

}

void pound(int n)
{

	while (n-- > 0)
	{

		printf("#");

		//printf("\n");//竖着打印

	}

	printf("\n");//换行后横着打印

}

//output:
//#####---5
//#################################---33
//######---6