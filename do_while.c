#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	const int secret_code = 13;

	int code_entered = 0;

	do 
	{
	
		printf("To enter the triskaidekaphobia therapy club,\n");

		printf("please enter the secret code number: ");

		scanf("%d", &code_entered);
	
	} while (code_entered != secret_code);


	printf("Congratulations! You are cured!\n");


	return 0;

}

//output:
//To enter the triskaidekaphobia therapy club,
//please enter the secret code number : 10
//To enter the triskaidekaphobia therapy club,
//please enter the secret code number : 20
//To enter the triskaidekaphobia therapy club,
//please enter the secret code number : 40
//To enter the triskaidekaphobia therapy club,
//please enter the secret code number : 13
//Congratulations!You are cured!