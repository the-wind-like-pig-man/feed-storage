//exercises_7.c---把一个单词存入一个字符数组，并倒序打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <string.h>

int main()
{

	char word[30] = { 0 };

	printf("Please enter the words: ");

	scanf("%s", word);

	printf("The word you enter is : %s\n", word);

	printf("The reverse word you enter is : \n");

	printf("%d\n", strlen(word));

	for (int i = strlen(word) - 1; i >= 0; i--)
	{

		printf("%c", word[i]);


	}

	printf("\n");


	return 0;

}

//output:
//Please enter the words : hello
//The word you enter is : hello
//The reverse word you enter is :
//5
//olleh