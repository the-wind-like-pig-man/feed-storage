//exercises_4.c---输入身高，以厘米和英寸打印出来

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define FEET_TO_CM 30.48

#define INCH_TO_CM 2.54//预定义转换为单位明示常量

int main(int argc, char* argv[])
{

	int feet = 0;

	float inches = 0, cm = 0;

	printf("CONVERT CM TO INCHES!\n");

	printf("Please enter the height in centimeters: ");

	scanf("%f", &cm);//读取用户输入的数据

	while (cm > 0)
	{

		feet = cm / FEET_TO_CM;

		inches = (cm - feet * FEET_TO_CM) / INCH_TO_CM;//数据转换计算

		printf("%.1f cm = %d feet, %.1f inches\n", cm, feet, inches);//打印结果

		printf("Please enter the height in centimeters(<= 0 TO QUIT): ");

		scanf("%f", &cm);

	}

	printf("PROGRAM EXIT!\n");


	return 0;

}

//output:
//CONVERT CM TO INCHES!
//Please enter the height in centimeters : 168
//168 cm = 5 feet, 6 inches
//Please enter the height in centimeters(<= 0 TO QUIT) : 182
//182 cm = 5 feet, 12 inches
//Please enter the height in centimeters(<= 0 TO QUIT) : 168.7
//169 cm = 5 feet, 6 inches
//Please enter the height in centimeters(<= 0 TO QUIT) : 0
//PROGRAM EXIT!

//output:
//CONVERT CM TO INCHES!
//Please enter the height in centimeters : 182
//182.0 cm = 5 feet, 11.7 inches
//Please enter the height in centimeters(<= 0 TO QUIT) : 175
//175.0 cm = 5 feet, 8.9 inches
//Please enter the height in centimeters(<= 0 TO QUIT) : 0
//PROGRAM EXIT!
