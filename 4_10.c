﻿
#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//输入一个整数数组，实现一个函数
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有的偶数位于数组的后半部分

void move_odd_even(int arr[], int sz)
{

	int left = 0;//定义左下标

	int right = sz - 1;//定义右下标

	while (left < right)
	{
		//从左向右找一个偶数停下来

		while ((left < right) && (arr[left] % 2 == 1))
		{

			left++;

		}

		//从右向左找一个奇数，找到后停下来
		while ((left < right) && (arr[right] % 2 == 0))
		{

			right--;

		}

		//交换奇数和偶数

		if (left < right)
		{

			int tmp = arr[left];

			arr[left] = arr[right];

			arr[right] = tmp;

			left++;

			right--;

		}
	}


}

int main()
{
	int  arr[10] = { 0 };

	//输入内容

	int i = 0;

	int sz = sizeof(arr) / sizeof(arr[0]);

	for (i = 0; i < sz; i++)//越界？
	{

		scanf("%d ", &arr[i]);

		//scanf("%d ", arr + i);//输入两次？

	}

	//调整
	//1 2 3 4 5 6 7 8  9 10
	//可能是注释符的原因 
	move_odd_even(arr, sz);

	//输出

	for (i = 0; i < sz; i++)
	{


		printf("%d ", arr[i]);//1 9 3 7 5 6 4 8 2 10

	}


	return 0;

}