//3_6_exercises_6.c---输入夸脱，计算水分子数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define MASS_PER_MOLE 3.0e-32

#define MASS_PER_QUART 950//使用预编译指令，定义水分子质量，1夸脱水的质量

int main()
{

	float quart = 0, quantity = 0;

	printf("Please enter how many quarts: ");

	scanf("%f", &quart);

	quantity = quart * MASS_PER_QUART / MASS_PER_MOLE;//计算水分子数量

	printf("There are %e molecule.\n", quantity);



	return 0;

}

//output:
//Please enter how many quarts : 950
//There are 3.008333e+37 molecule.
