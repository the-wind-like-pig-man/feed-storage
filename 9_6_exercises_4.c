//exericses_4.c---求一个数的调和平均数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

double harmean(double x, double y);
//函数的声明

int main()
{

	double d1 = 0, d2 = 0;

	printf("Please enter two float numbers: ");

	scanf("%lf %lf", &d1, &d2);

	printf("The harmean of %g and %g is %g.\n", d1, d2, harmean(d1, d2));

	printf("Well Done!\n");

	return 0;

}

double harmean(double x, double y)
{

	return 2 / (1 / x + 1 / y);//计算两个数的调和平均数

}

//output:
//Please enter two float numbers : 2 5
//The harmean of 2 and 5 is 2.85714.

//output:
//Please enter two float numbers : 3 6
//The harmean of 3 and 6 is 4.
//Well Done!