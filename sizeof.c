//sizeof.c---使用sizeof运算符

//使用C99新增的%zd转换说明---如果编译器不支持%zd，请将其改成%u或%lu

#include <stdio.h>

int main()
{

	int n = 0;

	size_t intsize;

	intsize = sizeof(int);

	printf("n = %d, n has %zd bytes; all ints has %zd bytes.\n",
		n, sizeof n, intsize);


	return 0;

}

//output:
//n = 0, n has 4 bytes; all ints has 4 bytes.