//quit_chk.c---某程序的开始部分

#include <stdio.h>

#include <string.h>

#define SIZE 80

#define LIM 10

#define STOP "quit"

char* s_gets(char* st, int n);

int main()
{

	char input[LIM][SIZE];

	int ct = 0;

	printf("Please enter up to %d lines (type quit to quit):\n", LIM);

	while (ct < LIM && s_gets(input[ct], SIZE) != NULL &&
		strcmp(input[ct], STOP) != 0)
	{

		ct++;

	}

	printf("%d strings entered\n", ct);



	return 0;

}

char* s_gets(char* st, int n)
{

	char* ret_val;

	int i = 0;

	ret_val = fgets(st, n, stdin);

	if (ret_val)
	{

		while (st[i] != '\n' && st[i] != '\0')
		{

			i++;

		}

		if (st[i] == '\n')
		{

			st[i] = '\0';

		}

		else
		{

			while (getchar() != '\n')
			{

				continue;

			}

		}

	}

	return ret_val;

}

//output:
//Please enter up to 10 lines(type quit to quit) :
//	gant
//	bright light
//
//
//	good day
//	very good
//	just good girl
//
//	let's go
//
//	10 strings entered

//output1:
//Please enter up to 10 lines(type quit to quit) :
//	quackery
//	quasar
//	quilt
//	quotient
//	no more
//	no more doesn't with q
//	quiz strcpy strcmp
//	Here are the words accepted
//	Be the best that you can be
//	Be the beast
//	10 strings entered
