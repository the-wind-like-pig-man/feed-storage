﻿
#include <string.h>

#include <assert.h>

#include <stdio.h>

//int main()
//{
//
//	char* c[] = { "ENTER", "NEW", "POINT", "FIRST" };
//
//	//char** cp = { c + 3, c + 2,c + 1, c };//少了方括号[]
//
//	char** cp[] = { c + 3, c + 2,c + 1, c };
//
//
//	char*** cpp = cp;
//
//	printf("%s\n", **++cpp);//--->POINT
//
//	printf("%s\n", *-- * ++cpp + 3);//cpp的当前值承接上面printf()中的cpp的值
//	//--->ER
//
//	printf("%s\n", *cpp[-2] + 3);//cpp的当前值承接上面printf()中的cpp的值
//	//---*(*(cpp-2))--->ST
//
//	printf("%s\n", cpp[-1][-1] + 1);//cpp的当前值承接上面printf()中的cpp的值
//	//*(*(cpp-1))-1)+1--->EW
//
//	//输出：
//	//POINT
//	//ER
//	//ST
//	//EW
//
//
//	return 0;
//
//}

//
//int main()
//{
//
//	char arr[] = "abcdef";//abcdef\0
//
//	int len = strlen(arr);
//
//	printf("%d\n", len);//6
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//if (strlen("abc") - strlen("abcdef") > 0)//3-6=-3,转化为无符号整型，结果大于>
//
//	if(strlen("abc") > strlen("abcdef"))//<=
//	{
//
//		printf(">\n");
//
//	}
//
//	else
//	{
//
//		printf("<=\n");
//
//	}
//
//
//	return 0;
//
//
//}

//模拟实现更重要

//1.计数器方法
//2.指针-指针
//3.递归的方法

//size_t my_strlen(const char* str)
//{
//
//	size_t count = 0;
//
//	assert(str);
//
//	while(*str != '\0')
//	{
//
//		count++; 
//		
//		str++;
//
//	}
//
//	return count;
//
//}
//
//int main()
//{
//
//	char arr[] = "abcdef";
//
//	size_t n = my_strlen(arr);
//
//	printf("%u\n", n);//6
//
//
//	return 0;
//
//}