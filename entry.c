//entry.c---出口条件循环

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	const int secret_code = 13;

	int code_entered = 0;

	printf("To enter the triskaidekaphobia threapy club, \n");

	printf("please enter the secret code number: ");

	scanf("%d", &code_entered);

	while (code_entered != secret_code)
	{

		printf("To enter the triakaphobia therapy clud, \n");

		printf("please enter the secret code number: ");

		scanf("%d", &code_entered);

	}

	printf("Congratulations! You are cured!\n");


	return 0;

}

//output:
//To enter the triskaidekaphobia threapy club,
//please enter the secret code number : 22
//To enter the triakaphobia therapy clud,
//please enter the secret code number : 33
//To enter the triakaphobia therapy clud,
//please enter the secret code number : 11
//To enter the triakaphobia therapy clud,
//please enter the secret code number : 133
//To enter the triakaphobia therapy clud,
//please enter the secret code number : 13
//Congratulations!You are cured!