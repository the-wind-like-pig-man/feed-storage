//4_5exercises.c---读取一个浮点数，并以小数和指数打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	float input = 0;

	printf("Please enter a float number: ");

	scanf("%f", &input);

	//printf("The input is %.lf or %.le \n", input, input);

	//printf("The input is %.1f or %.1e \n", input, input);

	printf("The input is %+.3f or %.3e\n", input, input);

	//转换说明符%f和%e分别表示以普通浮点数和科学计数法格式显示
	//如果不需要指定小数点位和字符宽度，可以直接使用如下代码
	//printf("The input is %f or %e \n", input, input);
    //printf("The input is %+.3f or %.3e\n", input, input);


	return 0;

}

//output:
//Please enter a float number : 21.3
//The input is 21 or 2e+01

//output:
//Please enter a float number : 21.3
//The input is 21.3 or 2.1e+01
//
//output:
//Please enter a float number : 21.3
//The input is + 21.300 or 2.130e+01
