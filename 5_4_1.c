﻿


#include <stdio.h>

#include <string.h>

#include <stdlib.h>

// int cmp_int(const void* e1, const void* e2)
//{
//
//	 //return (*(int*)e1 - *(int*)e2);//升序---两个return语句，谁在前面谁有效
//
//	 return (*(int*)e2 - *(int*)e1);//降序
//
//
//}


int cmp_int(const void* e1, const void* e2)
{

	return (*(int*)e1 - *(int*)e2);

}

//void bubble_sort(int arr[], int sz)
//{
//
//	int i = 0;
//
//	//冒泡的趟数
//
//	for (i = 0; i < sz - 1; i++)
//	{
//
//		int flag = 1;//假设数组是排序好的
//		//一趟冒泡排序的过程
//
//		int j = 0;
//
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//
//			if (arr[j] > arr[j + 1])
//			{
//
//				int tmp = arr[j];
//
//				arr[j] = arr[j + 1];
//
//				arr[j + 1] = tmp;
//
//				flag = 0;
//
//			}
//
//		}
//
//		if (flag == 1)//若是排序好的，不再进行冒泡排序
//		{
//
//			break;
//
//		}
//
//	}
//
//}


//void test3()
//{
//
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//
//	//0,1,2,3,4,5,6,7,8,9---升序排列
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//
//	}
//
//}

void Swap(char* buf1, char* buf2, int width)
{

	int i = 0;

	for (i = 0; i < width; i++)
	{

		char tmp = *buf1;

		*buf1 = *buf2;

		*buf2 = tmp;

		buf1++;

		buf2++;

	}

}

//将冒泡排序改造成可以排序任意类型的排序函数

//void bubble_sort(void* base,int sz,int width,int(*cmp)(const void* e1,const void* e2))//返回类型不能是void，应是int

void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))
{

	int i = 0;

	//冒泡的趟数

	for (i = 0; i < sz - 1; i++)
	{

		int flag = 1;//假设数组是排序好的
		//一趟冒泡排序的过程

		int j = 0;

		for (j = 0; j < sz - 1 - i; j++)
		{

			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				//进行交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);

				flag = 0;

			}

		}

		if (flag == 1)//若是排序好的，不再进行冒泡排序
		{

			break;

		}

	}

}


struct Stu//定义结构体数据
{

	char name[20];

	int age;

};


void test2()
{


	struct Stu s[] = {{"zhangsan", 20}, {"lisi", 30}, {"wangeu",25}};

	int sz = sizeof(s) / sizeof(s[0]);

	bubble_sort(s, sz, sizeof(s[0]), cmp_stu_by_age);

	//bubble_sort(s, sz, sizeof(s[0]), cmp_stu_by_name);




}

// int cmp_stu_by_name(const void* e1, const void* e2)
//{
//
//	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
//
//}

int cmp_stu_by_age(const void* e1, const void* e2)
{

	return (((struct Stu*)e1)->age - ((struct Stu*)e2)->age);

}


int main()
{

	test2();

	//test3();

	return 0;

}
