//put1.c---打印字符串，不添加\n

#include <stdio.h>

void put1(const char* string);

int main()
{

	const char* string = " I am a student.";

	//string = getchar();

	//void put1(const char* string);//不能正常调用函数

	put1(string);//可以调用函数

	return 0;

}

void put1(const char* string)
{

	while (*string != '\0')
	{

		putchar(*string++);

	}

}