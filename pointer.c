﻿


#include <stdio.h>

//int main()
//{
//
//	int a = 10;//a是整型变量，占用4个字节的内存空间
//
//	int* pa = &a;
//	//pa是一个指针变量，用来存放地址的
//
//	//pa
//	//
//	//本质上指针就是地址
//	//口语中说的指针，其实就是指针变量。指针变量就是一个变量，它是用来存放地址的一个变量。
//	//
//
//	return 0;
//
//}

//
//int main()
//{
//
//	char* pc = NULL;
//
//	short* ps = NULL;
//
//	int* pi = NULL;
//
//	double* pd = NULL;
//
//	//ptr_t_pt = NULL;
//	//x64---64位环境
//	// 
//	//sizeof返回值的类型是无符号整型---unsigned int
//	printf("%zu\n", sizeof(pc));//8个字节
//
//	printf("%zu\n", sizeof(ps));//8个字节
//	
//	printf("%zu\n", sizeof(pi));//8个字节
//
//	printf("%zu\n", sizeof(pd));//8个字节
//
//
//	return 0;
//
//}


//int main()
//{
//
//	int a = 0x11223344;
//
//	//int* pa = &a;
//
//	//*pa = 0;//访问4个字节
//
//	char* pc = (char*)&a;//int
//
//	*pc = 0;//访问1个字节
//
//	//结论：指针类型决定了指针在解引用的时候访问几个字节
//	//如果是int型的指针，解引用访问4个字节
//	//如果是char类型的指针，解引用访问1个字节
//	//此结论可以推广到其它类型
//	//
//	//
//	return 0;
//
//}