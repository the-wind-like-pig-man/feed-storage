//three_double.c---编写一个函数，以三个double变量的地址作为参数，
//把最小的放入第一个变量中，中间值方第二个变量中，最大值放第三个变量中 

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void ordering(double* x, double* y, double* z);
//ordering()函数的参数是3个指向double类型变量的指针，无返回值

int main()
{

	double d1 = 0, d2 = 0, d3 = 0;

	printf("Please enter three float numbers: ");

	scanf("%lf %lf %lf", &d1, &d2, &d3);

	printf("Data you input are %g , %g and %g.\n", d1, d2, d3);

	ordering(&d1, &d2, &d3);

	printf("After fuction, data are %g %g and %g.\n", d1, d2, d3);

	printf("Well Done!\n");


	return 0;

}


void ordering(double* x, double* y, double* z)
{

	double temp = 0;

	if (*x > *y)
	{

		temp = *x;

		*x = *y;

		*y = temp;

	}


	if (*x > *z)
	{

		temp = *x;

		*x = *z;

		*z = temp;

	}

	if(*y > *z)
	{ 
	
		temp = *y;

		*y = *z;

		*z = temp;
	
	}
	//ordering()函数的基本算法是，对三个变量进行两两比较，符合条件就交换数值
}

//output:
//Please enter three float numbers : 2.0 3.1 4.5
//Data you input are 2, 3.1 and 4.5.
//After fuction, data are 2 3.1 and 4.5.


//output:
//Please enter three float numbers : 1.2 3.6 7.2
//Data you input are 1.2, 3.6 and 7.2.
//After fuction, data are 1.2 3.6 and 7.2.
//Well Done!