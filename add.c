﻿//#define _CRT_SECURE_NO_WARNINGS 0

//全局变量---全局变量具有外部链接属性

//static int g_val = 2022;//static修饰全局变量的时候，这个全局变量的外部链接属性，就变成了内部链接属性。其他源文件（.c文件）就不能在使用到这个全局变量了
//int g_val = 2022;

//static int Add(int x, int y)//一个函数本来是具有外部连接属性的，但被static修饰的时候，外部连接属性就变成了内部链接属性，其他源文件
//（.c）就无法使用它了

// int Add(int x, int y)
//{
//	return x + y;
//}