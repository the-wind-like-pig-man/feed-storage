﻿
#define  _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>


//函数指针的用途

//实现一个计算器：加法、减法、乘法、除法

//void menu()
//{
//
//
//	printf("*********************************\n");
//
//
//	printf("******  1.Add      2.Sub   ******\n");
//
//
//	printf("******  3.Mul      4.Div   ******\n");
//
//
//	printf("******        0.Exit       ******\n");
//
//
//	printf("*********************************\n");
//
//
//}
//
//int Add(int x, int y)
//{
//
//	return x + y;
//
//}
//
//int Sub(int x, int y)
//{
//
//	return x - y;
//
//}
//
//int Mul(int x, int y)
//{
//
//	return x * y;
//
//}
//
//int Div(int x, int y)
//{
//
//	return x / y;
//
//}
//
//
//int main()
//{
//	int input = 0;
//
//	int x = 0;
//
//	int y = 0;
//
//	int ret = 0;
//
//	do
//	{
//
//		menu();
//
//		printf("Please choose:>");
//
//		scanf("%d", &input);
//
//		//printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//		//scanf("%d %d", &x, &y);
//
//		switch (input)
//		{
//
//		case 1:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Add(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 2:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Sub(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 3:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Mul(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 4:
//
//			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//			scanf("%d %d", &x, &y);
//
//			ret = Div(x, y);//函数体部分冗余
//
//			printf("%d\n", ret);
//
//			break;
//
//		case 0:
//
//			printf("Exit!\n");
//
//			break;
//
//		default:
//
//			printf("Choose wrong!\n");
//
//			break;
//
//
//		}
//
//
//	} while (input);
//
//
//	return 0;
//
//}

//void menu()
//{
//
//
//	printf("*********************************\n");
//
//
//	printf("******  1.Add      2.Sub   ******\n");
//
//
//	printf("******  3.Mul      4.Div   ******\n");
//
//
//	printf("******        0.Exit       ******\n");
//
//
//	printf("*********************************\n");
//
//
//}
//
//int Add(int x, int y)
//{
//
//	return x + y;
//
//}
//
//int Sub(int x, int y)
//{
//
//	return x - y;
//
//}
//
//int Mul(int x, int y)
//{
//
//	return x * y;
//
//}
//
//int Div(int x, int y)
//{
//
//	return x / y;
//
//}
//
////计算
////回调函数---函数自己调用自己
//
//void calc(int(*pf)(int x, int y))//封装calc---计算器函数，利用函数指针传递加、减、乘、除函数地址
//{
//
//	int x = 0;
//
//	int y = 0;
//
//	int ret = 0;
//
//	printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//	scanf("%d %d", &x, &y);
//
//	ret = pf(x, y);//消除函数体部分冗余
//
//	printf("%d\n", ret);
//
//}
//
//int main()
//{
//	int input = 0;
//
//	
//
//	do
//	{
//
//		menu();
//
//		printf("Please choose:>");
//
//		scanf("%d", &input);
//
//		//printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug
//
//		//scanf("%d %d", &x, &y);
//
//		switch (input)
//		{
//
//		case 1:
//
//			calc(Add);
//
//			break;
//
//		case 2:
//
//			calc(Sub);
//
//			break;
//
//		case 3:
//
//			calc(Mul);
//
//			break;
//
//		case 4:
//
//			calc(Div);
//
//			break;
//
//		case 0:
//
//			printf("Exit!\n");
//
//			break;
//
//		default:
//
//			printf("Choose wrong!\n");
//
//			break;
//
//
//		}
//
//
//	} while (input);
//
//
//	return 0;
//
//}


//int Add(int x, int y)
//{
//
//	return x + y;
//
//}
//
//int Sub(int x, int y)
//{
//
//	return x - y;
//
//}
//
//int Mul(int x, int y)
//{
//
//	return x * y;
//
//}
//
//int Div(int x, int y)
//{
//
//	return x / y;
//
//}
//
//int main()
//{
//
//	int (*pf)(int, int) = Add;//pf是函数指针
//
//	int (*arr[4])(int, int) = { Add, Sub, Mul, Div };//arr是函数指针数组
//
//	int i = 0;
//
//	for (i = 0; i < 4; i++)
//	{
//
//		int ret = arr[i](8, 4);
//
//		printf("%d \n", ret);
//
//	}
//
//
//	return 0;
//
//}

//实现一个计算器：加法、减法、乘法、除法

void menu()
{


	printf("*********************************\n");


	printf("******  1.Add      2.Sub   ******\n");


	printf("******  3.Mul      4.Div   ******\n");


	printf("******        0.Exit       ******\n");


	printf("*********************************\n");


}

int Add(int x, int y)
{

	return x + y;

}

int Sub(int x, int y)
{

	return x - y;

}

int Mul(int x, int y)
{

	return x * y;

}

int Div(int x, int y)
{

	return x / y;

}


int main()
{
	int input = 0;

	int x = 0;

	int y = 0;

	int ret = 0;

	//函数指针的数组
	//又叫转移表，当增加函数功能时，可以大大节省修改代码的工作量
	int (*pfArr[])(int, int) = { 0, Add, Sub, Mul, Div };//方便增加函数功能

	do
	{

		menu();

		printf("Please choose:>");

		scanf("%d", &input);

		//printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

		//scanf("%d %d", &x, &y);

		if (input == 0)
		{

			printf("Exit!\n");

		}

		else if (input >= 1 && input <= 4)
		{

			printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

					scanf("%d %d", &x, &y);

					ret = pfArr[input](x, y);//消除函数体冗余部分

					printf("%d\n", ret);

		}

		else
		{

			printf("Choose wrong!\n");

		}


	//	switch (input)
	//	{

	//	case 1:

	//		printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

	//		scanf("%d %d", &x, &y);

	//		ret = Add(x, y);//函数体部分冗余

	//		printf("%d\n", ret);

	//		break;

	//	case 2:

	//		printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

	//		scanf("%d %d", &x, &y);

	//		ret = Sub(x, y);//函数体部分冗余

	//		printf("%d\n", ret);

	//		break;

	//	case 3:

	//		printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

	//		scanf("%d %d", &x, &y);

	//		ret = Mul(x, y);//函数体部分冗余

	//		printf("%d\n", ret);

	//		break;

	//	case 4:

	//		printf("Please input two numbers:>");//放在switch外侧选择1，2，3，4，0之外的数字时出现bug

	//		scanf("%d %d", &x, &y);

	//		ret = Div(x, y);//函数体部分冗余

	//		printf("%d\n", ret);

	//		break;

	//	case 0:

	//		printf("Exit!\n");

	//		break;

	//	default:

	//		printf("Choose wrong!\n");

	//		break;


	//	}


	} while (input);


	return 0;

}