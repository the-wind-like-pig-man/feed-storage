#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int Temperatures(double fahrenheit);

int main(int argc, char* argv[])
{

	double input = 0;

	printf("This program convert fahrenheit to celsius and kelvin.\n");

	printf("Please enter a fahrenheit to start: ");

	while (scanf("%lf", &input) == 1)
	{

		Temperatures(input);

		printf("Please enter next fahrenheit!(q to quit): ");

	}

	printf("Done!\n");



	return 0;

}

int Temperatures(double fahrenheit) 
{

	const double F_TO_C = 32.0;

	const double C_TO_K = 273.16;

	double celsius = 0, kelvin = 0;

	celsius = 5.0 / 9.0 * (fahrenheit - F_TO_C);

	kelvin = celsius + C_TO_K;

	printf("%.2f. fahrenheit, equal %.2f celsuis, and %.2f kelvin\n", fahrenheit, celsius, kelvin);

	return 0;


}

//output:
//This program convert fahrenheit to celsius and kelvin.
//Please enter a fahrenheit to start : 120
//120.00.fahrenheit, equal 48.89 celsuis, and 322.05 kelvin
//Please enter next fahrenheit!(q to quit) : 300
//300.00.fahrenheit, equal 148.89 celsuis, and 422.05 kelvin
//Please enter next fahrenheit!(q to quit) : 250
//250.00.fahrenheit, equal 121.11 celsuis, and 394.27 kelvin
//Please enter next fahrenheit!(q to quit) : 0
//0.00.fahrenheit, equal - 17.78 celsuis, and 255.38 kelvin
//Please enter next fahrenheit!(q to quit) : q
//Done!
