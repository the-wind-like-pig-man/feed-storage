﻿
#include <stdio.h>

//void bubble_sort(int arr[], int sz)
//{
//
//	int i = 0;
//
//	//冒泡的趟数
//
//	for (i = 0; i < sz - 1; i++)
//	{
//
//		int flag = 1;//假设数组是排序好的
//		//一趟冒泡排序的过程
//
//		int j = 0;
//
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//
//			if (arr[j] > arr[j + 1])
//			{
//
//				int tmp = arr[j];
//
//				arr[j] = arr[j + 1];
//
//				arr[j + 1] = tmp;
//
//				flag = 0;
//
//			}
//
//		}
//
//		if (flag == 1)//若是排序好的，不再进行冒泡排序
//		{
//
//			break;
//
//		}
//
//	}
//
//}
//
//int main()
//{
//
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//
//	//0,1,2,3,4,5,6,7,8,9---升序排列
//	//把数组按升序排列
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz);
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//
//	}
//
//
//
//	return 0;
//
//}

//qsort---库函数，使用快速排序的思想实现排序的一个函数，这个函数可以排序任意类型的数据

//不同数据的比较排序有差异，方法不同

void qsort(void* base,//待排序的数据的起始位置
	       
	       size_t num,//待排序的数据元素的个数

	       size_t width,//待排序的数据元素的大小（单位是字节）
	      
	       int(* cmp)(const void* e1, const void* e2)//函数指针---比较函数


);