//exercises_11.c---设计程序计算ABC杂货店的邮购费用

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define PRICE_ARTI 2.05

#define PRICE_BEET 1.15

#define PRICE_CARROT 1.09

#define DISCOUNT 0.05
//常量定义

void show_menu(void);

float get_weight(void);
//函数声明

int main(void)
{

	float w_arti = 0;

	float w_beet = 0;

	float w_carrot = 0;

	char selected = 0;

	float weight = 0, amount = 0, rebate = 0, freight = 0, total = 0;

	do {
	
		show_menu();

		scanf("%c", &selected);

		switch (selected) 
		{

		case 'a':

			w_arti += get_weight();

			break;

		case 'b':

			w_beet += get_weight();

			break;

		case 'c':

			w_carrot += get_weight();

			break;

		case 'q':

			break;

		default:

			printf("Error input, retry!\n");


		}
	

	
	} while (selected != 'q');//获取所有订购货物的数量

	amount = w_arti * PRICE_ARTI + w_beet * PRICE_BEET + w_carrot * PRICE_CARROT;

	weight = w_arti + w_beet + w_carrot;
	//计算金额和货物重量

	if (amount > 100)
	{

		rebate = amount * DISCOUNT;

	}

	else

		rebate = 0;
	//依据货物金额计算折扣

	if (weight <= 5)
	{

		freight = 6.5;

	}

	else if (weight > 5 && weight <= 20)
	{

		freight = 14;

	}

	else
	{

		freight = 14 + (weight - 20) * 0.5;
		//依据条件计算邮费

	}

	total = amount + freight - rebate;

	printf("The price of vegetable:\nartichoke %g$/pound, beet %g$/pound, carrot %g$/pound.\n", 
		PRICE_ARTI, PRICE_BEET, PRICE_CARROT);

	printf("Your order %g pound artichoke, %g pound beet, %g pound carrot.\n",
		w_arti, w_beet, w_carrot);

	printf("Your total order is %g pounds, discount %g$, amount %g$, freight %g$, total %g$.\n",
		weight, rebate, amount, freight, total);

	printf("Well Done!\n");


	return 0;

}

void show_menu()
{
//显示订购选择菜单

	printf("**********************************************************************************\n");

	printf("Please enter the char corresponding to the desired vegetable.\n");

	printf("a) artichoke                               b) beet\n");

	printf("c) carrot                                  q) quit\n");

	printf("**********************************************************************************\n");

	printf("Please input the vegetable you want to buy(a, b, c, or q for quit): ");


}

float get_weight(void)
{
	//读取用户输入的购买数量

	float weight = 0;

	printf("Please input how many pounds you buy: ");

	scanf("%f", &weight);

	printf("OK, add %g pount to cart.\n", weight);

	getchar();

	return weight;


}
//
//output:
//**********************************************************************************
//Please enter the char corresponding to the desired vegetable.
//a) artichoke                               b) beet
//c) carrot                                  q) quit
//**********************************************************************************
//Please input the vegetable you want to buy(a, b, c, or q for quit) : b
//Please input how many pounds you buy : 4
//OK, add 4 pount to cart.
//* *********************************************************************************
//Please enter the char corresponding to the desired vegetable.
//a) artichoke                               b) beet
//c) carrot                                  q) quit
//**********************************************************************************
//Please input the vegetable you want to buy(a, b, c, or q for quit) : b
//Please input how many pounds you buy : 5
//OK, add 5 pount to cart.
//* *********************************************************************************
//Please enter the char corresponding to the desired vegetable.
//a) artichoke                               b) beet
//c) carrot                                  q) quit
//**********************************************************************************
//Please input the vegetable you want to buy(a, b, c, or q for quit) : a
//Please input how many pounds you buy : 5
//OK, add 5 pount to cart.
//* *********************************************************************************
//Please enter the char corresponding to the desired vegetable.
//a) artichoke                               b) beet
//c) carrot                                  q) quit
//**********************************************************************************
//Please input the vegetable you want to buy(a, b, c, or q for quit) : c
//Please input how many pounds you buy : 5
//OK, add 5 pount to cart.
//* *********************************************************************************
//Please enter the char corresponding to the desired vegetable.
//a) artichoke                               b) beet
//c) carrot                                  q) quit
//**********************************************************************************
//Please input the vegetable you want to buy(a, b, c, or q for quit) : q
//The price of vegetable :
//artichoke 2.05$ / pound, beet 1.15$ / pound, carrot 1.09$ / pound.
//Your order 5 pound artichoke, 9 pound beet, 5 pound carrot.
//Your total order is 19 pounds, discount 0$, amount 26.05$, freight 14$, total 40.05$.
//Well Done!