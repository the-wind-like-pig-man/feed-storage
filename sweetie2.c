//sweetie2.c---使用for循环的计数循环

#include <stdio.h>

//int main()
//{
//
//	const int NUMBER = 22;
//
//		int count = 0;
//
//	for (count = 1; count <= NUMBER; count++);
//
//	printf("Be my Valentine!\n");
//
//
//
//	return 0;
//
//}

//output:
//Be my Valentine!


int main()
{

	const int NUMBER = 22;

	int count = 0;

	//for (count = 1; count <= NUMBER; count++);

	for (count = 1; count <= NUMBER; count++)

	{
	
		printf("Be my Valentine!\n");


	}


	return 0;

}

//output:
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!
//Be my Valentine!