//compback.c---strcmp()�ķ���ֵ

#include <stdio.h>

#include <string.h>

int main()
{

	printf("strcmp(\"A\", \"A\") is");

	printf("%d\n", strcmp("A", "A"));


	printf("strcmp(\"A\", \"B\") is ");
	
	printf("%d\n", strcmp("A", "B"));


	printf("strcmp(\"B\", \"A\") is ");

	printf("%d\n", strcmp("B", "A"));


	printf("strcmp(\"C\", \"A\") is ");

	printf("%d\n", strcmp("C", "A"));


	printf("strcmp(\"Z\", \"A\") is ");

	printf("%d\n", strcmp("Z", "A"));


	printf("strcmp(\"apples\", \"apple\") is ");

	printf("%d\n", strcmp("apples", "apple"));


	printf("\n");

	printf("Well Done!Goodbye!\n");


	return 0;

}

//output:
//strcmp("A", "A") is0
//strcmp("A", "B") is - 1
//strcmp("B", "A") is 1
//strcmp("C", "A") is 1
//strcmp("Z", "A") is 1
//strcmp("apples", "apple") is 1

//output1:
//strcmp("A", "A") is0
//strcmp("A", "B") is - 1
//strcmp("B", "A") is 1
//strcmp("C", "A") is 1
//strcmp("Z", "A") is 1
//strcmp("apples", "apple") is 1
//
//Well Done!Goodbye!
