//3_7_exercises_8.c---以不同单位显示等价容量

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define PINT_CUP 2

#define CUP_OUNCE 8

#define OUNCE_SPOON 2

#define SPOON_TEA 3//用预编译命令进行进制转换的明示常量定义

int main()
{

	float pint = 0, cup = 0, ounce = 0, spoon = 0, tea_spoon = 0;

	printf("Enter how many cup : ");

	scanf("%f", &cup);

	pint = cup / PINT_CUP;

	ounce = cup * CUP_OUNCE;

	spoon = ounce * OUNCE_SPOON;

	tea_spoon = spoon * SPOON_TEA;//进行进制转换计算

	printf("%.1f cup equals %.1f pint, %1f ounce ,%.1f spoon, %.1f tea_spoon.\n",
		cup, pint, ounce, spoon, tea_spoon);


	return 0;

}

//output:
//Enter how many cup : 2
//2.0 cup equals 1.0 pint, 16.000000 ounce, 32.0 spoon, 96.0 tea_spoon.

//output:
//Enter how many cup : 5
//5.0 cup equals 2.5 pint, 40.000000 ounce, 80.0 spoon, 240.0 tea_spoon.
