//9_6_1min.c---设计一个函数，返回较小的

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

double min(double x, double y);//函数声明，返回两个double类型数中的较小数据，因此，返回值也是double类型

int main()
{

	double d1 = 0, d2 = 0;

	printf("Please enter two float numbers: ");

	scanf("%lf %lf", &d1, &d2);

	//printf("You input %g and %g.The MIN is %g.", d1, d2, min(d1, d2));

	printf("You input %f and %f.The MIN is %f.", d1, d2, min(d1, d2));


	return 0;

}

double min(double x, double y)
{

	if (x < y)
	{

		return x;

	}

	else
	{

		return y;

	}
	//对于简单的逻辑判断函数，除使用if语句外，还常用问号表达式
	//这样表达式更加简洁和清晰
	//return x < y ? x : y; 
}


//output：
//Please enter two float numbers : 3.0 3.1
//You input 3 and 3.1.The MIN is 3.
//
//output2:
//Please enter two float numbers : 3.1 6.5
//You input 3.100000 and 6.500000.The MIN is 3.100000.