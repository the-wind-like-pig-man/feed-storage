﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <assert.h>

#include <string.h>

#include <stdio.h>

//int my_strcmp(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//    while (*str1 == *str2)
//     {
//	   if (*str1 == '\0')
//		return 0;//二者相等
//
//	   str1++;
//
//	   str2++;
//
//     }
//
//    if (*str1 > *str2)
//
//          return 1;
//
//    else
//
//          return -1;
//
//}

//int my_strcmp(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;//二者相等
//
//		str1++;
//
//		str2++;
//
//	}
//
//
//		return (*str1 - *str2);//简化代码
//
//
//}
//
//
//int main()
//
//{
//	/*char arr1[20] = "zhangsan";
//
//	char arr2[] = "zhangsanfeng";*/
//	//两个字符串比较相等，应该使用strcmp
//	//
//
//	char arr1[] = "abcdefg";
//
//	char arr2[] = "abcdef";
//
//
//	//char arr2[] = "abc";
//
//	/*int arr1[] = "abcdef";
//
//	int arr2[] = "abc";*/
//
//	//两个字符串比较，应该用strcmp
//
//	int ret = my_strcmp(arr1, arr2);
//
//	//int ret = strcmp(arr1, arr2);
//	//比较一下两个字符串是否相等
//	//arr1是数组名，数组名是数组首元素的地址 
//	//arr2是数组名，数组名是数组首元素的地址
//
//	/*if (arr1 == arr2)
//	{
//
//		printf("==\n");
//
//	}
//
//	else
//	{
//
//		printf("!=\n");
//
//	}*/
//
//	if (ret < 0)
//	{
//
//		printf("<\n");
//
//
//	}
//
//	else if (ret == 0)
//	{
//
//		printf("==\n");
//
//	}
//
//	else
//	{
//
//		printf(">\n");
//
//	}
//
//
//
//	return 0;
//
//}

//长度受限制的字符串函数
//strncmp
//strncat 
//strncpy

//int main()
//{
//
//	char arr1[20] = "abcdef";
//
//	char arr2[] = "bit";
//
//	//strcpy(arr1, arr2);
//
//	strncpy(arr1, arr2, 5);
//
//	printf("%s\n", arr1);
//
//	printf("%s\n", arr1);
//
//	return 0;
//
//}


//int main()
//{
//
//	char arr1[20] = "hello\0xxxxxx";
//
//	char arr2[] = "bit";
//
//	strncat(arr1, arr2, 6);
//
//	printf("%s\n", arr1);
//
//
//	return 0;
//
//}

//int main()
//{
//
//	char arr1[] = "abcdef";
//
//	char arr2[] = "abcd";
//
//	int ret = strncmp(arr1, arr2, 4);
//
//	printf("%d\n", ret);
//
//	if (ret == 0)
//	{
//
//		printf("==\n");
//
//	}
//
//	else if (ret < 0)
//	{
//
//		printf("<\n");
//
//	}
//
//	else
//
//		printf(">\n");
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//char email[] = "zpw@bitjiuyeke.com";
//
//	char email[] = "zpw@bitjiuyek.com";
//
//
//	char substr[] = "bitjiuyeke";
//
//	char* ret = strstr(email, substr);
//
//	if (ret == NULL)
//	{
//
//		printf("The sub string doesn't exist");
//
//	}
//
//	else
//	{
//
//		printf("%s\n", ret);
//
//	}
//
//	return 0;
//
//}

//KMP算法
//这个算法也是用来实现一个字符串中查找子字符串的
//效率高，但是实现难度大
//B站：比特大博哥

//char* my_strstr(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//	const char* s1 = str1;//固定str1的初始位置不变
//
//	const char* s2 = str2;//固定str2的初始位置不变
//
//	const char* p = str1;//初始化相等的位置
//
//	while (*p)
//	{
//
//		s1 = p;
//
//		s2 = str2;
//
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//
//			s1++;
//
//			s2++;
//
//		}
//
//		if (*s2 == '\0')
//		{
//
//			//return p;
//
//			return (char*)p;
//
//
//		}
//
//		p++;
//	}
//
//	return NULL;
//
//
//}
//
//int main()
//{
//
//	//char email[] = "zpw@bitjiuyeke.com";
//
//	/*char email[] = "zpw@bitjiuyeke.com";
//
//	char substr[] = "bitjiuyeke";*/
//
//	/*char* ret = my_strstr(email, substr);*/
//
//	char arr1[] = "abcdef";
//
//	char arr2[] = "bcdef";
//
//	char* ret = my_strstr(arr1, arr2);
//
//	if (ret == NULL)
//	{
//
//		printf("The sub string doesn't exist");
//
//	}
//
//	else
//	{
//
//		printf("%s\n", ret);
//
//	}
//
//	return 0;
//
//}


//strtok
//切割字符串
//