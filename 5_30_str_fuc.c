﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <assert.h>

#include <string.h>

#include <stdio.h>

#include <errno.h>




//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//
//	assert(dest && src);
//
//	void* ret = dest;
//
//	while (num--)
//	{
//
//		*(char*)dest = *(char*)src;
//
//		dest = (char*)dest + 1;
//
//		src = (char*)src + 1;
//
//	}
//
//	return ret;
//
//}
//
////memcpy---负责拷贝两块独立空间中的数据
////重叠内存的拷贝，是怎么做的呢？memove()函数
////
//
//int main()
//{
//
//	int arr1[] = { 1, 2, 3, 4, 5, 6, 7 };
//
//	//int arr2[] = { 0 };
//
//	int arr2[10] = { 0 };
//
//
//	//memcpy(arr2, arr1, 28);
//
//	my_memcpy(arr2, arr1, 28);
//
//	/*float arr3[] = { 1, 2, 3, 4, 5, 6, 7 };
//
//	float arr4[] = { 0 };
//
//	memcpy(arr4, arr3, 20);*/
//
//
//
//	return 0;
//
//}


//void* my_memcpy(void* dest, const void* src, size_t num)//自己编的my_memcpy()函数不宜实现重复内存之间的拷贝
//{
//
//	assert(dest && src);
//
//	void* ret = dest;
//
//	while (num--)
//	{
//
//		*(char*)dest = *(char*)src;
//
//		dest = (char*)dest + 1;
//
//		src = (char*)src + 1;
//
//	}
//
//	return ret;
//
//}
////memcpy()函数不能用来处理重叠的内存之间的数据拷贝的
////需要用memmove()函数来实现重叠内存之间的数据拷贝
//// 
//
////void test1()
////{
////
////	int arr1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
////
////	int arr2[20] = { 0 };
////
////	my_memcpy((char*)arr2, (char*)arr1, 20);
////
////	int i = 0;
////
////	for (i = 0; i < 5; i++)
////	{
////
////		printf("%d ", arr2[i]);
////
////	}
////
////}
////
//
//void test2()
//{
//
//	int arr1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//
//	//int arr2[20] = { 0 };
//
//	//my_memcpy(arr1 + 2, arr1, 20);//输出：1 2 1 2 1 2 1 8 9 10
//	////不是想要的结果
//
//	memmove(arr1 + 2, arr1, 20);//输出想要的结果：1 2 1 2 3 4 5 8 9 10
//
//	int i = 0;
//
//	for (i = 0; i < 10; i++)
//	{
//
//		printf("%d ", arr1[i]);
//		
//
//	}
//
//}
//
//
//int main()
//{
//
//
//	//test1();
//
//	test2();
//
//	return 0;
//
//}

