﻿#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int arr[10] = { 0 };
//
//	char ch[10] = { 0 };
//
//	printf("%d\n", sizeof(ch));
//
//	printf("%d\n", sizeof(arr));
//
//	return 0;
//}

//int main()
//{
//
//	//int a = 0;
//
//	////~是按二进制位取反
//	////00000000000000000000000000000000 ---补码
//	////11111111111111111111111111111111 ---~a
//	////11111111111111111111111111111110 --->反码
//	////10000000000000000000000000000001 --->原码，即-1
//	//// 
//	//// 
//	////
//
//	//printf("%d\n", ~a);
//
//	//int a = 3;
//
//	////0000000000000000000000000000000000000011
//	////1111111111111111111111111111111111111100 --->~a
//	////1111111111111111111111111111111111111011 --->补码
//	////1000000000000000000000000000000000000100 --->原码 -4
//
//	//printf("%d\n", ~a);
//
//
//	int a = 13;
//
//	//a |= (1 << 1);
//
//	//printf("%d\n", a);//a=15
//
//	//0000000000000000000000000000000000001101
//	//0000000000000000000000000000000000000010
//	//1<<1,1向左移动1位
//	//0000000000000000000000000000000000001111
//
//	a |= (1 << 4);
//
//	printf("%d\n", a);//a=29
//
//	a &= (~(1 << 4));
//
//	printf("%d\n", a);//a=13
//	//000000000000000000000000000000000011101
//	//111111111111111111111111111111111101111---29与此数
//	//000000000000000000000000000000000001101---结果=13 
//	// 
//	//
//
//
//	return 0;
//}

//
//int main()
//{
//
//	//int a = 3;

	////int b = ++a;//前置++，先++，后使用
	////a = a+1 = 4,b = a = 4

	//int b = a++;//后置++，先使用，再++
	////b=a=3,a=a+1=4

	//printf("%d\n", a);

	//printf("%d\n", b);


	//int a = 3;

	//int b = a--;//后置--，先使用，再--
	////b=a=3,a=a-1=2

	////int b = --a;//前置--，先--，后使用
	//////a=a-1=2,b=a=2

	//printf("%d\n", a);

	//printf("%d\n", b);

	//int a = 10;

	//printf("%d\n", a--);//a=10

	//printf("%d\n", a);//a=9

	//int a = 10;

//	//test(a--);//a=10
//
//	return 0;
//
//}

//int main()
//{
//	int i = 0;
//
//	for (i = 0; i < 10; i++)//与下面前置等同，使用时都已经+1了
//	{
//
//
//
//	}
//
//	for (i = 0; i < 10; ++i)//自定义类型，前置++效率更高
//	{
//
//
//
//	}
//
//
//	return 0;
//
//}
//
//int main()
//{
//
//	int a = 10;
//
//	int* p = &a;
//
//	*p = 20;
//
//	printf("%d\n", a);//a=20
//
//	return 0;
//
//}


//int main()
//{
//
//	int a =  (int)3.14;//编译器默认3.14是double类型数据，进行强制转换
//
//	printf("%d\n", a);
//	
//
//	return 0;
//
//}


//int main()
//{
//	time_t;//long long类型
//
//	srand((unsigned int)time(NULL));
//
//	int a = (int)3.14;
//
//	printf("%d\n", a);
//
//	return 0;
//
//}

//
//int main()
//{
//
//	int a = 0;
//
//	printf("%d\n", sizeof(a));
//
//	printf("%d\n", sizeof(int));
//
//	printf("%d\n", sizeof a);//括号可以省略
//
//	//printf("%d\n", sizeof int);//erro!括号不能省略
//
//
//	return 0;
//}
//

//sizeof是操作数，不是库函数可以用来求整形、数组、字符串的大小
//strlen是库函数，仅能用来求字符串长度


//void test1(int arr[])//此处arr是指针，仅占4(x86环境)/8(x64环境)个字节
//{
//
//
//	printf("%d\n", sizeof(arr));//(2)=8
//
//
//}
//
//void test2(char ch[])//此处ch是指针，仅占4(x86环境)/8(x64环境)个字节
//{
//
//	printf("%d\n", sizeof(ch));//(4)=8
//
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//
//	char ch[10] = { 0 };
//
//	printf("%d\n", sizeof(arr));//(1)=40
//
//	printf("%d\n", sizeof(ch));//(3)=10
//
//	test1(arr);
//
//	test2(ch);
//
//
//	return 0;
//
//}


//int main()
//{
//
//	if (3 == 5)
//	{
//
//
//	}
//	//erro
//	if ("abc" == "abcdef")//这样写是在比较两个字符串的首字符的地址
//	{
//
//	}
//两个字符串比较相等应该使用strcmp库函数进行
//	return 0;
//}


//int main()
//{
//
//	//int a = 3;
//
//	int a = 0;
//
//
//	//int b = 5;
//
//	int b = 0;
//
//
//	//int c = a && b;//逻辑与等同于生活中的并且
//
//	int c = a || b;//生活中的或者
//
//	printf("%d\n", c);
//
//
//
//	return 0;
//
//}

////1.能被4整除，并且不能被100整除
////2.能被400整除是闰年
//
//int is_leap_year(int y)
//{
//	if ((y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0))
//	{
//
//		return 1;
//
//	}
//
//	else
//	{
//
//		return 0;
//
//	}
//
//
//}
//
//int main()
//{
//
//
//	return 0;
//
//}

//&& 左边为假，右边就不计算了
//|| 左边为真，右边就不算了

//int main()
//{
//	//int i = 0, a = 0, b = 2, c = 3, d = 4;//与时输出 1 2 3 4
//
//	//int i = 0, a = 1, b = 2, c = 3, d = 4;//与时输出 2 3 3 5
//
//	//int i = 0, a = 0, b = 2, c = 3, d = 4;//或时输出 1 3 3 4
//
//	int i = 0, a = 1, b = 2, c = 3, d = 4;//或时输出 2 2 3 4
//
//
//
//	//i = a++ && ++b && d++;
//
//	i = a++ || ++b || d++;
//
//	printf("a = %d\nb = %d\nc = %d\nd = %d\n", a, b, c, d);
//
//	return 0;
//
//}

//int main()
//{
//	int a = 3;
//
//	int b = 0;
//
//	int max = (a > b ? a : b);
//
//	if (a > 5)
//
//		b = 3;
//
//	else
//
//		b = -3;
//
//	(a > 5) ? (b = 3) : (b = -3);//与下式等同
//
//	b = (a > 5 ? 3 : -3);//与上式等同
//
//
//	return 0;
//}

//int main()//逗号表达式
//{
//
//	int a = 1;
//
//	int b = 2;
//
//	int d = 0;
//
//	int c = (a > b, a = b + 10, a, b = a + 1);//c=13
//
//	if (a = b + 1, c = a / 2, d > 0);//c=7
//
//	printf("c = %d\n", c);
//
//
//	return 0;
//}
//
//a = get_val();
//
//count_val(a);
//
////while (a > 0)
////{
////
//	//业务处理
//
//	a = get_val();
//
//	count_val(a);
//
//}
//
////改写成用逗号表达式
//
//while (a = get_val(), count_val(a), a > 0)
//{
//
//	//业务处理
//
//}
//
//int main()
//{
//
//	int arr[10] = { 0 };//[]是下标引用操作符,操作数是arr 10
//
//	//arr[7] ---> *(arr+7) ---> 7[arr] 三者等价
//	//arr是数组首元素的地址
//	//arr+7就是跳过7个元素，指向了第8个元素
//	//*（arr+7）就是第8个元素
//	//
//	arr[7] = 8;//[]是操作符，操作数是arr,7
//
//	7[arr] = 9;//印证了[]是操作符，但不建议这样写
//
//	return 0;
//
//}

////函数定义
//int Add(int x, int y)
//{
//
//	return x + y;
//
//}
//
//
//int main()
//{
//
//	int a = 10;
//
//	int b = 20;
//	
//	//函数调用
//	int c = Add(a, b);//()是函数调用操作符，不能省略，操作数：Add,a,b
//
//	//siazeof a 此处sizeof不是函数
//
//	return 0;
//}

//struct Stu
//{
//
//char name[20];
//
//int age;
//
//double score;
//
//};
//
////void set_stu(struct Stu ss)
////{
////	strcpy(ss.name, "zhangsan");
////
////	ss.age = 20;
////
////	ss.score = 100.0;
////
////}
//
//void set_stu(struct Stu* ps)//用指针传参可以实现
//{
//	/*strcpy((*ps).name, "zhangsan");
//
//	(*ps).age = 20;
//
//	(*ps).score = 100.0;*///有点繁琐
//
//	strcpy(ps->name, "zhangsan");
//
//	ps->age = 20;
//
//	ps->score = 100.0;
//
//}
//
//
////void print_stu(struct Stu ss)
////{
////
////	printf("%s %d %lf", ss.name, ss.age, ss.score);//0 0.000,传参出了问题
////
////}
//
//void print_stu(struct Stu* ps)
//{
//
//	printf("%s %d %lf", ps->name, ps->age, ps->score);//直接从结构体相应地址找参数
//
//}
//
//
//int main()
//{
//	struct Stu s = { 0 };
//
//	//set_stu(s);
//
//	set_stu(&s);
//
//
//	//print_stu(s);
//
//	print_stu(&s);
//
//
//	return 0;
//}
