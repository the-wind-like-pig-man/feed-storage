//exercises_3.c---编写一个函数，接收三个参数，一个字符和两个整数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void print_char(char ch, int cols, int rows);
//函数声明，无返回值，三个参数分别是char、int、int
//第一个整数表示行内字符数，即列，第二个参数表示行数
//这一点和编程练习二不同

int main()
{

	char c = 0;

	int i = 0, j = 0;

	printf("Please enter the char you want to print: ");

	scanf("%c", &c);

	printf("Please enter the rows and cols you want to print: ");

	scanf("%d %d", &i, &j);
	//scanf()函数读入顺序为行、列、i对应行，j对应列

	print_char(c, i, j);



	return 0;

}


void print_char(char ch, int cols, int rows)
{

	for (int n = 0; n < rows; n++)
	{

		for (int m = 0; m < cols; m++)
		{

			printf("%c", ch);

		}

		printf("\n");


	}

}

//output:
//Please enter the char you want to print : W
//Please enter the rows and cols you want to print :
//5 5
//WWWWW
//WWWWW
//WWWWW
//WWWWW
//WWWWW