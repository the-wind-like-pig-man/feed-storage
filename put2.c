//put2.c---打印一个字符串，并统计打印的字符数

#include <stdio.h>

int main()
{

	const char* string = "Hello world! Hello my love!";

	put2(string);


	return 0;

}

int put2(const char* string)
{

	int count = 0;

	while (*string)//常规用法
	{

		putchar(*string++);

		count++;

	}

	putchar('\n');//不统计换行符

	return(count);

}

//output:
//Hello world!Hello my love!