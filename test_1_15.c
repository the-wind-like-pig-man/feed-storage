﻿#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <string.h>

//int main()
//{
//	//char arr1[20] = { 0 };
//
//	//char arr2[] = "hello world";
//
//	//strcpy(arr1, arr2);//把arr2中的内容拷贝到arr1中
//
//	//printf("%s\n", arr1);
//
//	char arr[20] = "hello world";
//
//	memset(arr, 'x', 5);//把arr中的前五个字符用x代替
//
//	memset(arr+6, 'w', 3);//把arr中的前五个字符用x代替
//
//	printf("%s\n", arr);
//
//	return 0;
//}

////函数的定义--->求两个整数中的最大值
//int get_max(int x, int y)
//{
//	return(x > y ? x : y);
//}
//
//int main()
//{
//	int a = 0;
//
//	int b = 0;
//
//	scanf("%d,%d", &a, &b);
//	//求最大值
//	//函数的调用
//	int m = get_max(a, b);
//
//	printf("%d\n", m);
//
//	return 0;
//}


////形式参数
//void Swap(int x, int y)//无法实现a,b交换
//{
//	int z = 0;
//
//	z = x;
//
//	x = y;
//
//	y = z;
//}

void Swap(int *px, int *py)//可以实现a,b交换
{
	int z = *px;//z=a

	*px = *py;//a=b

	*py = z;//b=a

}


//当实参传递给形参的时候，形参是实参的一份临时拷贝
//对形参的修改不会影响实参

int main()
{
	int a = 0;

	int b = 0;

	scanf("%d,%d", &a, &b);
	//交换

	printf("before change:a = %d,b = %d\n", a, b);
	//a和b叫实参

	Swap(&a, &b);

	printf("after change:a = %d,b = %d\n", a, b);


	return 0;
}