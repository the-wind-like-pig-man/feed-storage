﻿
#include <math.h>

#include <stdio.h>

//打印100-200之间的素数
//素数是只能被1和它本身整除的数

int main()
{
	int i = 0;

	int count = 0;

	for (i = 100; i <= 200; i++)
	{
		//判断i是否是素数
		//是素数就打印
		//拿2~i-1之间的数去试除i

		int flag = 1;//flag=1，表示是素数

		int j = 0;

		//for (j = 2; j <= i - 1; j++)
			for (j = 2; j <= sqrt(i); j++)//缩小试除的次数

		{
			if (i % j == 0)
			{
				flag = 0;

				break;
			}
		}

		if (flag == 1)
		{
			count++;//统计素数的个数

			printf("%d,", i);

		}
	}

	printf("\ncount = %d\n", count);//打印素数的个数

	return 0;
}
