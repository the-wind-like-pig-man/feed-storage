#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

float cacl(float x, float y);

int main()
{

	float x = 0, y = 0;

	printf("Please enter the two float data(seprate by blank): ");

	while (scanf("%f %f", &x, &y) == 2)
	{

		printf("The answer is %f\n", cacl(x, y));

		printf("Please enter the two float data(seprate by blank): ");

	}

	printf("The program end!");


	return 0;

}

float cacl(float x, float y)
{

	float result;

	result = (x - y) / (x * y);

	return result;

}