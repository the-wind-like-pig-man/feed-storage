//exercises_8.c---在一个函数内调用另一个函数，并打印one、two、three、Done！

#include <stdio.h>

int one_three(void);

int two(void);//两个函数的声明

int main()
{

	printf("Starting now:\n");

	one_three();

	printf("Done!\n");


	return 0;

}

int one_three()
{

	printf("One!\n");

	two();//调用two()函数

	printf("Three!\n");


	return 0;

}

int two()
{

	printf("Two!\n");

	return 0;

}

//output:
//Starting now :
//One!
//Two!
//Three!
//Done!
