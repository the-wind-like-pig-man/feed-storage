//prntval.c---printf()的返回值

#include <stdio.h>

int main()
{

	int bph2o = 212;

	int rv;

	rv = printf("%d F is water's boiling point.\n", bph2o);

	printf("The printf() fuction printed %d characters.\n",

		rv);



	return 0;

}

//output:
//212 F is water's boiling point.
//The printf() fuction printed 32 characters.---包括换行符\n
