﻿

#include <stdio.h>

#include <string.h>


//int main()
//{
//
//	char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
//
//	//printf("%d\n", strlen(arr));//输出随机值--->42  0xC0000005: Access violation reading location 0x0000000000000061.
//
//	//printf("%d\n", strlen(arr + 0));//输出随机值---42 0xC0000005: Access violation reading location 0x0000000000000061.
//
//	//printf("%d\n", strlen(*arr));//--->strlen('a'); --->strlen('97');//野指针--- : 0xC0000005: Access violation reading location 0x0000000000000062.
//
//
//	//printf("%d\n", strlen(arr[1]));//--->strlen('b'); --->strlen('98');//野指针---: 0xC0000005: Access violation reading location 0x0000000000000062.
//
//
//	printf("%d\n", strlen(&arr));//随机值---42
//
//	printf("%d\n", strlen(&arr + 1));//随机值-6 ---36
//
//	printf("%d\n", strlen(&arr[0] + 1));//随机值-1 ---41
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
//
//	char arr[] = "abcdef";
//
//	//[a b c d e f \0]
//
//	//printf("%d\n", sizeof(arr));//---7
//
//	//printf("%d\n", sizeof(arr + 0));//---4/8
//
//	//printf("%d\n", sizeof(*arr));//---1
//
//
//	//printf("%d\n", sizeof(arr[1]));//---1
//
//
//	//printf("%d\n", sizeof(&arr));//---4/8
//
//	//printf("%d\n", sizeof(&arr + 1));//---4/8
//
//	//printf("%d\n", sizeof(&arr[0] + 1));//---4/8
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
//
//	char arr[] = "abcdef";
//	//[a b c d e f \0]
//	//strlen是求字符串长度的，关注的是字符串中的\0，计算的是\0之前出现的字符的个数
//	//strlen是库函数，只针对字符串
//	//sizeof只关注占用内存空间的大小，不在乎内存中存放的内容
//	//sizeof是操作符，不是库函数
//
//	printf("%d\n", strlen(arr));//6
//
//	printf("%d\n", strlen(arr + 0));//6
//
//	//printf("%d\n", strlen(*arr));//error
//
//
//	//printf("%d\n", strlen(arr[1]));//error
//
//
//	printf("%d\n", strlen(&arr));//6
//
//	printf("%d\n", strlen(&arr + 1));//随机值
//
//	printf("%d\n", strlen(&arr[0] + 1));//5
//
//
//	return 0;
//
//}

//int main()
//{
//
//	//char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
//
//	char* p = "abcdef";
//
//	//[a b c d e f \0]
//
//	printf("%d\n", sizeof(p));//---4/8
//
//	printf("%d\n", sizeof(p));//---4/8
//
//	printf("%d\n", sizeof(*p));//---1
//
//
//	printf("%d\n", sizeof(p[0]));//---1
//
//
//	printf("%d\n", sizeof(&p));//---4/8
//
//	printf("%d\n", sizeof(&p + 1));//---4/8
//
//	printf("%d\n", sizeof(&p[0] + 1));//---4/8
//
//
//	return 0;
//
//}

int main()
{

	//char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };

	char* p = "abcdef";

	//[a b c d e f \0]

	printf("%d\n", strlen(p));//---6

	printf("%d\n", strlen(p + 1));//---5

	//printf("%d\n", srelen(*p));//---error


	//printf("%d\n", strlen(p[0]));//---error


	printf("%d\n", strlen(&p));//---6

	printf("%d\n", strlen(&p + 1));//---随机值

	printf("%d\n", strlen(&p[0] + 1));//---5


	return 0;

}
