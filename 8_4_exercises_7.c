//exercises_7.c---用字符代替数字来标记菜单的选项，用q代替5来作为结束输入的标记

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define EXTRA_HOUR 1.5

#define BASE_TAX 0.15

#define EXTRA_TAX 0.2

#define EXCEED_TAX 0.25

void show_menu(void);

float get_hours(void);

void calc_salary(float base_salary, float hours);

int main()
{

	float hours = 0;

	char selected = 0;

	do {
	
		show_menu();

		selected = getchar();

		while (getchar() != '\n')
		{

			continue;//使用getchar()函数读取第一个字符，并且抛弃输入的其他字符，清空缓存区
			//为scanf()函数正确读取数据做准备。switch语句合并了输入的大小写字符判断
			//

		}

		switch (selected)
		{

		case 'a':

		case 'A':

			printf("Hello, you select $8.75/hr.Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(8.75, hours);

			break;

		case 'b':

		case 'B':

			printf("Hello,you select $8.75 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(9.33, hours);

			break;

		case 'c':

		case 'C':

			printf("Hello, you select $8.75 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(10.00, hours);

			break;

		case 'd':

		case 'D':

			printf("Hello, you select $8.75 / hr. Enter the work hours: ");

			scanf("%f", &hours);

			calc_salary(11.20, hours);

			break;

		case 'q':

		case 'Q':

			break;

		default:

			printf("Error selected! Please retry!\n");

			break;

		}

	
	} while (selected != 'q' && selected != 'Q');

	printf("Done!\n");


	return 0;

}

void show_menu()
{

	char s1[] = "a) $8.75 / hr";

	char s2[] = "b) $9.33 / hr";

	char s3[] = "c) $10.00 / hr";

	char s4[] = "d) $ 11.20 / hr";

	char s5[] = "q) quit";

	printf("**********************************************************************\n");

	printf("Please enter the number corresponding to the desired pay rate or action\n");

	printf("%-40s", s1);

	printf("%-40s\n", s2);

	printf("%-40s", s3);

	printf("%-40s\n", s4);

	printf("%-40s\n", s5);

	printf("************************************************************************\n");

}

void calc_salary(float base_salary, float hours)
{

	float salary = 0, tax = 0, taxed_salary = 0;

	if (hours <= 30)
	{

		salary = hours * base_salary;

		tax = salary * BASE_TAX;

		taxed_salary = salary - tax;

	}

	else if (hours <= 40)
	{

		salary = hours * base_salary;

		tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		taxed_salary = salary - tax;

	}

	else
	{

		salary = (40 + (hours - 40) * EXTRA_HOUR) * base_salary;

		if (salary <= 450)
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX;

		}

		else
		{

			tax = 300 * BASE_TAX + (salary - 300) * EXTRA_TAX + (salary - 450) * EXCEED_TAX;

			taxed_salary = salary - tax;

		}

	}

	printf("Your salary before tax is %.2f, tax is %.2f, salary after tax is %.2f\n",
		salary, tax, taxed_salary);

	printf("\ncountinue...\n");

}

//output:
//**********************************************************************
//Please enter the number corresponding to the desired pay rate or action
//a) $8.75 / hr                           b) $9.33 / hr
//c) $10.00 / hr                          d) $ 11.20 / hr
//q) quit
//************************************************************************
//b
//Hello, you select $8.75 / hr.Enter the work hours : 50
//Your salary before tax is 513.15, tax is 103.42, salary after tax is 409.73
//
//countinue...
//**********************************************************************
//Please enter the number corresponding to the desired pay rate or action
//a) $8.75 / hr                           b) $9.33 / hr
//c) $10.00 / hr                          d) $ 11.20 / hr
//q) quit
//************************************************************************
//a)
//Error selected!Please retry!
//**********************************************************************
//Please enter the number corresponding to the desired pay rate or action
//a) $8.75 / hr                           b) $9.33 / hr
//c) $10.00 / hr                          d) $ 11.20 / hr
//q) quit
//************************************************************************
//c
//Hello, you select $8.75 / hr.Enter the work hours : 50
//Your salary before tax is 550.00, tax is 120.00, salary after tax is 430.00
//
//countinue...
//**********************************************************************
//Please enter the number corresponding to the desired pay rate or action
//a) $8.75 / hr                           b) $9.33 / hr
//c) $10.00 / hr                          d) $ 11.20 / hr
//q) quit
//************************************************************************
//q
//Error selected!Please retry!
//**********************************************************************
//Please enter the number corresponding to the desired pay rate or action
//a) $8.75 / hr                           b) $9.33 / hr
//c) $10.00 / hr                          d) $ 11.20 / hr
//q) quit
//************************************************************************
//q
//Done!
