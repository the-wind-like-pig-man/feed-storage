//for_loop.c---for循环示例
//
//#include <stdio.h>
//
//int main()
//{
//
//	int secs = 0;
//
//	for (secs = 5; secs > 0; secs--)
//	{
//
//		printf("%d seconds!\n", secs);
//
//	}
//
//	printf("We have ignition!\n");
//
//
//	return 0;
//
//}
//
////output:
////5 seconds!
////4 seconds!
////3 seconds!
////2 seconds!
////1 seconds!
////We have ignition!---点火


//#include <stdio.h>
//
//int main()
//{
//
//	int n = 0;//从2开始，每次递增13
//
//	for (n = 2; n < 60; n = n + 13)
//	{
//
//		printf("%d \n", n);
//
//	}
//
//
//	return 0;
//
//}
//
//output:
//2
//15
//28
//41
//54


//for_char.c---打印ASCII码

//#include <stdio.h>
//
//int main()
//{
//
//	char ch = '0';
//
//	for (ch = 'a'; ch <= 'z'; ch++)
//	{
//
//		printf("The ASCII value for %c is %d.\n", ch, ch);
//
//	}
//
//
//	return 0;
//
//}
//
//output:
//The ASCII value for a is 97.
//The ASCII value for b is 98.
//The ASCII value for c is 99.
//The ASCII value for d is 100.
//The ASCII value for e is 101.
//The ASCII value for f is 102.
//The ASCII value for g is 103.
//The ASCII value for h is 104.
//The ASCII value for i is 105.
//The ASCII value for j is 106.
//The ASCII value for k is 107.
//The ASCII value for l is 108.
//The ASCII value for m is 109.
//The ASCII value for n is 110.
//The ASCII value for o is 111.
//The ASCII value for p is 112.
//The ASCII value for q is 113.
//The ASCII value for r is 114.
//The ASCII value for s is 115.
//The ASCII value for t is 116.
//The ASCII value for u is 117.
//The ASCII value for v is 118.
//The ASCII value for w is 119.
//The ASCII value for x is 120.
//The ASCII value for y is 121.
//The ASCII value for z is 122.
