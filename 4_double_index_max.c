//double_array_index_max.c---编写一个程序，返回存储在double类型数组中的最大值的下标，并在一个简单的程序中测试该函数

#include <stdio.h>

int get_max_index(double number[], int n);
//使用传统方式传递参数，n表示number数组的长度

int main()
{

	/*double source[100] = {
	
		2.5, 3.2, 1.2, 1.6, 1.8, 2.4, 0.0, 5.2, 5.4, 6.4, 0.3, 0.9, 1.4, 7.2
	
	};*/

	double source[100] = {

		2.5, 3.2, 1.2, 1.6, 1.8, 2.4, 0.0, 5.2, 10.8, 5.4, 6.4, 0.3, 0.9, 1.4, 7.2

	};

	printf("The largest number's index is %d.\n", get_max_index(source, 100));


	return 0;

}

int get_max_index(double number[], int n)
{

	double max = number[0];

	int index = 0;

	for (int i = 0; i < n; i++)
	{

		if (max < number[i])
		{

			max = number[i];

			index = i;

		}

	}
	//函数在遍历、比较数组元素时，同时需要保存元素值和下标，元素值用于下一次比较
	//下标值需要保存并且在函数末尾返回

	return index;

}

//output:
//The largest number's index is 13.

//output2:
//The largest number's index is 8.
