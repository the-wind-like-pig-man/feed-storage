﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <assert.h>

#include <string.h>

#include <stdio.h>

#include <errno.h>
//int my_strcmp(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//    while (*str1 == *str2)
//     {
//	   if (*str1 == '\0')
//		return 0;//二者相等
//
//	   str1++;
//
//	   str2++;
//
//     }
//
//    if (*str1 > *str2)
//
//          return 1;
//
//    else
//
//          return -1;
//
//}

//int my_strcmp(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;//二者相等
//
//		str1++;
//
//		str2++;
//
//	}
//
//
//		return (*str1 - *str2);//简化代码
//
//
//}
//
//
//int main()
//
//{
//	/*char arr1[20] = "zhangsan";
//
//	char arr2[] = "zhangsanfeng";*/
//	//两个字符串比较相等，应该使用strcmp
//	//
//
//	char arr1[] = "abcdefg";
//
//	char arr2[] = "abcdef";
//
//
//	//char arr2[] = "abc";
//
//	/*int arr1[] = "abcdef";
//
//	int arr2[] = "abc";*/
//
//	//两个字符串比较，应该用strcmp
//
//	int ret = my_strcmp(arr1, arr2);
//
//	//int ret = strcmp(arr1, arr2);
//	//比较一下两个字符串是否相等
//	//arr1是数组名，数组名是数组首元素的地址 
//	//arr2是数组名，数组名是数组首元素的地址
//
//	/*if (arr1 == arr2)
//	{
//
//		printf("==\n");
//
//	}
//
//	else
//	{
//
//		printf("!=\n");
//
//	}*/
//
//	if (ret < 0)
//	{
//
//		printf("<\n");
//
//
//	}
//
//	else if (ret == 0)
//	{
//
//		printf("==\n");
//
//	}
//
//	else
//	{
//
//		printf(">\n");
//
//	}
//
//
//
//	return 0;
//
//}

//长度受限制的字符串函数
//strncmp
//strncat 
//strncpy

//int main()
//{
//
//	char arr1[20] = "abcdef";
//
//	char arr2[] = "bit";
//
//	//strcpy(arr1, arr2);
//
//	strncpy(arr1, arr2, 5);
//
//	printf("%s\n", arr1);
//
//	printf("%s\n", arr1);
//
//	return 0;
//
//}


//int main()
//{
//
//	char arr1[20] = "hello\0xxxxxx";
//
//	char arr2[] = "bit";
//
//	strncat(arr1, arr2, 6);
//
//	printf("%s\n", arr1);
//
//
//	return 0;
//
//}

//int main()
//{
//
//	char arr1[] = "abcdef";
//
//	char arr2[] = "abcd";
//
//	int ret = strncmp(arr1, arr2, 4);
//
//	printf("%d\n", ret);
//
//	if (ret == 0)
//	{
//
//		printf("==\n");
//
//	}
//
//	else if (ret < 0)
//	{
//
//		printf("<\n");
//
//	}
//
//	else
//
//		printf(">\n");
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//char email[] = "zpw@bitjiuyeke.com";
//
//	char email[] = "zpw@bitjiuyek.com";
//
//
//	char substr[] = "bitjiuyeke";
//
//	char* ret = strstr(email, substr);
//
//	if (ret == NULL)
//	{
//
//		printf("The sub string doesn't exist");
//
//	}
//
//	else
//	{
//
//		printf("%s\n", ret);
//
//	}
//
//	return 0;
//
//}

//KMP算法
//这个算法也是用来实现一个字符串中查找子字符串的
//效率高，但是实现难度大
//B站：比特大博哥

//char* my_strstr(const char* str1, const char* str2)
//{
//
//	assert(str1 && str2);
//
//	const char* s1 = str1;//固定str1的初始位置不变
//
//	const char* s2 = str2;//固定str2的初始位置不变
//
//	const char* p = str1;//初始化相等的位置
//
//	while (*p)
//	{
//
//		s1 = p;
//
//		s2 = str2;
//
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//
//			s1++;
//
//			s2++;
//
//		}
//
//		if (*s2 == '\0')
//		{
//
//			//return p;
//
//			return (char*)p;
//
//
//		}
//
//		p++;
//	}
//
//	return NULL;
//
//
//}
//
//int main()
//{
//
//	//char email[] = "zpw@bitjiuyeke.com";
//
//	/*char email[] = "zpw@bitjiuyeke.com";
//
//	char substr[] = "bitjiuyeke";*/
//
//	/*char* ret = my_strstr(email, substr);*/
//
//	char arr1[] = "abcdef";
//
//	char arr2[] = "bcdef";
//
//	char* ret = my_strstr(arr1, arr2);
//
//	if (ret == NULL)
//	{
//
//		printf("The sub string doesn't exist");
//
//	}
//
//	else
//	{
//
//		printf("%s\n", ret);
//
//	}
//
//	return 0;
//
//}


//strtok
//切割字符串
//

//int main()
//{
//
//	const char* sep = "@.";
//
//	char email[] = "Lei.Kunfeng@rockwellautomation.com";
//
//	//char cp[30] = { 0 };//"Lei.Kunfeng@rockwellautomation.com"---要切割的字符串---初始化长度小，栈溢出
//
//	char cp[40] = { 0 };//"Lei.Kunfeng@rockwellautomation.com"---要切割的字符串
//
//
//	strcpy(cp, email);
//
//	char* ret = NULL;
//
//	for (ret = strtok(cp, sep);
//		ret != NULL;
//		ret = strtok(NULL, sep))//用for循环时字符串长度不受限制，均可打印
//	{
//
//		printf("%s\n", ret);
//
//	}
//
//	////char* ret = NULL;
//
//	//char* ret = strtok(cp, sep);
//
//	//if(ret != NULL)
//
//	//    printf("%s\n", ret);
//
//	//ret = strtok(NULL, sep);
//
//	//if (ret != NULL)
//
//	//   printf("%s\n", ret);
//
//
//	//ret = strtok(NULL, sep);
//
//	//if (ret != NULL)
//
//	//    printf("%s\n", ret);
//
//	//ret = strtok(NULL, sep);
//
//	//if (ret != NULL)
//
//	//	printf("%s\n", ret);
//	////---分割成几段就要使用strtok()函数几次
//	////输出：
//	////Lei
//	////Kunfeng
//	////rockwellautomation
//	////com
//	
//	
//
//	return 0;
//
//}


//C语言的库函数，在执行失败的时候，都会设置错误码
//0 1 2 3 4 5 6 7 8 等等 
//
//
//int main()
//{
//
//	//printf("%s\n", strreeor(0));//---函数写错了多了一个r，后一个e应为rr
//
//	//printf("%d\n", strerror(0));
//
//	//printf("%s\n", strerror(1));
//
//	//printf("%s\n", strerror(2));
//
//	//printf("%s\n", strerror(3));
//
//	//printf("%s\n", strerror(4));
//
//	//printf("%s\n", strerror(5));
//
//	//printf("%s\n", strerror(6));
//
//	//输出：
//	//84374656
//	//Operation not permitted
//	//No such file or directory
//	//No such process
//	//Interrupted function call
//	//Input / output error
//	//No such device or address
//	
//	//errno---C语言设置的一个全局错误码存放的变量,头文件包含后可以直接用
//
//	//FILE* pf = fopen("test.txt", "r");
//
//	FILE* pf = fopen("C:\\Users\\KLei1\\Desktop\\test.txt", "r");//绝对路径下找到文件输内存出地址：000001C19DE35580
//
//
//	if (pf == NULL)
//	{
//
//		printf("%s\n", strerror(errno));
//
//		return 1;
//
//	}
//
//	else
//	{
//
//		printf("%p\n", pf);//输出文件地址：00000207EB0C3790
//
//	}
//
//
//	return 0;
//
//}

//输出：No such file or directory

//#include <ctype.h>
//
//int main()
//{
//
//	int a = isspace(' ');//判断是否是空白字符函数
//
//	//int a = isspace('j');
//
//	printf("%d\n", a);
//
//	//printf("%d\n", a);
//
//	/*int a = isdigit('0');//判断是否是数字的函数
//
//	int a = isdigit('e');
//
//	printf("%d\n", a);*/
//
//	printf("%c\n", tolower('W'));//大写字母转换为小写函数
//
//
//
//	return 0;
//
//}



int main()
{

	int arr1[] = { 1, 2, 3, 4, 5, 6, 7 };

	int arr2[] = { 0 };

	//memcpy(arr2, arr1, 28);

	my_memcpy(arr2, arr1, 28);

	/*float arr3[] = { 1, 2, 3, 4, 5, 6, 7 };

	float arr4[] = { 0 };

	memcpy(arr4, arr3, 20);*/



	return 0;

}