//exercises_4.c---打印四段话

#include <stdio.h>

int jolly(void);

int deny(void);//函数声明

int main()
{

	int i = 0;

	while (i < 5)
	{

		jolly();

		i++;

	}

	deny();

	return 0;

}

int jolly(void)//jolly()函数定义
{

	printf("For he's a jolly good fellow!\n");

	return 0;

}

int deny(void)//deny()函数定义
{

	printf("Which nobady can deny!\n");

	return 0;

}

//output:
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//Which nobady can deny!

//output:
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//For he's a jolly good fellow!
//Which nobady can deny!