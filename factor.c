//factor.c---使用循环和递归计算阶乘

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

long fact(int n);

long rfact(int n);

int main()
{

	int num = 0;

	printf("This program calculates factorials.\n");

	printf("Please enter a value in the range 0-12 (q to quit): \n");

	while (scanf("%d", &num) == 1)
	{

		if (num < 0)
		{

			printf("No negative numbers,please.\n");

		}

		else if (num > 12)
		{

			printf("Please keep input under 13.\n");

		}

		else
		{

			printf("loop: %d factorial = %ld\n",
				num, fact(num));

			printf("recursion: %d factorial = %ld\n",
				num, rfact(num));

		}

		printf("Please enter a value in the range 0-12(q to quit): \n");

	}

	printf("Bye!\n");


	return 0;

}

long fact(int n)//使用循环的函数
{

	long ans = 0;

	for (ans = 1; n > 1; n--)
	{

		ans *= n;

		//return ans;

	}

	return ans;

}

long rfact(int n)//使用递归的函数
{

	long ans = 0;

	if (n > 0)
	{

		ans = n * rfact(n - 1);

	}

	else
	{

		ans = 1;

	}

	return ans;


}

//output:
//This program calculates factorials.
//Please enter a value in the range 0 - 12 (q to quit) :
//	3
//	loop : 3 factorial = 3
//	recursion : 3 factorial = 6
//	Please enter a value in the range 0 - 12(q to quit) :
//	6
//	loop : 6 factorial = 6
//	recursion : 6 factorial = 720
//	Please enter a value in the range 0 - 12(q to quit) :
//	9
//	loop : 9 factorial = 9
//	recursion : 9 factorial = 362880
//	Please enter a value in the range 0 - 12(q to quit) :
//	q
//	Bye!

//output2：
//This program calculates factorials.
//Please enter a value in the range 0 - 12 (q to quit) :
//	3
//	loop : 3 factorial = 6
//	recursion : 3 factorial = 6
//	Please enter a value in the range 0 - 12(q to quit) :
//	5
//	loop : 5 factorial = 120
//	recursion : 5 factorial = 120
//	Please enter a value in the range 0 - 12(q to quit) :
//	6
//	loop : 6 factorial = 720
//	recursion : 6 factorial = 720
//	Please enter a value in the range 0 - 12(q to quit) :
//	10
//	loop : 10 factorial = 3628800
//	recursion : 10 factorial = 3628800
//	Please enter a value in the range 0 - 12(q to quit) :
//	15
//	Please keep input under 13.
//	Please enter a value in the range 0 - 12(q to quit) :
//	0
//	loop : 0 factorial = 1
//	recursion : 0 factorial = 1
//	Please enter a value in the range 0 - 12(q to quit) :
//	q
//	Bye!
