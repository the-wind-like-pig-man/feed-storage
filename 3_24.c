﻿
#include <stdio.h>

#include <windows.h>

//int main()
//{
//
//	int a = 20;
//	//20
//	//00000000000000000000000000010100
//	// 0x00 00 00 14
//	//00000000000000000000000000010100
//	//00000000000000000000000000010100
//	//
//
//	int b = -10;
//	//10000000000000000000000000001010---原码
//	//0x80 00 00 0a 
//	//11111111111111111111111111110101---反码
//	//0xff ff ff f5 
//	//11111111111111111111111111110110---补码
//	//0xff ff ff f6
//	//
//	//
//
//	return 0;
//
//}

//int main()//判断存储器是大端字节序存储，还是小端字节序存储
//{
//
//	int a = 1;
// //0x00000000 00000000 00000000 00000001
//
//	//printf("xiao duan\n");//err\o
//
//	if (*(char*) & a == 1)
//	{
//
//		printf("xiao duan\n");//小端存储
//
//	}
//
//	else
//	{
//
//		printf("da duan\n");//大端存储
//
//	}
//
//	return 0;
//
//}
//
//int check_sys()//非简介版
//{
//
//	int a = 1;
//
//	if (*(char*)&a == 1)//对整型a数据的存储内容第一个字节解引用
//	{
//
//		return 1;//返回小端
//
//	}
//
//	else
//	{
//
//		return 0;//返回大端
//
//	}
//}
//
////int check_sys()//简介版
////{
////
////	int a = 1;
////
////	return *(char*)&a;
////
////}
//
//int main()
//{
//
//	int ret = check_sys();
//
//	if (ret == 1)
//	{
//
//		printf("small head\n");
//
//	}
//
//	else
//	{
//
//		printf("big head\n");
//
//	}
//
//
//	return 0;
//
//}

//int main()
//{
//
//	unsigned int i;
//
//	for (i = 9; i >= 0; i--)//死循环
//	{
//
//		printf("%u\n", i);//9 8 7 6 5 4 3 2 1 0 4294967295 ...
//		//0-1 ---> -1
//		//10000000000000000000000000000001 --- -1的原码
//		//11111111111111111111111111111110 --- -1的反码
//		//11111111111111111111111111111111 --- -1的补码
//		Sleep(1000);//休眠1000毫秒
//
//	}
//
//	return 0;
//
//}

//int main()
//{
//
//	char a[1000];
//
//	int i;
//
//	for (i = 0; i < 1000; i++)
//	{
//
//		a[i] = -1 - i;
//
//	}
//	//arr[i] ---> char范围：-128~127
//	//-1，-2，-3，-4，...-1000 ---> 循环1000次
//	//-1,-2,-3...-128,127,126,125...3,2,1,0...
//	//128+127=255
//	//
//	printf("%d", strlen(a));//输出255
//	
//	//stelen是求字符串的长度，关注的是字符串中'\0'(数字0)之前出现多少字符
//	// 
//	// 
//	// 
//	//
//
//	return 0;
//
//}

//unsigned char i = 0;
////
////unsigned char的取值范围是0~255
////
//int main()
//{
//
//	for (i = 0; i <= 255; i++)
//	{
//
//		printf("hello world!\n");//死循环
//
//		Sleep(1000);
//
//	}
//
//	return 0;
//
//}

int my_strlen(const char* str)
{

	assert(str);

	int count = 0;

	while (*str)
	{

		str++;

		count++;

	}

	return count;


}

int main()
{
	//int len = strlen("abcdef");
	// printf("%d\n",len);
	//size_t ---> unsigned int类型，只有正数，没有负数
	//
	//
	//if(my_strlen("abc") - my_strlen("abcdef"))//强制类型转换为int，有符号数
	//   printf(">\n");
	//else
	//    printf("<\n");//输出<号
	//

	if (strlen("abc") - strlen("abcdef") >= 0)//两个无符号数相减，结果为无符号数
	{

		printf(">\n");

	}

	else
	{

		printf("<\n");

	}
	return 0;

}