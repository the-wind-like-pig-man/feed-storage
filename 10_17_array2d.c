//array2d.c---处理二维数组的函数

#include <stdio.h>

#define ROWS 3

#define COLS 4

void sum_rows(int ar[][COLS], int rows);

void sum_cols(int[][COLS], int);//省略形参名，没问题

int sum2d(int(*ar)[COLS], int rows);//另一种语法

int main()
{

	int junk[ROWS][COLS] = {
		{2, 4, 6, 8},

		{3, 5, 7, 9},

		{12, 10, 8, 6},
	
	};

	sum_rows(junk, ROWS);
	
	sum_cols(junk, ROWS);

	printf("Sum of all elements = %d\n", sum2d(junk, ROWS));


	return 0;

}

void sum_rows(int ar[][COLS], int rows)
{

	int r = 0;

	int c = 0;

	int tot = 0;

	for (r = 0; r < rows; r++)
	{

		tot = 0;//清零上一行tot值

		for (c = 0; c < COLS; c++)
		{

			tot += ar[r][c];

		}


		printf("rows %d: sum = %d\n", r, tot);

	}

	//printf("rows %d: sum = %d\n", r, tot);
	return tot;


}

void sum_cols(int ar[][COLS], int rows)
{

	int r = 0;

	int c = 0;

	int tot = 0;

	for (c = 0; c < COLS; c++)
	{

		tot = 0;//清零上一列tot值

		for (r = 0; r < rows; r++)
		{

			tot += ar[r][c];

		}

		printf("cols %d: sum = %d\n", c, tot);

	}

	//printf("cols %d: sum = %d\n", c, tot);
	return tot;


}

int sum2d(int ar[][COLS], int rows)
{

	int r = 0;

	int c = 0;

	int tot = 0;

	for (r = 0; r < rows; r++)
	{

		for (c = 0; c < COLS; c++)
		{

			tot += ar[r][c];

		}

	}

	return tot;

}
//
//output:
//rows 0 : sum = 20
//rows 1 : sum = 24
//rows 2 : sum = 36
//cols 0 : sum = 17
//cols 1 : sum = 19
//cols 2 : sum = 21
//cols 3 : sum = 23
//Sum of all elements = 80