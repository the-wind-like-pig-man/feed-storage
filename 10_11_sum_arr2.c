//sum_arr2.c---数组元素之和

#include <stdio.h>

#define SIZE 10

int sump(int* start, int* end);

int main()
{

	int marbles[SIZE] = { 20, 10, 5, 39, 4, 16, 19, 26, 31, 20 };

	long answer = 0;

	answer = sump(marbles, marbles + SIZE);

	printf("(marbles + SIZE) = %p\n", (marbles + SIZE));

	printf("The total number of marbles is %ld.\n", answer);



	return 0;

}

//使用指针算法

int sump(int* start, int* end)
{

	int total = 0;

	while (start < end)//-		start	0x0000005ede0ff9c0 {-858993460}	int *
//-		end	0x0000005ede0ff9c0 {-858993460}	int *
	{

		total += *start;//把数组元素的值加起来

		start++;//让指针指向下一个元素

	}

	return total;


}

//output:
//The total number of marbles is 190.

//output:
//(marbles + SIZE) = 0000009AE8AFF910
//The total number of marbles is 190.