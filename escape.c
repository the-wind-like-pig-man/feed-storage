//escape.c---ʹ��ת������

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	float salary = 0;

	printf("\aEnter your desired monthly salary:");//1

	printf("$______\b\b\b\b\b\b\b");//2

	scanf("%f", &salary);

	printf("\n\t$%.2f a month is $%.2f a year.", salary,
		
		salary * 12.0);//4

	printf("\rGee!\n");



	return 0;

}
//
//output:
//Enter your desired monthly salary : 4000
//
//Gee!$4000.00 a month is $48000.00 a year.