//s_gets.c---s_gets()函数的使用

#include <stdio.h>

#define STLEN 20

char* s_gets(char* st, int n);

int main()
{

	char st[STLEN];

	int n = STLEN;

	puts("Please enter strings (empty line to quit):");

	
	char* s_gets(char* st, int n);
	
	fgets(st, STLEN, stdin);

	puts(st);

	return 0;

}

char* s_gets(char* st, int n)//没有进入函数调用？
{

	char* ret_val;

	int i = 0;

	ret_val = fgets(st, n, stdin);

	if (ret_val)//即，ret_val != NULL
	{

		while (st[i] != '\n' && st[i] != '\0')
		{

			i++;

		}

		if (st[i] == '\n')
		{

			st[i] = '\0';

		}

		else
		{

			while (getchar() != '\n')
			{

				continue;

			}

		}

		return ret_val;

	}

}

//output:
// Please enterstrings(empty line to quit) :
//	we are level
//	we are level
//
