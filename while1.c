//while1.c---注意花括号的使用

//糟糕的代码创建了一个无限循环

//#include <stdio.h>
//
//int main()
//{
//
//	int n = 0;
//
//	while (n < 3)//---not {}
//
//		printf("n is %d\n", n);
//
//		n++;
//
//	printf("That's all this program does\n");
//
//
//	return 0;
//
//}
//
//output:
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0
//n is 0


#include <stdio.h>

int main()
{

	int n = 0;

	while (n < 3)
	{
	
		printf("n is %d\n", n);

		n++;
	
	}
		
	printf("That's all this program does\n");


	return 0;

}

//output:
//n is 0
//n is 1
//n is 2
//That's all this program does