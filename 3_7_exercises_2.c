//3_7_exercises_2.c---输入一个ASCII码值，并打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int input = 0;

	printf("Please enter a value of char int ASCII: ");

	scanf("%d", &input);//通过scanf()函数读取用户输入，并存储在input变量中

	printf("You input value is %d, and char is %c\n", input, input);//通过转换说明符%d与%c打印整形数值和字符




	return 0;

}

//output:
//Please enter a value of char int ASCII : 65
//You input value is 65, and char is A 
// 
//output:
//Please enter a value of char int ASCII : 66
//You input value is 66, and char is B

//output:
//Please enter a value of char int ASCII : 67
//You input value is 67, and char is C

//output:
//Please enter a value of char int ASCII : 97
//You input value is 97, and char is a