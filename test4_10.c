﻿#define _CRT_SECURE_NO_WARNINGS 0

typedef unsigned int unit;

//typedef struct Node//typedef只能对类型重命名
//{
//	int data;
//	struct Node* next;
//
//}Node;
//
//int main()
//{
//	unsigned int num = 0;
//	unit num2=1;
//	struct Node n;
//	Node n2;
//
//	return 0;
//
//}

//static:1.修饰局部变量 2.修饰全局变量 3.修饰函数

//#include <stdio.h>
//
//void test()
//{
//	//int a= 1;
//	static int a = 1;
//	a++;
//	printf("%d\n", a);//局部变量进入区域创建，出了区域销毁
//}
//
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test();
//		i++;
//	}
//
//	return 0;
//}

//void test()//void---不需要返回任何值
//{
////执行任务
//	printf("hello world!\n");
//}

//修饰全局变量
//extern int g_val;//extern---声明外部符号

//int main()
//{
//	printf("%d\n", g_val);
//
//	return 0;
//}

//static修饰函数

//int Add(int x, int y);
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int z = Add(a, b);
//
//	printf("%d\n", z);
//
//	return 0;
//}

//int main()
//{
//	register int num = 3;//告诉计算机，建议把3存放到寄存器中
//
//	return 0;
//}

//#define NUM 100
//
//#define ADD(x,y) ((x)+(y))//ADD---宏名  x、y---宏参，参数是无类型  ((x)+(y))---宏体
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	printf("%d\n", NUM);
//	int n= NUM;
//	printf("%d\n", n);
//	int arr[NUM] = { 0 };
//	int a = 10;
//	int b = 20;
//	int c = ADD(a, b);
//	printf("%d\n", c);
//
//	return 0;
//}

//int main()
//{
//	int a = 10;//向内存申请4个字节，存储10
//	&a;//取地址操作符
//	int* p = &a;
//	*p = &a;
//	//p是指针变量
//	char ch = 'w';
//	char* pc = &ch;//指针就是用来存放变量地址的
//	//p就是指针变量
//	*p = 20;//解引用操作符，意思就是通过p中存放的地址，找到p所指的对象，*p就是p所指向的对象
//	printf("%d\n", a);//*与&是一对组合，前者是解引用操作符，后者是取地址操作符
//
//
//	return 0;
//}

//int main()
//{
//	int* p;
//	char* p2;
//	//不管什么类型的指针，都是在创建指针变量，指针变量是用来存放地址的
//	//指针变量的大小取决于一个地址存放的时候需要多大空间
//	//32位机器上的地址：32bit位---4byte，所以指针变量的大小是4个字节
//	//64位机器上的地址：64bit位---8byte，所以指针变量的大小是8个字节
//
//	/*printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(float*));
//	printf("%d\n", sizeof(double*));*/
//	printf("%zu\n", sizeof(char*));
//	printf("%zu\n", sizeof(short*));
//	printf("%zu\n", sizeof(int*));
//	printf("%zu\n", sizeof(float*));
//	printf("%zu\n", sizeof(double*));
//
//
//	return 0;
//}

//学生---结构体
//struct Stu
//{
//	//成员
//	char name[20];
//	int age;
//	char sex[10];
//	char tele[12];
//
//};
//
//void print(struct Stu* ps)
//{
//	printf("%s %d %s %s\n", (*ps).name, (*ps).age, (*ps).sex, (*ps).tele);
//	//->:结构体指针变量->成员名
//	printf("%s %d %s %s\n", ps->name, ps->age, ps->sex, ps->tele);
//
//
//}
//
//int main()
//{
//	struct Stu s = { "zhangsan",20,"nan","15236667777" };
//	//结构体对.成员名
//	printf("%s %d %s %s\n", s.name, s.age, s.sex, s.tele);
//
//	//printf(&s);//不是调用print函数--->打印出“张三”
//
//	print(&s);//调用print函数
//
//	return 0;
//}

#include <stdio.h>

int main()
{
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);

	//scanf("%d %d\n", &a, &b);//不能加换行符，否则无结果

	int m = a / b;
	int n = a % b;
	printf("%d %d\n", m, n);

	printf("%d %d\n", a / b, a % b);

	return 0;
}