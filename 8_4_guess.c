//guess.c---一个拖沓且错误的猜数字程序

#include <stdio.h>

int main()
{

	int guess = 1;

	char ch = 0;

	printf("Pick an intrger from 1 to 100.I will try to guess ");

	printf("it.\nRespond with a y if my guess is right and with ");

	printf("\n an n if it is wrong.\n");

	printf("Uh...is your number %d?\n", guess);

	while ((ch = getchar()) != 'y')//获取响应，与y作对比(getchar()读取了'n'和'\n'两个字符)
	{

		//printf("Well, then, is it %d?\n", ++guess);

		printf("Well, then, is it %d?\n", guess++);


	}

	printf("I knew I could do it!\n");



	return 0;

}

//output:
//Pick an intrger from 1 to 100.I will try to guess it.
//Respond with a y if my guess is right and with
//an n if it is wrong.
//Uh...is your number 1 ?
//n
//Well, then, is it 2 ?
//Well, then, is it 3 ?
//n
//Well, then, is it 4 ?
//Well, then, is it 5 ?
//n
//Well, then, is it 6 ?
//Well, then, is it 7 ?
//n
//Well, then, is it 8 ?
//Well, then, is it 9 ?
//y
//I knew I could do it!