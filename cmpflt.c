//cmpflt.c---�������Ƚ�

#define _CRT_SECURE_NO_WARNINGS 0

#include <math.h>

#include <stdio.h>

int main()
{

	const double ANSWER = 3.14159;

	double response;

	printf("What is the value of pi?\n");

	scanf("%lf", &response);

	while (fabs(response - ANSWER) > 0.0001)
	{

		printf("Try again!\n");

		scanf("%lf", &response);

	}

	printf("Close enough!\n");


	return 0;

}

//output:
//What is the value of pi ?
//3.14
//Try again!
//3.1416
//Close enough!

//output:
//What is the value of pi ?
//3.14
//Try again!
//3.141
//Try again!
//3.1415
//Close enough!