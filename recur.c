//recur.c---�ݹ���ʾ

#include <stdio.h>

void up_and_down(int);

int main()
{

	up_and_down(1);


	return 0;

}

void up_and_down(int n)
{

	printf("Level %d: n location %p.\n", n, &n);//#1

	if (n < 4)
	{

		up_and_down(n + 1);

	}

	printf("Level %d: n location %p.\n", n, &n);//#2


}

//output:
//Level 1 : n location 0000007E0D13FB40.
//Level 2 : n location 0000007E0D13FA40.
//Level 3 : n location 0000007E0D13F940.
//Level 4 : n location 0000007E0D13F840.
//Level 4 : n location 0000007E0D13F840.
//Level 3 : n location 0000007E0D13F940.
//Level 2 : n location 0000007E0D13FA40.
//Level 1 : n location 0000007E0D13FB40.