//ptr_ops.c---指针操作

#include <stdio.h>

int main()
{

	int urn[5] = { 100, 200, 300, 400, 500 };

	int* ptr1, * ptr2, * ptr3;

	ptr1 = urn;//把数组首元素的地址赋给指针

	ptr2 = &urn[2];//把数组urn的第三个元素的地址赋给指针
	//解引用指针，以及获得指针的地址

	printf("pointer value, derefrrenced pointer, pointer address: \n");

	printf("ptr1 = %p, *ptr1 = %d, &ptr1 = %p\n", ptr1, *ptr1, &ptr1);

	//指针加法
	ptr3 = ptr1 + 4;

	printf("\nadding an int to a pointer:\n");

	printf("ptr1 + 4 = %p, *(ptr1 + 4) = %d\n", ptr1 + 4, *(ptr1 + 4));

	ptr1++;//递增指针

	printf("\nvalues after ptr1++:\n");

	printf("ptr1 = %p, *ptr1 = %d, &ptr1 = %p\n", ptr1, *ptr1, &ptr1);

	ptr2--;//递减指针

	printf("\nvalues after --ptr2:\n");

	printf("ptr2 = %p, *ptr2 = %d, &ptr2 = %p\n", ptr2, *ptr2, &ptr2);

	--ptr1;//恢复为初始值

	++ptr2;//恢复为初始值

	printf("\npointers reset to original values:\n");

	printf("ptr1 = %p, ptr2 = %p\n", ptr1, ptr2);
	
	//一个指针减去另一个指针
	printf("\nsubtracting one pointer from another:\n");

	printf("ptr2 = %p, ptr1 = %p, ptr2 - ptr1 = %td\n", ptr2, ptr1, ptr2 - ptr1);//ptr2-ptr1---意思指这两个指针所指向的元素相隔两个int

	//一个指针减去一个整数
	printf("\nsubtracting an int from a pointer:\n");

	printf("ptr3 = %p, ptr3 - 2 = %p\n", ptr3, ptr3 - 2);





	return 0;

}

//output:
//pointer value, derefrrenced pointer, pointer address :
//ptr1 = 000000F678AFF928, * ptr1 = 100, & ptr1 = 000000F678AFF958
//
//adding an int to a pointer :
//ptr1 + 4 = 000000F678AFF938, *(ptr1 + 4) = 500
//
//values after ptr1++ :
//	ptr1 = 000000F678AFF92C, *ptr1 = 200, &ptr1 = 000000F678AFF958
//
//	values after --ptr2 :
//	ptr2 = 000000F678AFF92C, *ptr2 = 200, &ptr2 = 000000F678AFF978
//
//	pointers reset to original values :
//ptr1 = 000000F678AFF928, ptr2 = 000000F678AFF930
//
//subtracting one pointer from another :
//ptr2 = 000000F678AFF930, ptr1 = 000000F678AFF928, ptr2 - ptr1 = 2
//
//subtracting an int from a pointer :
//ptr3 = 000000F678AFF938, ptr3 - 2 = 000000F678AFF930
