﻿#pragma once

#include <stdio.h>

#include <stdlib.h>

#include <time.h>

#define _CRT_SECURE_NO_WARNINGS 0

#define ROW 3 //若后续需要更改行和列，在此处更改即可，很方便

#define COL 3 //若后续需要更改行和列，在此处更改即可，很方便

//初始化棋盘
void InitBoard(char board[ROW][COL], int row, int col);

//打印棋盘
void DisplayBoard(char board[ROW][COL], int row, int col);

//玩家下棋
void PlayerMove(char board[ROW][COL], int row, int col);

//电脑下棋
void ComputerMove(char board[ROW][COL], int row, int col);

//判断四种状态---
// 1.玩家赢---返回“*”
// 2.电脑赢---返回“#”
// 3.平局---返回“Q”
// 4.继续---返回“C”
char IsWin(char board[ROW][COL],int row,int col);
