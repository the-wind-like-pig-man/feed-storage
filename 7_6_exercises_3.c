//exercises_3.c---读取整数，直到用户输入0。输入结束后，报告输入的偶数（不包括0）个数、这些偶数的平均值
//输入的奇数个数和这些奇数的平均值

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int odd_sum = 0;

	int even_sum = 0;

	int odd_count = 0;

	int even_count = 0;

	int input = 0;
	//定义变量分别对奇数和偶数进行计算与求和

	printf("Please input integers (0 for exit): ");

	while (scanf("%d", &input))
	{

		if (input == 0) break;

		if (input % 2 == 0)
		{

			even_sum = even_sum + input;
			//even_sum += input;

			even_count++;

		}

		else
		{

			odd_sum = odd_sum + input;
			//odd_sum += input;

			odd_count++;

		}

	}

	printf("Have %d even number, average is %g\n", even_count, 1.0 * even_sum / even_count);
	//平均数应当以浮点数型数据显示，因此先乘以1.0,1.0*even_sum将结果隐式转换为浮点数据类型

	printf("Have %d odd number, average is %g\n", odd_count, 1.0 * odd_sum / odd_count);

	printf("Done!\n");


	return 0;

}
//
//output:
//Please input integers(0 for exit) : 12 23 42 45 55 60 80 0
//
//
//0
//Have 4 even number, average is 48.5
//Have 3 odd number, average is 41
//Done!

//output1:
//Please input integers(0 for exit) : 12
//23
//22
//11
//33
//45
//0
//Have 2 even number, average is 17
//Have 4 odd number, average is 28
//Done!
