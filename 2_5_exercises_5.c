//exercises_5.c---打印函数

#include <stdio.h>

int c_i(void);

int r_b(void);//函数声明

int main()
{

	c_i();//函数调用

	printf(", ");

	r_b();

	printf("\n");

	c_i();

	printf(", \n");

	r_b();

	printf("\n");


	return 0;

}

int c_i(void)//函数定义
{

	printf("China, India");

	return 0;

}

int r_b(void)
{

	printf("Russia, Brazil");

	return 0;

}

//output:
//China, India, Russia, Brazil
//China, India,
//Russia, Brazil
