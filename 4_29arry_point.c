﻿

#include <stdio.h>

//int Add(int x, int y)
//{
//
//
//	return x + y;
//
//
//}
//
//int main()
//{
//
//	int arr[5] = { 0 };
//
//	//&数组名---取出的是数组的地址
//
//	int(*p)[5] = &arr;//数组指针
//
//	//&函数名---取出的是函数的地址
//
//	//printf("%p\n", &Add);//00007FF6980F13D9
//
//	printf("%p\n", Add);//00007FF6980F13D9
//
//	//对于函数来说，&函数名和函数名都是函数的地址
//
//	int (*pf)(int, int) = &Add;
//
//	//int (*pf)(int, int) = Add;// --- ok
//
//		//int ret = (*pf)(2, 3);//---ok
//
//	int ret = Add(2, 3);//---ok
//
//	//int ret = pf(2, 3);//---ok
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}

//
//int Add(int x, int y)
//{
//
//
//	return x + y;
//
//
//}
//
//void calc(int(*pf)(int, int))//函数传参，指针接收
//{
//
//	int a = 3;
//
//	int b = 5;
//
//	int ret = pf(a, b);
//
//	printf("%d\n", ret);
//
//
//}
//
//int main()
//{
//
//	calc(Add);
//
//	return 0;
//
//}

//int main()
//{
//
//	//指针数组
//
//	int* arr[4];
//
//	char* ch[5];
//
//	int** p = arr;
//
//	//数组指针
//
//	int arr2[5];
//
//	int(*pa)[5] = &arr2;
//
//	char* arr3[6];
//
//	char(*p3)[6] = &arr3;
//
//
//	return 0;
//
//}
//
//int test(const char* str)
//{
//
//	printf("test()\n");
//
//
//	return 0;
//
//}

//int main()
//{
//
//	//函数指针---也是一种指针，是指向函数的指针
//
//	//printf("%p\n", test);//---00007FF7937813DE
//
//	//printf("%p\n", &test);//---00007FF7937813DE
//
//	//int (*pf)(const char*) = test;
//
//	//(*pf)("abc");//--->test()
//
//	////pf <---> test
//
//	//test("abc");//--->test()
//
//	//pf("abc");//--->test()
//
//
//
//	return 0;
//
//}

//typedef unsigned int uint;

typedef void (*pf_t)(int);//把void(*)(int)类型重命名为pf_t

int main()
{

	//(*(void(*)())0)();//调用首地址为0处的函数

	//void(*signal(int, void(*)(int)))(int);//难以阅读和理解

	pf_t signal(int, pf_t);//简单易阅读、理解

	return 0;

}