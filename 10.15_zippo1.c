//zippo1.c---zippo的相关信息

#include <stdio.h>

int main()
{

	int zippo[4][2] = { {2, 4}, {6, 8}, {1, 3}, {5, 7} };

	printf("   zippo = %p,   zippo + 1 =%p\n", zippo, zippo + 1);

	printf("zippo[0] = %p, zippo[0] + 1 =%p\n", zippo[0], zippo[0] + 1);

	printf("  *zippo = %p,  *zippo + 1 = %p\n", *zippo, *zippo + 1);

	printf("zippo[0][0] = %d\n", zippo[0][0]);

	printf("   *zippo[0] = %d\n", *zippo[0]);

	printf("   **zippo = %d\n", **zippo);

	printf("   xippo[2][1] = %d\n", zippo[2][1]);

	printf("*(*(zippo + 2) + 1) = %d\n", *(*(zippo + 2) + 1));



	return 0;

}

//output:
//zippo = 0000004B528FF768, zippo + 1 = 0000004B528FF770
//zippo[0] = 0000004B528FF768, zippo[0] + 1 = 0000004B528FF76C
//* zippo = 0000004B528FF768, *zippo + 1 = 0000004B528FF76C
//zippo[0][0] = 2
//* zippo[0] = 2
//* *zippo = 2
//xippo[2][1] = 3
//* (*(zippo + 2) + 1) = 3