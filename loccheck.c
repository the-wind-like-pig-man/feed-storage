//loccheck.c---查看变量被储存在何处

#include <stdio.h>

void mikado(int);//函数原型

int main()
{

	int pooh = 2, bah = 5;//main()函数的局部变量

	printf("In main(), pooh = %d and &pooh = %p\n", pooh, &pooh);

	printf("In main(), bah = %d and &bah = %p\n", bah, &bah);

	mikado(pooh);


	return 0;

}

void mikado(int bah)//定义函数
{

	int pooh = 10;//mikado()的局部变量

	printf("In main(), pooh = %d and &pooh = %p\n", pooh, &pooh);

	printf("In main(), bah = %d and &bah = %p\n", bah, &bah);


}

//output:
//In main(), pooh = 2 and &pooh = 000000CADD1FFC54
//In main(), bah = 5 and &bah = 000000CADD1FFC74
//In main(), pooh = 10 and &pooh = 000000CADD1FFB34
//In main(), bah = 2 and &bah = 000000CADD1FFC30
