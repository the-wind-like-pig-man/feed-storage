#define _CRT_SECURE_NO_WARNINGS 0

//exercises_3.c---输入天数将其转化成周数和天数打印出来

#include <stdio.h>

#define WEEK_PER_DAY 7

int main(int argc, char* argv[])
{

	int days = 0, weeks = 0, input = 0;

	printf("CONVERT DAYS TO WEEKS!\n");

	printf("PLEASE INPUT THE NUMBERS OF DAYS (<= 0 TO QUIT): ");

	//printf("%d", &input);

	scanf("%d", &input);

	while (input > 0)
	{

		weeks = input / WEEK_PER_DAY;

		days = input % WEEK_PER_DAY;

		printf("%d days are %d weeks , %d days.\n", input, weeks, days);

		printf("PLEASE INPUT THE NUMBERS OF DAYS (<= 0 TO QUIT): ");

		scanf("%d", &input);

	}

	printf("PROGRAM EXIT!\n");


	return 0;

}

//output:
//CONVERT DAYS TO WEEKS!
//PLEASE INPUT THE NUMBERS OF DAYS(<= 0 TO QUIT) : 10
//10 days are 1 weeks, 3 days.
//PLEASE INPUT THE NUMBERS OF DAYS(<= 0 TO QUIT) : 24
//24 days are 3 weeks, 3 days.
//PLEASE INPUT THE NUMBERS OF DAYS(<= 0 TO QUIT) : 28
//28 days are 4 weeks, 0 days.
//PLEASE INPUT THE NUMBERS OF DAYS(<= 0 TO QUIT) : 0
//PROGRAM EXIT!