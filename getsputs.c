//getsputs.c---使用gets和puts

#include <stdio.h>

#define STLEN 81

int main()
{

	char words[STLEN];

	puts("Enter a string, please.");

	gets(words);//典型用法

	printf("Your string twice:\n");

	printf("%s\n", words);

	puts(words);

	puts("Done!");



	return 0;

}

//output:
//Enter a string, please.
//I want to learn about string theory!
//Your string twice :
//I want to learn about string theory!
//I want to learn about string theory!
//Done!
