//exercises_17.c---�洢100������

#include <stdio.h>

#include <string.h>

int main()
{

	float chuckie = 100;

	int year = 0;

	do {
	
		chuckie = chuckie + chuckie * 0.08;

		chuckie -= 10;

		year++;

		printf("%f\n", chuckie);
	
	} while (chuckie > 9);

	printf("%d years later. Chuckie's account is %f \n", year, chuckie);

	printf("%d years later. Chuckie's account is null.\n", year, chuckie);

	printf("\nDone!\n");



	return 0;

}

//output:
//98.000000
//95.839996
//93.507195
//90.987770
//88.266792
//85.328133
//82.154381
//78.726730
//75.024872
//71.026863
//66.709015
//62.045738
//57.009399
//51.570152
//45.695763
//39.351425
//32.499538
//25.099503
//17.107462
//8.476059
//20 years later.Chuckie's account is 8.476059
//20 years later.Chuckie's account is null.
//
//Done!
