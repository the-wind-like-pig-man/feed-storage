//4_5_exercises_6.c---输入名和姓，并打印名和姓，以及名和姓的字符数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	char name[40] = { 0 }, surname[40] = { 0 };

	int wname = 0, wsurname = 0;

	printf("Please input your first name: ");

	scanf("%s", name);

	printf("Please input your last name: ");

	scanf("%s", surname);
	//通过scanf()函数分别读取用户的名和姓

	wname = printf("%s", name);

	printf(" ");

	wsurname = printf("%s", surname);//分别打印字符的名和姓，通过返回值记录其字符数量

	//printf("\n%*d %*d", wname, wname, wsurname, wsurname);

	//printf("\n%d* %d*", wname, wname, wsurname, wsurname);

	printf("\n%-*d %-*d", wname, wname, wsurname, wsurname);

	//打印字符数量，由于字符数量不确定，因此用*号修饰符和参数的形式
	//如果使用strlen()函数，则可以不用定义wname和wsurname变量
	//可以直接使用以下代码：
	//printf("\n%*d %*d", strlen(name), strlen(name), strlen(surname), strlen(surname));



	return 0;

}

//output1:
//Please input your first name : Melissa
//Please input your last name : Honeybee
//Melissa Honeybee
//7        8

//output2:
//Please input your first name : David
//Please input your last name : Lei
//David Lei
//5 * 5 *
//
//output:
//Please input your first name : David
//Please input your last name : Lei
//David Lei
//5     3