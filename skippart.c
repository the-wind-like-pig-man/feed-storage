//skippart.c---使用continue跳过部分循环

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	const float MIN = 0.0f;

	const float MAX = 100.0f;

	float score = 0;

	float total = 0.0f;

	int n = 0;

	float min = MAX;

	float max = MIN;

	printf("Please enter the first score (q to quit): ");

	while (scanf("%f", &score) == 1)
	{

		if (score < MIN || score > MAX)
		{

			printf("%0.lf is an invalid value. Try again: ", score);

			continue;//跳转至while循环的测试条件

		}

		printf("Accepting %0.lf:\n", score);

		min = (score < min) ? score : min;

		max = (score > max) ? score : max;

		total += score;

		n++;

		printf("Please enter next score (q to qiut): ");
	}

	if (n > 0)
	{

		printf("Average of %d scores is %0.lf.\n", n, total/n);

		printf("Low = %0.lf, high = %0.lf\n", min, max);

	}

	else
	{

		printf("No valuid scores were entered.\n");

	}



	return 0;

}

//output:
//Please enter the first score(q to quit) : 188
//188 is an invalid value.Try again : 99
//Accepting 99 :
//	Please enter next score(q to qiut) : 70.0
//	Accepting 70 :
//	Please enter next score(q to qiut) : 0
//	Accepting 0 :
//	Please enter next score(q to qiut) : 20
//	Accepting 20 :
//	Please enter next score(q to qiut) : 30
//	Accepting 30 :
//	Please enter next score(q to qiut) : 120
//	120 is an invalid value.Try again : 130
//	130 is an invalid value.Try again : 140
//	140 is an invalid value.Try again : 170
//	170 is an invalid value.Try again : 40
//	Accepting 40 :
//	Please enter next score(q to qiut) : q
//	Average of 6 scores is 43.
//	Low = 0, high = 99
