﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <string.h>

#include <assert.h>

#include <stdio.h>

//int main()
//{
//
//	char* c[] = { "ENTER", "NEW", "POINT", "FIRST" };
//
//	//char** cp = { c + 3, c + 2,c + 1, c };//少了方括号[]
//
//	char** cp[] = { c + 3, c + 2,c + 1, c };
//
//
//	char*** cpp = cp;
//
//	printf("%s\n", **++cpp);//--->POINT
//
//	printf("%s\n", *-- * ++cpp + 3);//cpp的当前值承接上面printf()中的cpp的值
//	//--->ER
//
//	printf("%s\n", *cpp[-2] + 3);//cpp的当前值承接上面printf()中的cpp的值
//	//---*(*(cpp-2))--->ST
//
//	printf("%s\n", cpp[-1][-1] + 1);//cpp的当前值承接上面printf()中的cpp的值
//	//*(*(cpp-1))-1)+1--->EW
//
//	//输出：
//	//POINT
//	//ER
//	//ST
//	//EW
//
//
//	return 0;
//
//}

//
//int main()
//{
//
//	char arr[] = "abcdef";//abcdef\0
//
//	int len = strlen(arr);
//
//	printf("%d\n", len);//6
//
//
//	return 0;
//
//}


//int main()
//{
//
//	//if (strlen("abc") - strlen("abcdef") > 0)//3-6=-3,转化为无符号整型，结果大于>
//
//	if(strlen("abc") > strlen("abcdef"))//<=
//	{
//
//		printf(">\n");
//
//	}
//
//	else
//	{
//
//		printf("<=\n");
//
//	}
//
//
//	return 0;
//
//
//}

//模拟实现更重要

//1.计数器方法
//2.指针-指针
//3.递归的方法

//size_t my_strlen(const char* str)
//{
//
//	size_t count = 0;
//
//	assert(str);
//
//	while(*str != '\0')
//	{
//
//		count++; 
//		
//		str++;
//
//	}
//
//	return count;
//
//}
//
//int main()
//{
//
//	char arr[] = "abcdef";
//
//	size_t n = my_strlen(arr);
//
//	printf("%u\n", n);//6
//
//
//	return 0;
//
//}

//模拟实现字符串拷贝暴力版

//char* my_strcpy(char* dest, const char* src)
//{
//	
//	assert(dest && src);
//
//	//assert(dest);
//
//	//assert(src);
//
//	char* ret = dest;
//
//	while (*src)
//	{
//
//		*dest++ = *src++;
//
//	}
//
//	*dest = *src;
//
//	return ret;
//
//}


//模拟实现字符串拷贝简洁版
//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest);
//
//	assert(src);
//
//	char* ret = dest;
//
//	while (*dest++ = *src++)
//	{
//
//		;
//
//	}
//
//	return ret;
//
//}

//字符串追加
//
//
//char* my_strcat(char* dest, const char* src)
//{
//
//	char* ret = dest;
//
//	assert(dest && src);
//	//1.找到目标空间的末尾\0 
//
//	//while (*dest != '0')//为字符0时，不进行追加
//	
//	while (*dest != '\0')
//
//	{
//
//		dest++;
//
//	}
//	//2.拷贝字符串
//
//	while (*dest++ = *src++)
//	{
//
//
//		;
//
//	}
//
//	return ret;
//
//
//}

//int main()
//{
//
//
//	char arr1[20] = "hello ";
//
//	//strcat(arr1, "world");//字符串追加函数
//
//	my_strcat(arr1, "world");//字符串追加函数
//
//
//	printf("%s\n", arr1);
//
//	//char arr1[] = "abcdef";
//
//	//char arr2[20] = { 0 };
//
//	//my_strcpy(arr2, arr1);
//
//	//printf("%s\n", arr2);//输出abcdef
//
//
//
//	return 0;
//
//}

//自己给自己追加？
//int main()
//{
//
//
//	char arr1[20] = "hello ";
//
//	//strcat(arr1, "world");//字符串追加函数
//
//	//my_strcat(arr1, "world");//字符串追加函数
//
//	strcat(arr1, arr1);//字符串追加函数
//
//	printf("%s\n", arr1);//可以实现，但不建议这样做
//
//	//char arr1[] = "abcdef";
//
//	//char arr2[20] = { 0 };
//
//	//my_strcpy(arr2, arr1);
//
//	//printf("%s\n", arr2);//输出abcdef
//
//
//
//	return 0;
//
//}

