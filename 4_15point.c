﻿

#include <stdio.h>


//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };//arr1,arr2,arr3三个数组在内存中不是连续存放的
//
//	int arr2[] = { 2,3,4,5,6 };
//
//	int arr3[] = { 3,4,5,6,7 };
//
//	int* parr[3] = { arr1,arr2,arr3 };//不是二维数组，是指针数组
//
//	//0 1 2
//
//	int i = 0;
//
//	for (i = 0; i < 3; i++)
//	{
//
//      //*(p+i) <---> p[i]
// 
//		int j = 0;
//
//		for (j = 0; j < 5; j++)
//		{
//
//			//printf("%d ", *(parr[i] + j));
// 
//            printf("%d ",parr[i][j]);//与上面输出结果相同
//
//
//		}
//
//		printf("\n");
//
//	}
//
//
//	return 0;
//
//}

//int main()
//{
//
//	char ch = 'w';
//
//	char* pc = &ch;
//
//	*pc = 'w';
//
//	printf("%c\n", *pc);
//
//
//	return 0;
//
//}

//int main()
//{
//
//	char ch = 'w';
//
//	char* pc = &ch;
//
//	*pc = 'b';
//
//	printf("%c\n", ch);
//
//	const char* p = "abcdef";//把字符串首字符a的地址赋给了指针p
//
//	char arr[] = "abcdef";
//
//	//*p = 'w';//const修饰p，p指向的内容无法被修改
//
//	printf("%s\n", p);
//
//	return 0;
//
//}
//
//int main()
//{
//
//	const char* p1 = "abcdef";
//
//	const char* p2 = "abcdef";
//
//	char arr1[] = "abcdef";
//
//	char arr2[] = "abcdef";
//
//	if (p1 == p2)
//	{
//
//		printf("p1 == p2\n");//输出p1==p2，因p1,p2指向的都是a在内存中的地址
//
//	}
//
//	else
//	{
//
//		printf("p1 != p2");
//
//	}
//
//	if (arr1 == arr2)
//	{
//
//		printf("arr1 == arr2\n");
//
//	}
//
//	else
//	{
//
//		printf("arr1 != arr2");//输出arr1!=arr2,因arr1,arr2代表两个数组在内存中首元素的地址
//
//	}
//
//
//	return 0;
//
//}


//再次讨论数组名

//int main()
//{
//	char* arr[5] = { 0 };
//
//	char* (*pc)[5] = &arr;//*pc是指向字符数组指针变量的数组指针
//
//	char ch = 'w';
//
//	char* p1 = &ch;
//
//	char** p2 = &p1;//p2是指向一级字符指针变量p1的指针
//
//	int arr[10] = { 0 };
//
//
//	int* p = arr;
//
//	int (*p2)[10] = &arr;
//
//	//整型指针是用来存放整型地址的
//	//字符指针是用来存放字符地址的
//	//数组指针是用来存放数组的地址的
//	//
//	//
//	printf("%p\n", arr);//0000006B611BF688
//
//	printf("%p\n", &arr[0]);//0000006B611BF688
//
//	printf("%p\n", &arr);//0000006B611BF688
//
//	int sz = sizeof(arr);
//
//		printf("%d\n", sz);//40
//
//
//	return 0;
//
//}

//数组名通常表示的是数组首元素的地址
//但是有两个例外
//1.sizeof(数组名)，这里的数组名表示整个数组，计算的是整个数组的大小
//2.&（数组名），这里的数组名表示的依然是整个数组，所以&数组名取出的是整个数组的地址


