//wordcnt.c---统计字符数、单词数、行数

#include <stdio.h>

#include <ctype.h>//为isspace()函数提供原型

#include <stdbool.h>//为bool、true、false提供定义

#define STOP '|'

int main()
{

	char c = 0;//读入字符

	char prev = 0;//读入前一个字符

	long n_chars = 0l;//字符数

	int n_words = 0;//单词数

	int n_lines = 0;//行数

	int p_lines = 0;//不完整的行数

	bool inword = false;//如果C在单词中，inword等于true

	printf("Please enter text to be analyzed (| to be terminate):\n");

	prev = '\n';//用于识别完整的行

	while ((c = getchar()) != STOP)
	{

		n_chars++;//统计字符

		if (c == '\n')
		{

			n_lines++;//统计行

		}

		if (!isspace(c) && !inword)
		{

			inword = true;//开始一个新的单词

			n_words++;//统计单词

		}

		if (isspace(c) && inword)
		{

			inword = false;//达到单词的末尾

			prev = c;//保存字符的值
		}

		
	}

	if (prev != '\n')
	{

		p_lines = 1;

	}

	printf("characters = %ld, words = %d, lines = %d, ",
		n_chars, n_words, n_lines);

	printf("partial lines = %d\n", p_lines);



	return 0;

}

//output:
//Please enter text to be analyzed(| to be terminate) :
//	Reason is a
//	powerful servant but
//	an inadequate master.
//	|
//	characters = 55, words = 9, lines = 3, partial lines = 0

//output:
//Please enter text to be analyzed(| to be terminate) :
//	I love  China   very much.
//	|
//	characters = 27, words = 5, lines = 1, partial lines = 0
