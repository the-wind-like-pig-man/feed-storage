//running.c---A useful program for runners

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

const int S_PER_M = 60;//1分钟的秒数

const int S_PER_H = 3600;//1小时的秒数

const double M_PER_K = 0.62137;//1公里的英里数

int main()
{


	//double distk , distm ;//跑过的距离（分别以公里和英里为单位）

	//double rate ;//平均速度（以英里/小时位单位）

	//int min, sec;//跑步用时（以分钟和秒为单位）

	//int time;//跑步用时（以秒为单位）

	//double mtime;//跑1英里需要的时间，以秒为单位

	//int mmin, msec;//跑1英里需要的时间，以分钟和
	//以上未初始化时不能正常编译

	double distk = 0, distm = 0;//跑过的距离（分别以公里和英里为单位）

	double rate = 0;//平均速度（以英里/小时位单位）

	int min, sec = 0;//跑步用时（以分钟和秒为单位）

	int time = 0;//跑步用时（以秒为单位）

	double mtime = 0;//跑1英里需要的时间，以秒为单位

	int mmin = 0, msec = 0;//跑1英里需要的时间，以分钟和秒为单位

	printf("This program converts your time for a metric race\n");

	printf("to a time for running a mile and to your average\n");

	printf("speed in miles per hour.\n");

	printf("Please enter, in kilometers, the distance run.\n");

	//scanf("%1f", &distk);//%1f表示读取一个double类型的值//是lf，不是1f

	scanf("%lf", &distk);//%1f表示读取一个double类型的值


	printf("Next enter the time in minutes and seconds.\n");

	printf("Begin by entering the minutes.\n");

	scanf("%d", &min);

	printf("Now enter the seconds.\n");

	scanf("%d", &sec);

	time = S_PER_M * min + sec;//把时间转换成秒

	distm = M_PER_K * distk;//把公里转换成英里

	//rate = distm / time * S_PER_M;//英里/秒×秒/小时=英里/小时---WRONG

	rate = distm / time * S_PER_H;//英里/秒×秒/小时=英里/小时

	//mtime = (double)mtime / S_PER_M;//求分钟数---WRONG

	mtime = (double)time / distm;//时间/距离=跑一英里所需的时间

	//msec = (int) mtime / S_PER_M;//求出分钟数

	//msec = (int)mtime % S_PER_M;//求出剩余的秒数

	mmin = (int) mtime / S_PER_M;//求出分钟数

	msec = (int) mtime % S_PER_M;//求出剩余的秒数

	//printf("You ran %1.2f km (%1.2f miles) in %d min, %d sec.\n",
	//	distk, distm, min, sec);

	printf("You ran %1.2f km (%1.2f miles) in %d min, %d sec.\n",
		distk, distm, min, sec);

	printf("That pace corresponds to run a mile in %d min, ",
		mmin);

	printf("%d sec.\nYour average speed was %1.2f mph.\n", msec,
		rate);



	return 0;

}

//output1:(?)
//This program converts your time for a metric race
//to a time for running a mile and to your average
//speed in miles per hour.
//Please enter, in kilometers, the distance run.
//10.0
//Next enter the time in minutes and seconds.
//Begin by entering the minutes.
//Now enter the seconds.
//You ran 0.00 km(0.00 miles) in 0 min, 0 sec.
//That pace corresponds to run a mile in 0 min, 0 sec.
//Your average speed was inf mph.

//output:---no focus no wright
//This program converts your time for a metric race
//to a time for running a mile and to your average
//speed in miles per hour.
//Please enter, in kilometers, the distance run.
//10.0
//Next enter the time in minutes and seconds.
//Begin by entering the minutes.
//36
//Now enter the seconds.
//23
//You ran 10.00 km(6.21 miles) in 36 min, 23 sec.
//That pace corresponds to run a mile in 5 min, 51 sec.
//Your average speed was 10.25 mph.