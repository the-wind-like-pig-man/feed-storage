//binary.c---以二进制形式打印整数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void to_binary(unsigned long n);

int main()
{

	unsigned long number = 0;

	printf("Please enter an integer (q to quit): \n");

	while (scanf("%lu", &number) == 1)
	{

		printf("Binary equivalent: ");

		to_binary(number);

		putchar('\n');

		printf("Please enter an integer (q to quit): \n");

	}

	printf("Done!\n");



	return 0;

}

void to_binary(unsigned long n)//递归函数
{

	int r = 0;

	r = n % 2;

	if (n >= 2)
	{

		to_binary(n / 2);

	}

	putchar(r == 0 ? '0' : '1');

	return n;
}

//output:
//Please enter an integer(q to quit) :
//	2
//	Binary equivalent : 10
//	Please enter an integer(q to quit) :
//	5
//	Binary equivalent : 101
//	Please enter an integer(q to quit) :
//	4
//	Binary equivalent : 100
//	Please enter an integer(q to quit) :
//	6
//	Binary equivalent : 110
//	Please enter an integer(q to quit) :
//	q
//	Done!
