//exercises_11.c---在数组中读入八个整数，后倒序打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{
	int i = 0;

	int data[8] = { 0 };

	printf("Please enter 8 integer data (seprate by blank): ");

	for (i = 0; i < 8; i++)
	{

		scanf("%d", &data[i]);

	}

	printf("OK! The reverse data is : ");

	for (i = 7; i >= 0; i--)
	{

		printf(" %d", data[i]);

	}

	printf("\nDone!\n");



	return 0;

}

//output:
//Please enter 8 integer data(seprate by blank) : 1 2 3 4 5 6 7 8
//OK!The reverse data is : 8 7 6 5 4 3 2 1
//Done!