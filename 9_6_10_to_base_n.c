//to_base_n().c---该函数接收两个参数，且第二个参数介于2~10之间，然后以第二个参数中指定的进制打印第一个数的数值

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void to_base_n(unsigned long n, unsigned short t);
//进制转换，待转换数类型是正整数，因此使用无符号类型标识

int main()
{

	unsigned long number = 0;

	unsigned short target = 0;

	printf("Please enter the integer and N for notation(q to quit): ");

	while ((scanf("%lu %hu", &number, &target)) == 2)
	{

		if (target < 2 || target > 10)
		{

			printf("Please input N between 2~10!\n");

			printf("Please enter the integer and N for notation(q to quit): ");

			continue;

		}

		printf("Convert %lu to %hu notation is: ", number, target);

		to_base_n(number, target);

		putchar('\n');

		printf("Please enter the integer and N for notation(q to quit): ");

		printf("Enjoy you trip!\n");

	}


	return 0;

}

void to_base_n(unsigned long n, unsigned short t)
{

	if (t < 2 || t > 10)
	{

		printf("The function only convert 2 ~ 10.\n ");

		return;//函数内部的参数判断

	}

	int r = 0;

	r = n % t;

	if (n >= 2)
	{

		to_base_n(n / t, t);

	}

	printf("%d", r);


}

//output:
//Please enter the integer and N for notation(q to quit) : 100 2
//Convert 100 to 2 notation is : 1100100
//Please enter the integer and N for notation(q to quit) : 16 8
//Convert 16 to 8 notation is : 020
//Please enter the integer and N for notation(q to quit) : 16 2
//Convert 16 to 2 notation is : 10000
//Please enter the integer and N for notation(q to quit) : q

//output2:
//Please enter the integer and N for notation(q to quit) : 4 2
//Convert 4 to 2 notation is : 100
//Please enter the integer and N for notation(q to quit) : Enjoy you trip!
//16 8
//Convert 16 to 8 notation is : 020
//Please enter the integer and N for notation(q to quit) : Enjoy you trip!
//100 8
//Convert 100 to 8 notation is : 144
//Please enter the integer and N for notation(q to quit) : Enjoy you trip!
//q
