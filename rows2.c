//rows2.c---依赖外部循环的嵌套循环

#include <stdio.h>

int main()
{

	//const int ROWS = 8;

	const int ROWS = 26;

	//const int CHARS = 8;

	const int CHARS = 26;

	int row = 0;

	char ch = '0';

	for (row = 0; row < ROWS; row++)
	{

		for (ch = ('A' + row); ch < ('A' + CHARS); ch++)
		{

			printf("%c", ch);

		}

		printf("\n");

	}


	return 0;

}

//output:
//ABCDEFGHIJKLMNOPQRSTUVWXYZ
//BCDEFGHIJKLMNOPQRSTUVWXYZ
//CDEFGHIJKLMNOPQRSTUVWXYZ
//DEFGHIJKLMNOPQRSTUVWXYZ
//EFGHIJKLMNOPQRSTUVWXYZ
//FGHIJKLMNOPQRSTUVWXYZ
//GHIJKLMNOPQRSTUVWXYZ
//HIJKLMNOPQRSTUVWXYZ
//IJKLMNOPQRSTUVWXYZ
//JKLMNOPQRSTUVWXYZ
//KLMNOPQRSTUVWXYZ
//LMNOPQRSTUVWXYZ
//MNOPQRSTUVWXYZ
//NOPQRSTUVWXYZ
//OPQRSTUVWXYZ
//PQRSTUVWXYZ
//QRSTUVWXYZ
//RSTUVWXYZ
//STUVWXYZ
//TUVWXYZ
//UVWXYZ
//VWXYZ
//WXYZ
//XYZ
//YZ
//Z