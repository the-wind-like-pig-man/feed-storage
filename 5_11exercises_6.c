#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int count = 0, sum = 0;

	printf("Please enter the number of days you work: ");

	scanf("%d", &count);

	while (count > 0)
	{

		sum = sum + count * count;

		count--;

	}

	printf("You earned $ %d total!\n", sum);

	printf("PROGRAM EXIT!\n");

	return 0;

}

//output:
//Please enter the number of days you work : 3
//You earned $ 14 total!
//PROGRAM EXIT!