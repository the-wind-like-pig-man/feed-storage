﻿

#include <stdio.h>

//一维数组传参
//void test(int arr[])//ok
//{}
//
//void test(int arr[10])//ok
//{}
//
//void test(int* arr)//ok
//{}
//
//void test2(int* arr[20])//ok
//{}
//void test2(int** arr)//ok
//{}
//
//int main()
//{
//
//	int arr[10] = { 0 };
//
//	int* arr2[20] = { 0 };
//
//	test(arr);
//
//	teat2(arr2);
//
//	return 0;
//
//}

//形参的二维数组，行可以省略，列不可以省略
//二维数组传参：二维数组传参，函数形参的设计只能省略第一行
//因为对于一个二维数组，可以不知道有多少行，但是必须知道有多少列，这样才方便运算
//void test(int arr[][5])//ok
//{}
//
//void test(int arr[][])//not ok
//{}
//
//void test(int* arr)//not ok
//{}
//
//void test(int* arr[5])//not ok
//{}
//
//void test(int(*arr)[5])//ok
//{}
//
//void test(int** arr)//not ok
//{}
//
//int main()
//{
//
//	int arr[3][5] = { 0 };
//	//二维数组的数组名，表示首元素的地址，其实是第一行的地址。第一行是一个一维数组
//	//
//
//	test(arr);
//
//
//	return 0;
//
//}