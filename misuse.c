//misuse.c---错误的使用函数

#include <stdio.h>

int imax();//旧式函数声明

int main()
{

	printf("The maxinum of %d and %d is %d.\n", 3, 5, imax(3));

	//printf("The maxinum of %d and %d is %d.\n", 3, 5, imax(3, 5));

	printf("The maxinum of %d and %d is %d.\n", 3, 5, imax(3.0, 5.0));



	return 0;

}

int imax(n, m)

int n, m;
{

	return(n > m ? n : m);

}

//output:
//The maxinum of 3 and 5 is 3.
//The maxnum of 3 and 5 is 5.

//output1:
//The maxinum of 3 and 5 is 3.
//The maxinum of 3 and 5 is 0.