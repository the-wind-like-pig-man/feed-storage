//exercises_4.c---输入字符流，然后打印每个输入的字符及其相应的ASCII十进制

#include <stdio.h>

int main()
{

	int counter = 0;

	char ch = 0;

	while ((ch = getchar()) != EOF)
	{

		if (counter++ == 10)
		{

			printf("\n");

			counter = 1;//输入计数器，并判断是否打印换行符

		}

		if (ch >= '\040')
		{

			printf("\'%c\'--%3d", ch, ch);//大于空格字符的可显示为字符的处理和判断

		}

		else if (ch = '\n')
		{

			printf("\\n--\\n\n");

			counter = 0;//换行符的处理

		}

		else if (ch = '\t')
		{

			printf("\\t--\\t");//制表符的处理

		}

		else
		{

			printf("\'%c\'--^%c", ch, (ch + 64));//其他非显示字符的处理

		}
	}


	return 0;

}

//output:
//hello world!hello China!
//'h'--104'e'--101'l'--108'l'--108'o'--111' '-- 32'w'--119'o'--111'r'--114'l'--108
//'d'--100'!'-- 33'h'--104'e'--101'l'--108'l'--108'o'--111' '-- 32'C'-- 67'h'--104
//'i'--105'n'--110'a'-- 97'!'-- 33\n--\n
//
//\n--\n
//
//\n--\n
//
//\n--\n
//EOF
//'E'-- 69'O'-- 79'F'-- 70\n--\n
//
//\n--\n
//'EOF'
//'''-- 39'E'-- 69'O'-- 79'F'-- 70'''-- 39\n--\n
