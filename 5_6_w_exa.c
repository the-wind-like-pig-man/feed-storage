﻿

#include <stdio.h>

//int main()
//{
//
//	int a = 20;
//
//	int* pa = &a;
//
//	printf("%d\n", a);//--->20
//
//	//& <---> *
//
//	char ch = 'w';
//
//	char* pc = &ch;
//
//
//	return 0;
//
//}

int Add(int x, int y)
{

	return x + y;

}

int main()
{

	printf("%p\n", &Add);//--->00007FF7C85613D4

	int (*pf)(int, int) = &Add;

	//函数的地址存放到函数指针变量中


	return 0;

}