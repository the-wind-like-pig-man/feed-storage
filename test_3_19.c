﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <stdlib.h>

#include <assert.h>

//                               8
//                  7                           6
//         6                  5            5         4
//      5          4       4      3     4      3   3   2
//   4      3    3   2   3   2   2 1  3   2   2 1 2 1 1 0
//  3   2   2 1  2 1 1 0 2 1 1 0 1 0  2 1 1 0 1 0 1 0
// 2 1 1 0 1 0  1 0     1 0          1 0
//1 0
//共计=67次

//int cnt = 0;
//
//int fib(int n)
//{
//
//	cnt++;
//
//	if (n == 0)
//	{
//
//		return 1;
//
//	}
//
//	else if (n == 1)
//	{
//
//		return 2;
//
//	}
//
//	else
//	{
//
//		return fib(n - 1) + fib(n - 2);
//
//	}
//
//}
//
//void main()
//{
//
//	fib(8);
//
//	printf("%d\n", cnt);
//
//	return 0;
//
//}
//
//int main()
//{
//
//	int x = 1;
//
//	do {
//	
//		printf("%2d\n", x++);//后置加加，先打印1，后加1，死循环
//	
//	} while (x--);
//
//	return 0;
//
//}

//
//int a = 1;
//
//void test()
//{
//
//	int a = 2;//局部变量优先，但仅局部有效
//
//	a += 1;
//
//}
//
//int main()
//{
//
//	test();
//
//	printf("%d\n", a);//输出 a=1
//
//	return 0;
//
//}

//注释是给程序员看的
//
//
//
//int main()
//{
//	int count = 0;
//
//	int x = 0;
//
//	int y = 0;
//
//	for (x = 0, y = 0; (y = 123) && (x < 4); x++)
//	{
//
//		;
//		count++;
//
//	}
//
//	printf("%d\n", count);
//
//
//
//	return 0;
//
//}

//char a;int b;float c;double d;
//a * b + d - c --->数据类型是？
//a会整型提升，(a*b)+d --->double 算术转换
//(a*b+d) - c --->c发生算术转换
//最终为double类型

//求两个数的最小公倍数
//

//int main()
//{
//
//	int a = 0;
//
//	int b = 0;
//
//	scanf("%d %d", &a, &b);
//
//	//计算a和b的最小公倍数
//	int m = (a > b ? a : b);
//
//	while (1)
//	{
//
//
//		if (m % a == 0 && m % b == 0)
//		{
//
//			break;
//
//		}
//
//		m++;
//	}
//
//	//打印最小公倍数
//
//	printf("%d\n", m);
//
//
//	return 0;
//
//}

//int main()
//{
//
//	int a = 0;
//
//	int b = 0;
//
//	scanf("%d %d", &a, &b);
//
//	//计算a和b的最小公倍数
//	//int m = (a > b ? a : b);
//
//	int i = 1;
//
//	while (a * i % b)
//	{
//
//		i++;
//
//	}
//
//	//打印最小公倍数
//
//	printf("%d\n", a * i);
//
//
//	return 0;
//
//}
//

//
//
//
//1.把整个字符串逆序
//2.把每个单词逆序
//例如输入： I Like beijing.
//输出:Beijing. Like I
//

//void reverse(char* left, char* right)
//{
//
//	assert(left);
//
//	assert(right);
//
//	while (left < right)
//	{
//
//		char tmp = *left;
//
//		*left = *right;
//
//		*right = tmp;
//
//		left++;
//
//		right--;
//
//	}
//
//}
//
//int main()
//{
//
//	char arr[101] = { 0 };
//
//	//输入字符串
//
//	gets(arr);//I Like Beijing.
//
//	//将字符串逆序
//
//	int len = strlen(arr);
//
//	//1.逆序整个字符串
//
//	reverse(arr, arr + len - 1);
//
//	//2.逆序每一个单词
//
//	char* start = arr;
//
//	while (*start)
//	{
//
//
//		char* end = start;
//
//		while ((*end != ' ') && (*end != '\0'))
//		{
//
//			end++;
//
//		}
//
//		reverse(start, end - 1);
//
//		if (*end != '\0')
//
//			end++;
//
//		start = end;
//
//	}
//
//	//输出逆序后的字符串
//
//	printf("%s\n", arr);//Beijing. Like I
//
//
//	return 0;
//
//}


