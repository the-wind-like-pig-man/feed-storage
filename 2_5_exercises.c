//exercises_3.c---把年龄转换为天数，并打印

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define DAYS_PER_YEAR 365//利用预编译指令指定一年的天数

int main()
{

	int age = 0, days = 0;

	printf("Please enter your age: \n");

	scanf("%d", &age);//用户输入年龄

	days = age * DAYS_PER_YEAR;

	printf("Your age is %d, and it is %d days.\n", age, days);





	return 0;

}

//output:
//Please enter your age :
//35
//Your age is 35, and it is 12775 days.