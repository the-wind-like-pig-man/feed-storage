//strings1.c---字符串1程序

#include <stdio.h>

#define MSG "I am a symbolic string constant."

#define MAXLENGTH 81

int main()
{

	char words[MAXLENGTH] = "I am a sting in an array.";

	const char* pt1 = "Something is pointing at me.";

	puts("Here are some strings: ");

	puts(MSG);

	puts(words);

	puts(pt1);

	words[8] = 'p';//没改变字符数组相应位置的字符

	puts(words);



	return 0;

}

//output:
//Here are some strings :
//I am a symbolic string constant.
//I am a sting in an array.
//Something is pointing at me.
//I am a sping in an array.