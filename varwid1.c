
//varwid.c---使用变宽输出字段

//#define CRT_SECURE_NO_WARNINGS 0 //少了下划线"_"

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	unsigned width = 0;

	unsigned precision = 0;

	int number = 256;

	double weight = 242.5;

	printf("Enter a field width:\n");

	scanf("%d", &width);

	printf("The number is :%*d:\n", width, number);

	printf("Now enter a width and precision:\n");

	scanf("%d %d", &width, &precision);

	printf("Weight = %*.*f\n", width, precision, weight);

	printf("Done!\n");

	return 0;

}
//
//output:
//Enter a field width :
//6
//The number is : 256 :
//	Now enter a width and precision :
//	8 3
//	Weight = 242.500
//	Done!