﻿

#include <stdio.h>


//人
//声明的结构体类型
// 

struct Peo//结构体类型
{

	char name[20];

	char tele[12];

	//int sex[10];//女、男、保密

	char sex[10];//女、男、保密


	int high ;

}p3,p4;

struct Peo p5, p6;

struct St
{

	struct Peo p;//结构体中可以包含结构体

	int num;

	float f;


};

//struct Peo
//{
//
//	char name[20];
//
//	char tele[12];
//
//	char sex[5];
//
//	int high;
//
//
//}p1, p2;//p1和p2是两个全局的结构体变量



int main()
{

	//struct Peo p1 = { 0 };//p1是结构体变量，结构体变量的创建

	struct Peo p1 = { "zhangsan", "1523610***8", "male", 180 };//结构体变量的初始化

	struct St s = { { "lisi", "15888888888", "female", 166 }, 100, 3.14f };

	printf("%s %s %s %d\n", p1.name, p1.tele, p1.sex, p1.high);

	printf("%s %s %s %d %f\n", s.p.name, s.p.tele, s.p.sex, s.p.high, s.num, s.f);

	return 0;

}