//scan_str.c---ʹ��scanf()����

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	char name1[11], name2[11];

	int count = 0;

	printf("Please enter 2 names.\n");

	count = scanf("%5s %10s", name1, name2);

	printf("I read the %d names %s and %s.\n", count, name1, name2);


	return 0;

}

//output1:
//Please enter 2 names.
//Jesse Jukes
//I read the 2 names Jesse and Jukes.

//output2:
//Please enter 2 names.
//Liza Applebottham
//I read the 2 names Liza and Applebotth.

//output3:
//Please enter 2 names.
//Portenisa Callowit
//I read the 2 names Porte and nisa.