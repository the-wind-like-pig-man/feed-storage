//exercises_10.c---输出两个整数的平方和，当下限小于或等于上限时，终止程序

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int lower = 0, upper = 0;

	printf("Enter lower and upper integer limits: ");

	scanf("%d %d", &lower, &upper);

	while (upper > lower)
	{

		int sum = 0;

		for (int i = lower; i <= upper; i++)
		{

			sum = sum + i * i;

		}

		//printf("The sum of the squares form %d to %d is %d.\n", lower, upper);

		printf("The sum of the squares form %d to %d is %d.\n", lower, upper, sum);


		printf("Enter lower and upper integer limits: ");

		scanf("%d %d", &lower, &upper);

	}

	printf("Done!");

	return 0;

}

//output:---upper<lower
//Enter lower and upper integer limits : 4 6
//Done!

//output:
//Enter lower and upper integer limits : 2 4
//The sum of the squares form 2 to 4 is 29.
//Enter lower and upper integer limits : 5 9
//The sum of the squares form 5 to 9 is 255.
//Enter lower and upper integer limits : 3 25
//The sum of the squares form 3 to 25 is 5520.
//Enter lower and upper integer limits : 5 5
//Done!