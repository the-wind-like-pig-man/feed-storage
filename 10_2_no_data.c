//no_data.c---为初始化数组

#include <stdio.h>

#define SIZE 4

int main()
{

	int no_data[SIZE];//未初始化数组

	int i = 0;

	printf("%2s%14s\n", "i", "no_data[i]");

	for (i = 0; i < SIZE; i++)
	{

		//printf("%2%14d\n", i, no_data[i]);

		printf("%2d%14d\n", i, no_data[i]);


	}



	return 0;

}

//output:
//i    no_data[i]
//% 14d
//% 14d
//% 14d
//% 14d

//output:
//i    no_data[i]
//0 - 858993460
//1 - 858993460
//2 - 858993460
//3 - 858993460
