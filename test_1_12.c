﻿#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <stdlib.h>

#include <time.h>//时间戳函数头文件


//void fun(int a[])
//{
//	printf("%zu", sizeof(a));//a是指针变量，大小是4/8个字节
//}
//
//int main()
//{
//	int a[10] = { 0 };
//
//	printf("%zu\n", sizeof(a));//数组大小40个字节
//
//	printf("%zu\n", sizeof(a[1]));//数组首元素大小4个字节
//
//	fun(a);
//
//	return 0;
//}

//猜数字游戏*****
//电脑产生一个随机数（1~100）
//猜大了
//猜小了
//猜对了

void menu()
{
	printf("*********************************\n");

	printf("****************1.Play***********\n");

	printf("****************0.Exit***********\n");


}

//0~RAND_MAX(32767)
void game()
{
	//RAND_MAX;

	int guess = 0;

	//srand((unsigned int)time(NULL));//在此只是产生随机数

	//1.生成随机数
	//0~99--->1~100
	int ret = rand()%100+1;//生成随机数的函数

	//printf("%d\n", ret);
	
	//2.猜数字

	while (1)
	{
		printf("Guess the number:>");

		scanf("%d", &guess);

		if (guess < ret)
		{

			printf("Guess smaller:\n");//无大括号时，猜不出来
		}

		//else if ("guess > ret")//条件不能加双引号
		else if (guess > ret)//条件不能加双引号

		{
			printf("Guess biger:\n");//无大括号时，猜不出来
		}

		else
		{
			printf("Congratulations!Guess right!\n");

			break;

		}
	}
}

//指针
//int *p = NULL---空指针，指针指向数字0
//int a = 0;

int main()
{
	int input = 0;

	srand((unsigned int)time(NULL));//只需调用一次

	do
	{
		menu();

		printf("Please choose:>");

		scanf("%d", &input);

		switch (input)
		{
		case 1://case和序号间需要加空格

			game();//猜数字的整个逻辑

			printf("Guess words:\n");

			break;

		case 0:

			printf("Exit the game:\n");

			break;

		default:

			printf("Choose wrong.Choose again!\n");

			break;
		}
	} while (input);

	return 0;
}