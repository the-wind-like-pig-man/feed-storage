//praise.c
//如果编译器不识别%zd，尝试换成%u或者%lu

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <string.h>

#define PRAISE "You are an extraordinary being."

int main()
{

	char name[40];

	printf("What's your name?");

	scanf("%s", name);

	printf("Hello, %s. %s\n", name, PRAISE);

	printf("Your name of %zd letters occupies %zd memory cells.\n",
		
		strlen(name), sizeof(name));

	printf("The phrase of praise has %zd letters ",

		strlen(PRAISE));

	printf("and occupies %zd memory cells.\n", sizeof PRAISE);




	return 0;

}

//output:
//What's your name?Serendipity Chance
//Hello, Serendipity.You are an extraordinary being.
//Your name of 11 letters occupies 40 memory cells.
//The phrase of praise has 31 letters and occupies 32 memory cells.
