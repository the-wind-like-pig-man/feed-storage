//skiptwo.c---跳过输入中的前两个数

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int n = 0;

	printf("Please enter three integers:\n");

	scanf("%*d %*d %d", &n);

	printf("The last integer was %d\n", n);


	return 0;

}
//
//output:
//Please enter three integers :
//2013 2014 2015
//The last integer was 2015