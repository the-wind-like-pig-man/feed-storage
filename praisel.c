//praisel.c---使用不同类型的字符串

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define PRAISE "You are an extraordinary being."

int main()
{

	char name[40] = { 0 };

	printf("What's your name?");

	scanf("%s", name);

	printf("Hello, %s. %s\n", name, PRAISE);


	return 0;

}

//output:
//What's your name?Angela Plains
//Hello, Angela.You are an extraordinary being.