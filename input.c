//input.c---何时使用&

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	int age;//变量

	float assets;//变量

	char pet[30] = { 0 };//字符数组，用于存储字符串

	printf("Enter your age, assets, and favorite pet.\n");

	scanf("%d %f", &age, &assets);//这里要使用&

	scanf("%s", pet);//字符数组不适用&

	printf("%d $%.2f %s\n", age, assets, pet);




	return 0;

}

//output:
//Enter your age, assets, and favorite pet.
//38 92360.88 llama
//38 $92360.88 llama
