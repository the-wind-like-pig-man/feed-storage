//exercises_8.c---编程实现两个浮点数之差除以二者的乘积

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	float x = 0, y = 0, z = 0;

	printf("Please enter the two float data(seprate by blank): ");

	while (scanf("%f %f", &x, &y) == 2)
	{

		printf("The answer is %f\n", z = (x - y) / (x * y));

		printf("Please enter the two float data(seprate by blank): ");

	}

	printf("The program end!\n");


	return 0;

}

//output:
//Please enter the two float data(seprate by blank) : 1.0 2.0
//The answer is - 0.500000
//Please enter the two float data(seprate by blank) : 2.0 3.0
//The answer is - 0.166667
//Please enter the two float data(seprate by blank) : 5.0 2.0
//The answer is 0.300000
//Please enter the two float data(seprate by blank) : a 1.0
//The program end!