//exercixes_6.c---读取第一个非空白字符

#include <stdio.h>

char get_first(void);//函数声明，返回类型为字符类型

int main()
{

	char ch = 0;

	ch = get_first();

	printf("%c\n", ch);


	return 0;

}

char get_first()
{

	char ch = 0;

	do {
	
		ch = getchar();

	} while (ch == ' ' || ch == '\n' || ch == '\t');
	//通过do...while循环来读取标准输入，如果为三种空白字符中的一种，
	//则再次读取下一个字符，直到非空白字符才返回ch
	//
	//
	//

	return ch;

}
//
//output:
// '\n' '\t''\n'abcdef
//'

//output:
// \n \t \n \t     i love China
//\