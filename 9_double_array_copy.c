//9_double_array_copy.c---编写一个程序，初始化一个double类型的3x5的二维数组，使用一个处理变长数组的函数将其复制到另一个
//二维数组中，还要编写一个以变长数组为形参的函数以显示两个数组的内容。这两个函数应该能处理任意NxM的数组（如果编译器不支持变长
//数组，则使用传统C函数处理Nx5的数组）

#include <stdio.h>

//void copy_array(int n, int m, double target[n][m], const double source[n][m]);

void copy_array(int n, double target[][5], const double source[][5]);

void show_array(int n, const double[][5]);
//以变长数组作为函数的参数，需要以二维数组的下标作为参数，在形参的数组中也需要使用相同的
//形参名作为下标来标识出数组的长度，即(intn, int m, const double array[n][m]),关键字 
//const可以防止源数组在函数内的误操作

int main()
{

	int n = 3;

	int m = 5;

	double target[3][5], source[][5] = {
	
		{0.2, 0.4, 2.4, 3.5, 6.6},

		{8.5, 8.2, 1.2, 1.6, 2.4},

		{9.1, 8.5, 2.3, 6.1, 8.4},
	
	
	};

	copy_array(n, target, source);

	show_array(n, target);


	return 0;

}

void copy_array(int n, double target[][5], const double source[][5])
{

	for (int i = 0; i < n; i++)
	{

		for (int j = 0; j < 5; j++)
		{

			target[i][j] = source[i][j];

		}

	}
	//使用嵌套循环来循环复制，内层循环用于复制行，外层循环用于多列的数据复制
	//形参n和m既可以表示二维数组的下标，也可以直接表示数组的循环终止条件
	
	
}

void show_array(int n, const double array[][5])
{

	for (int i = 0; i < n; i++)
	{


		for (int j = 0; j < 5; j++)
		{

			printf("%g ", array[i][j]);

		}

		printf("\n");

	}

}

//output:
//0.2 0.4 2.4 3.5 6.6
//8.5 8.2 1.2 1.6 2.4
//9.1 8.5 2.3 6.1 8.4
