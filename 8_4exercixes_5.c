//exercises_5.c---使用二分查找法进行猜数字

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

//int main()
//{
//
//	int head = 1;
//
//	int tail = 100;
//
//	int guess = (head + tail) / 2;//定义3个变量分别标识查找区域的起始位置、终止位置以及中数
//
//	char ch;
//
//	printf("Pick an integer from 1 to 100.I will try to guess ");
//
//	printf("it.\nRespond with a y if my guess is right and with ");
//
//	printf("\n an n if it is wrong.\n");
//
//	do {
//	
//		printf("Un...is your number %d?: ", guess);
//
//		if (getchar() == 'y')
//		{
//
//			break;
//
//		}
//
//		printf("Well, then, %d is larger or smaller than yours? (l or s): ", guess);
//
//		while ((ch = getchar()) == '\n') continue;
//
//		if (ch == 'l' || ch == 'L')
//		{
//
//			tail = guess - 1;
//
//			guess = (head + tail) / 2;
//
//			continue;//如果输入L，则表示目标数在区间的前半区，因此可以舍弃总数到终止位置的后半段数据，
//			//随后切换tail和中数guess
//
//		}
//
//		else if (ch == 's' || ch == 'S')
//		{
//
//			head = guess + 1;
//
//			guess = (head + tail) / 2;
//
//			continue;//如果输入S，则表示目标数在区间的后半区，因此可以舍弃起始数到中数的前半段数据
//			//随后切换变量head和中数guess
//
//		}
//
//		else
//		{
//
//			continue;
//
//		}
//	
//	} while (getchar() != 'y');
//
//	printf("I knew i could do it!\n");
//
//	return 0;
//
//}

//output:
//Pick an integer from 1 to 100.I will try to guess it.
//Respond with a y if my guess is right and with
//an n if it is wrong.
//Un...is your number 50 ? : l
//Well, then, 50 is larger or smaller than yours ? (l or s) : l
//Un...is your number 25 ? : 20
//Well, then, 25 is larger or smaller than yours ? (l or s) : Un...is your number 25 ? : l
//Well, then, 25 is larger or smaller than yours ? (l or s) : l
//Un...is your number 12 ? : 12
//Well, then, 12 is larger or smaller than yours ? (l or s) : Un...is your number 12 ? : y
//I knew i could do it!

int main()
{

	int head = 0;

	int tail = 0;

	int guess = (head + tail) / 2;//定义3个变量分别标识查找区域的起始位置、终止位置以及中数

	char ch;

	printf("Pick an integer from 1 to 100.I will try to guess ");

	printf("it.\nRespond with a y if my guess is right and with ");

	printf("\n an n if it is wrong.\n");

	scanf("%d %d", &head, &tail);

	do {

		printf("Un...is your number %d?: ", guess);

		if (getchar() == 'y')
		{

			break;

		}

		printf("Well, then, %d is larger or smaller than yours? (l or s): ", guess);

		while ((ch = getchar()) == '\n') continue;

		if (ch == 'l' || ch == 'L')
		{

			tail = guess - 1;

			guess = (head + tail) / 2;

			continue;//如果输入L，则表示目标数在区间的前半区，因此可以舍弃总数到终止位置的后半段数据，
			//随后切换tail和中数guess

		}

		else if (ch == 's' || ch == 'S')
		{

			head = guess + 1;

			guess = (head + tail) / 2;

			continue;//如果输入S，则表示目标数在区间的后半区，因此可以舍弃起始数到中数的前半段数据
			//随后切换变量head和中数guess

		}

		else
		{

			continue;

		}

	} while (getchar() != 'y');

	printf("I knew i could do it!\n");

	return 0;

}
//
//output:
//Pick an integer from 1 to 100.I will try to guess it.
//Respond with a y if my guess is right and with
//an n if it is wrong.
//1 100
//Un...is your number 0 ? : Well, then, 0 is larger or smaller than yours ? (l or s) : s
//Un...is your number 50 ? : s
//Well, then, 50 is larger or smaller than yours ? (l or s) : s
//Un...is your number 75 ? : n
//Well, then, 75 is larger or smaller than yours ? (l or s) : s
//Un...is your number 88 ? : n
//Well, then, 88 is larger or smaller than yours ? (l or s) : s
//Un...is your number 94 ? : n
//Well, then, 94 is larger or smaller than yours ? (l or s) : s
//Un...is your number 97 ? : y
//I knew i could do it!