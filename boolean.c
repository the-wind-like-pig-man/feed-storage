//boolean.c---使用BOOL类型的变量variable

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	long num = 0;

	long sum = 0L;

	_Bool input_is_good = 0;

	printf("Please enter an integer to be summed ");

	printf("(q to be quit): ");

	input_is_good = (scanf("%ld", &num) == 1);

	while (input_is_good)
	{

		sum = sum + num;

		printf("Please enter next integer (q to be quit): ");

		input_is_good = (scanf("%ld", &num) == 1);

	}

	printf("Those integers sum to %ld.\n", num);



	return 0;

}

//output:
//Please enter an integer to be summed(q to be quit) : 20
//Please enter next integer(q to be quit) : 5
//Please enter next integer(q to be quit) : 30
//Please enter next integer(q to be quit) : q
//Those integers sum to 30.