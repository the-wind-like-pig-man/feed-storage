//double_array_sort.c---编写一个函数，把double类型数组中的数据倒序排列，并在一个简单的程序中测试该函数

#include <stdio.h>

void r_sort(double number[], int n);
//以传统方式传递数组参数

int main()
{

	double source[12] = { 2.5, 3.2, 1.2, 1.6, 2.4, 0.1, 5.2, 0.9, 0.3, 0.9, 1.4, 7.3 };

	for (int i = 0; i < 12; i++)
	{

		printf("%g ", source[i]);

	}

	printf("\n");

	printf("\n");

	r_sort(source, 12);

	for (int i = 0; i < 12; i++)
	{

		printf("%g ", source[i]);

	}



	return 0;

}

void r_sort(double number[], int n)
{
    //排序算法使用常用的比较排序法，即判断相邻元素的大小关系，
	//并对需要排序的两个元素进行交换位置，内层循环一次能保证将一个元素
	//调整到合适的位置，在n-1次循环中保证所有元素都换到正确的位置

	double temp = 0;

	for (int i = 0; i < n - 1; i++)
	{

		for (int j = 0; j < n - 1 - i; j++)
		{
			//循环判断条件设置为j<n-1也可以，不过会执行一些无效的循环判断，
			//具体原因是每次循环可以保证1个元素到达正确位置，该位置后续排序过程
			//可以忽略，以提高效率

			if (number[j] < number[j + 1])
			{

				//temp = number[j + 1];

				temp = number[j];

				number[j] = number[j + 1];

				//number[j] = temp;

				number[j + 1] = temp;


			}

			//由于这次编程的主要目的并非是排序算法，但在此处打印
			//每次循环的排序结果，有利于理解交换排序的基本思想

		}

	}

}

//output:
//2.5 3.2 1.2 1.6 2.4 0.1 5.2 0.9 0.3 0.9 1.4 7.3
//7.3 7.3 7.3 7.3 7.3 7.3 7.3 7.3 7.3 7.3 7.3 7.3

//output2:
//2.5 3.2 1.2 1.6 2.4 0.1 5.2 0.9 0.3 0.9 1.4 7.3
//7.3 5.2 3.2 2.5 2.4 1.6 1.4 1.2 0.9 0.9 0.3 0.1