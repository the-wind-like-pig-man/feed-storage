//exercises_15.c---读取一行输入，然后倒序打印

//#include  _CRT_SECURE_NO_WARNINGS 0

#define  _CRT_SECURE_NO_WARNINGS 0


#include <stdio.h>

#include <string.h>

int main()
{

	char data[256] = { '0'};

	printf("Enter the char in a line : ");

	int i = 0;

	do
	{

		scanf("%c", &data[i]);

	} while(data[i] != '\n' && ++i && i < 255);

	printf("The reverse char of the data : ");

	for (i--; i >= 0; i--)
	{

		printf("%c", data[i]);

	}

	printf("\nDone!\n");


	return 0;

}

//output:
//Enter the char in a line : abcdefghijkl
//The reverse char of the data : lkjihgfedcba
//Done!

//output:
//Enter the char in a line : abcdefghijklmnopqrstuvwxyz
//The reverse char of the data : zyxwvutsrqponmlkjihgfedcba
//Done!