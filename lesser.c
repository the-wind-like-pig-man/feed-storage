//lesser.c--找出两个整数中较小的一个

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int imin(int, int);

int main()
{

	int evil1 = 0, evil2 = 0;

	printf("Please enter a pair of integers (q to quit): \n");

	while (scanf("%d %d", &evil1, &evil2) == 2)
	{

		printf("The lesser of %d and %d is %d.\n",
			evil1, evil2, imin(evil1, evil2));

		printf("Please enter a pair of integers (q to quit): \n");

	}

	printf("Bye!\n");


	return 0;

}

int imin(int n, int m)
{

	int min = 0;

	if (n < m)
	{

		min = n;

	}

	else
	{

		min = m;

	}

	return min;

}

//output:
//Please enter a pair of integers(q to quit) :
//	509 333
//	The lesser of 509 and 333 is 333.
//	Please enter a pair of integers(q to quit) :
//	09393 3
//	The lesser of 9393 and 3 is 3.
//	Please enter a pair of integers(q to quit) :
//	-9396 1
//	The lesser of - 9396 and 1 is - 9396.
//	Please enter a pair of integers(q to quit) :
//	q
//	Bye!