//exercises_14.c---用循环创建两个8个元素的数组，其中第二个数组的值为第一个数组对应值的和

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	double first[8] = { 0 }, second[8] = { 0 };

	printf("Enter 8 data to the FIRST array: ");

	for (int i = 0; i < 8; i++)
	{

		scanf("%lf", &first[i]);

	}

	for (int i = 0; i < 8; i++)
	{

		double sum = 0;

		for (int j = 0; j <= i; j++)
		{

			sum = sum + first[j];

		}

		second[i] = sum;

	}

	printf("All the data of the two array are: \n");

	printf("The FIRST Array is : \n");

	for (int i = 0; i < 8; i++)
	{

		printf("%12lf. ", first[i]);

	}

	printf("\nThe second Array is: \n");

	for (int i = 0; i < 8; i++)
	{

		printf("%12lf. ", second[i]);

	}

	printf("\nDone!\n");


	return 0;

}

//output:
//Enter 8 data to the FIRST array : 1.0 2.0 3.0 1.5 2.5 3.6 4.1 2.8
//All the data of the two array are :
//	The FIRST Array is :
//1.000000.     2.000000.     3.000000.     1.500000.     2.500000.     3.600000.     4.100000.     2.800000.
//The second Array is :
//1.000000.     3.000000.     6.000000.     7.500000.    10.000000.    13.600000.    17.700000.    20.500000.
//Done!