//3x5_chang_lenth_array.c---以变长数组作为函数形参，完成编程练习10.5.13

#include <stdio.h>

void input_array(int rows, int cols, double arr[rows][cols]);

double col_average(int cols, const double arr[cols]);

double array_average(int rows, int cols, const double arr[rows][cols]);

double array_max_number(int rows, int cols, const double arr[rows][cols]);

void show_result(int rows, int cols, const double arr[rows][cols]);
//函数的参数定义使用变长数组实现。数组的输入函数将会修改数组的元素值，因此不能
//使用const关键词，其他函数应使用const来防止实参被误修改。

int main()
{

	int rows = 3;

	int cols = 5;

	double array[rows][cols];

	input_array(rows, cols, array);

	show_result(rows, cols, array);



	return 0;

}


void input_array(int rows, int cols, double arr[rows][cols])
{

	printf("Please enter the array number.\n");

	for (int i = 0; i < rows; i++)
	{

		printf("Please enter five double number seperate by enter:\n");

		for (int j = 0; j < cols; j++)
		{

			scanf("%lf", &arr[i][j]);

		}

	}

}

double col_average(int rows, int cols, const double arr[cols])
{

	double sum = 0;

	for (int i = 0; i < cols; i++)
	{

		sum += arr[i];

	}

	return sum / cols;

}

double array_average(int rows, int cols, const double arr[rows][cols])
{

	double sum = 0;

	for (int i = 0; i < rows; i++)
	{

		sum += col_average(cols, arr[i]);

	}

	return sum / rows;


}

double array_max_number(int rows, int cols, const double arr[rows][cols])
{

	double max = arr[0][0];

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < cols; j++)
		{

			if (max < arr[i][j])
			{

				max = arr[i][j];

			}

		}

	}

	return max;


}

void show_result(int rows, int cols, const double arr[rows][cols])
{

	printf("Now, Let\'s check the array!\n");

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < cols; j++)
		{

			printf("%5g", arr[i][j]);

		}

		printf("\n");

	}

	printf("The average of every column is :\n");

	for (int i = 0; i < rows; i++)
	{

		printf("The %d column's average is %g.\n", i, col_average(cols, arr[i]));

	}

	printf("The array's data average is %g\n", array_average(rows, cols, arr));

	printf("The max datum in the array is %g.\n", array_max_number(rows, cols, arr));

}