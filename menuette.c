//menuette.c---菜单程序

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

char get_choice(void);

char get_first(void);

int get_int(void);

void count(void);

int main()
{

	int choice = 0;

	void count(void);

	while ((choice = get_choice()) != 'q')
	{

		switch (choice)
		{


		case 'a': printf("Buy low, sell high.\n");

			break;

		case 'b': putchar('\a');//ANSI

			break;

		case 'c': count();

			break;

		default: printf("Program error!\n");


		}

	}

	printf("Bye!\n");


	return 0;

}

void count(void)
{

	int n = 0, i = 0;

	printf("Count how far? Enter an integer:\n");

	n = get_int();

	for (i = 1; i <= n; i++)
	{

		printf("%d\n", i);

		while ((getchar()) != '\n')
		{

			continue;

		}

	}

}

char get_choice(void)
{

	int ch = 0;

	printf("Enter the letter of your choice:\n");

		printf("a. advice       b. bell\n");

		printf("c. count        q. quit\n");

		ch = get_first();

		while ((ch < 'a' || ch > 'c') && ch != 'q')
		{

			printf("Please respond with a, b, c, or q.\n");

			ch = get_first();

		}

		return ch;

}

char get_first()
{

	int ch = 0;

	ch = getchar();

	while (getchar() != '\n')
	{

		continue;

	}

	return ch;

}

int get_int()
{

	int input = 0;

	char ch = 0;

	while (scanf("%d", &input) != 1)
	{

		while ((ch = getchar()) != '\n')
		{

			putchar(ch);//处理错误输出

		}

		printf("is not  an integer.\nPlease enter un ");

		printf("integer value, such as 25, -178, or 3: ");


	}

	return input;


}

//output:
//Enter the letter of your choice :
//a.advice       b.bell
//c.count        q.quit
//a
//Buy low, sell high.
//Enter the letter of your choice :
//a.advice       b.bell
//c.count        q.quit
//count
//Count how far ? Enter an integer :
//two
//twois not an integer.
//Please enter un integer value, such as 25, -178, or 3 : 6
//1
//2
//d
//3
//q
//4
//
//output1:
//Enter the letter of your choice :
//a.advice       b.bell
//c.count        q.quit
//a
//Buy low, sell high.
//Enter the letter of your choice :
//a.advice       b.bell
//c.count        q.quit
//count
//Count how far ? Enter an integer :
//two
//twois not an integer.
//Please enter un integer value, such as 25, -178, or 3 : 5
//1
//2
//d
//3
//q
//4
