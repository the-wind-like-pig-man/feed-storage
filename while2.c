//while2.c---注意分号的位置

#include <stdio.h>

//int main()
//{
//
//	int n = 0;
//
//	while (n++ < 3);//第7行
//
//	printf("n is %d\n", n);//第8行
//
//	printf("That's all this program does.\n");
//
//
//	return 0;
//
//}
//
//output:
//n is 4
//That's all this program does.

int main()
{

	int n = 0;

	while (n++ < 3) //第7行
	{

		printf("n is %d\n", n);//第8行

	}

	//printf("n is %d\n", n);//第8行

	printf("That's all this program does.\n");


	return 0;

}

//output:
//n is 1
//n is 2
//n is 3
//That's all this program does.