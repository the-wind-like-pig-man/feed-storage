//exercises_4.c---用if...else读取输入，读到#字符时停止，用感叹号替换句号，用两个感叹号替换原来的感叹号
//最后报告替换了几次

#include <stdio.h>


int main()
{

	int counter = 0;

	char ch = 0;

	printf("Please input chars (# for exit): ");

	while ((ch = getchar()) != '#')
	{

		if (ch == '!')
		{

			printf("!!");

			counter++;//替换感叹号，并计数

		}

		else if (ch == '.')
		{

			printf("!");

			counter++;//替换句号，并计数

		}

		else
		{

			printf("%c", ch);//对于其余字符，直接输出

		}

		printf("\nTotal replace %d times.\n", counter);

		printf("Done!\n");


	}



	return 0;

}

//output:
//Please input chars(# for exit) : I come from China, I am a Chinese people!I love China!
//I
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//c
//Total replace 0 times.
//Done!
//o
//Total replace 0 times.
//Done!
//m
//Total replace 0 times.
//Done!
//e
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//f
//Total replace 0 times.
//Done!
//r
//Total replace 0 times.
//Done!
//o
//Total replace 0 times.
//Done!
//m
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//C
//Total replace 0 times.
//Done!
//h
//Total replace 0 times.
//Done!
//i
//Total replace 0 times.
//Done!
//n
//Total replace 0 times.
//Done!
//a
//Total replace 0 times.
//Done!
//,
//Total replace 0 times.
//Done!
//I
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//a
//Total replace 0 times.
//Done!
//m
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//a
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//C
//Total replace 0 times.
//Done!
//h
//Total replace 0 times.
//Done!
//i
//Total replace 0 times.
//Done!
//n
//Total replace 0 times.
//Done!
//e
//Total replace 0 times.
//Done!
//s
//Total replace 0 times.
//Done!
//e
//Total replace 0 times.
//Done!
//
//Total replace 0 times.
//Done!
//p
//Total replace 0 times.
//Done!
//e
//Total replace 0 times.
//Done!
//o
//Total replace 0 times.
//Done!
//p
//Total replace 0 times.
//Done!
//l
//Total replace 0 times.
//Done!
//e
//Total replace 0 times.
//Done!
//!!
//Total replace 1 times.
//Done!
//I
//Total replace 1 times.
//Done!
//
//Total replace 1 times.
//Done!
//l
//Total replace 1 times.
//Done!
//o
//Total replace 1 times.
//Done!
//v
//Total replace 1 times.
//Done!
//e
//Total replace 1 times.
//Done!
//
//Total replace 1 times.
//Done!
//C
//Total replace 1 times.
//Done!
//h
//Total replace 1 times.
//Done!
//i
//Total replace 1 times.
//Done!
//n
//Total replace 1 times.
//Done!
//a
//Total replace 1 times.
//Done!
//!!
//Total replace 2 times.
//Done!
//
//
//Total replace 2 times.
//Done!

