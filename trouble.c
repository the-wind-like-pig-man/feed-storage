//trouble.c---误用=号会导致死循环

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main()
{

	long num;

	long sum = 0L;

	int status;

	printf("Please enter an integer to be summed ");

	printf("(q to quit): ");

	status = scanf("%ld", &num);

	while (status = 1)
	{

		sum = sum + num;

		printf("Please enter next integer (q to qiut): ");

		status = scanf("%ld", &num);

	}

	printf("THose integers sum to be %ld.\n", sum);


	return 0;

}

////output:
//Please enter an integer to be summed(q to quit) : 20
//Please enter next integer(q to qiut) : 5
//Please enter next integer(q to qiut) : 30
//Please enter next integer(q to qiut) : 30
//Please enter next integer(q to qiut) : 20
//Please enter next integer(q to qiut) :q---无限循环
