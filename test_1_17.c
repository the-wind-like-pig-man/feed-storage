﻿//#include <math.h>//不包含头文件，求出来的是奇数，不是素数

#include <math.h>//包含头文件，求出来的是素数，不是奇数


#include <stdio.h>

//写一个函数判断一个数是不是素数

int is_prime(int n)
{
	int j = 0;

	for (j = 2; j <= sqrt(n); j++)
	{
		if (n % j == 0)
		{
			return 0;//return的功能远远强大于break.
		}
	}

	return 1;

}

int main()
{
	int i = 0;

	int count = 0;

	for (i = 101; i <= 200; i += 2)
	{
		if (is_prime(i))
		{
			printf("%d ", i);

			count++;
		}
	}

	printf("\ncount = %d\n", count);

	return 0;
}