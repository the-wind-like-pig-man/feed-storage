//showchar1.c---有较大I/O问题的程序

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void display(char cr, int lines, int width);

int main()
{

	int ch = 0;//待打印字符

	int rows = 0, cols = 0;//行数和列数

	printf("Please enter a character and two integers : \n");

	while ((ch = getchar()) != '\n')
	{

		//scanf("%d %d", &rows, &cols);

		if (scanf("%d %d", &rows, &cols) != 2)

			break;

		display(ch, rows, cols);

		while (getchar() != '\n')
		{

			continue;

		}

		printf("Please enter another character and two integers;\n");

		printf("Please enter a newline to quit.\n");

	}

	printf("Bye!\n");




	return 0;

}

void display(char cr, int lines, int width)
{

	int row, col;


	for (row = 1; row <= lines; row++)
	{

		for (col = 1; col <= width; col++)
		{

			putchar(cr);

		}

		putchar('\n');//结束一行并开始新的一行

	}

}


//output:
//Please enter a character and two integers :
//a 3 3
//aaa
//aaa
//aaa
//Please enter another character and two integers;
//Please enter a newline to quit.
//Bye!


//output1:
//Please enter a character and two integers :
//c 3 3
//ccc
//ccc
//ccc
//Please enter another character and two integers;
//Please enter a newline to quit.
//a 3 3
//aaa
//aaa
//aaa
//Please enter another character and two integers;
//Please enter a newline to quit.
//l 3 3
//lll
//lll
//lll
//Please enter another character and two integers;
//Please enter a newline to quit.
//'\n'
//Bye!
