//file_eof.c---打开一个文件，并显示该文件

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <stdlib.h>//为了使用exit()函数

int main()
{

	int ch = 0;

	FILE * fp;

	char fname[50] = { 0 };//储存文件名

	printf("Please enter the name of the file: ");

	scanf("%s", fname);

	fp = fopen(fname, "r");//打开待读取文件

	if (fp == NULL)//如果失败
	{

		printf("Failed to open file. Bye!\n");

		exit(1);//退出程序

	}

	//get(fp)从打开的文件中获取一个字符

	while ((ch = getc(fp)) != EOF)
	{

		putchar(ch);

		fclose(fp);//关闭文件

	}

	//fclose(fp);//关闭文件


	return 0;

}

//output:
//Please enter the name of the file : 程序清单8_2_echo_eof.c
//Failed to open file.Bye!

//output:
//Please enter the name of the file : D:\C_program\C_program\c_lesson_write.txt
//绗 ? 30鑺 ? --exercise
//
//free锛氫粎閲婃斁绌洪棿锛屼絾涓嶄細瀵归噴鏀剧殑绌洪棿缃┖銆 ?

//output:
//Please enter the name of the file : D:\C_program\C_program\c_lesson_write.txt
//?