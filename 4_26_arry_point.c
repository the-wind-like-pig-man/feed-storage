﻿

#include <stdio.h>

//int Add(int x, int y)
//{
//
//
//	return x + y;
//
//
//}
//
//int main()
//{
//
//	int arr[5] = { 0 };
//
//	//&数组名---取出的是数组的地址
//
//	int(*p)[5] = &arr;//数组指针
//
//	//&函数名---取出的是函数的地址
//
//	//printf("%p\n", &Add);//00007FF6980F13D9
//
//	printf("%p\n", Add);//00007FF6980F13D9
//
//	//对于函数来说，&函数名和函数名都是函数的地址
//
//	int (*pf)(int, int) = &Add;
//
//	//int (*pf)(int, int) = Add;// --- ok
//
//		//int ret = (*pf)(2, 3);//---ok
//
//	int ret = Add(2, 3);//---ok
//
//	//int ret = pf(2, 3);//---ok
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}

//
//int Add(int x, int y)
//{
//
//
//	return x + y;
//
//
//}
//
//void calc(int(*pf)(int, int))//函数传参，指针接收
//{
//
//	int a = 3;
//
//	int b = 5;
//
//	int ret = pf(a, b);
//
//	printf("%d\n", ret);
//
//
//}
//
//int main()
//{
//
//	calc(Add);
//
//	return 0;
//
//}

int main()
{

	//指针数组

	int* arr[4];

	char* ch[5];

	int** p = arr;

	//数组指针

	int arr2[5];

	int(*pa)[5] = &arr2;

	char* arr3[6];

	char(*p3)[6] = &arr3;


	return 0;

}