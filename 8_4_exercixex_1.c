//exercises_1.c---一份文件的字符数

#include <stdio.h>

int main()
{

	int counter = 0;

	//char ch = '0';

	char ch ;


	//while ((ch = getchar()) != EOF)
		while ((ch = getchar()) != '\n')

	{

		counter++;

		//printf("The File has %d characters.\n", counter);

	}
	//如果程序需要分别统计不同类型的字符，可以在本处使用分支判断语句分类别统计
	//此外，本处counter++的计数语句也可以使用如下语句：
	//if(ch > '\040')counter++;
	//其含义是ASCII码表内大于空格符（'\040'）的字符均是文本中使用的字符，  
	//由于本题并未对待统计的具体字符类型做出规定，因此可以直接使用counter++
	//

	printf("The File has %d characters.\n", counter);

	return 0;

}
//
//OUTPUT:
//I
//The File has 1 characters.
//The File has 2 characters.
//R
//The File has 3 characters.
//The File has 4 characters.
//F
//The File has 5 characters.
//The File has 6 characters.
//EOF
//The File has 7 characters.
//The File has 8 characters.
//The File has 9 characters.
//The File has 10 characters.

//output:
//hello world!hello China!
//The File has 25 characters.