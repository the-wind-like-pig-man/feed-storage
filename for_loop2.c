#define _CRT_SECURE_NO_WARNINGS 0

//for_none.c---循环结束控制

//#include <stdio.h>
//
//int main()
//{
//
//	int ans = 0, n = 0;
//
//	ans = 2;
//
//	for (n = 3; ans <= 60;)
//	{
//
//		ans = ans * n;
//
//		printf("n = %d; ans = %d.\n", n, ans);
//
//	}
//
//
//
//	return 0;
//
//}

//output:
//n = 3; ans = 6.
//n = 3; ans = 18.
//n = 3; ans = 54.


//for_show.c---循环体表达式的执行

#include <stdio.h>

int main()
{

	int num = 0;


	for (printf("Keep entering numbers!\n"); num != 6;)
	{

		scanf("%d", &num);

		printf("That's the one i want!\n");

	}


	return 0;

}

//output:
//Keep entering numbers!
//5
//That's the one i want!
//2
//That's the one i want!
//3
//That's the one i want!
//8
//That's the one i want!
//7
//That's the one i want!
//1
//That's the one i want!
//6
//That's the one i want!