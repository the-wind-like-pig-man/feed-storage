//3_7_exercises_7.c---输入英寸身高，输出厘米身高

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define INCH_TO_CM 2.54//使用预编译命令定义英寸和厘米的转换系数

int main()
{

	float inch = 0, cm = 0;

	printf("Please enter the inch of your heigh: ");

	scanf("%f", &inch);

	cm = inch * INCH_TO_CM;

	printf("Hi, you are %0.2f inch, or %.2f cm heigh.\n", inch, cm);




	return 0;

}

//output:
//Please enter the inch of your heigh : 80.5
//Hi, you are 80.50 inch, or 204.47 cm heigh.