//power.c---power()函数返回一个double类型的整数次幂
//0的任何次幂都是0，任何数的0次幂都是1

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

double power(double n, int p);//计算double类型数据n的p次幂，返回类型为double

int main()
{

	double x = 0, xpow = 0;

	int exp = 0;

	printf("Please enter a number and the integer power ");

	printf(" to which \n the number will be raised.Enter q");

	printf(" to  quit.\n");

	while (scanf("%lf %d", &x, &exp) == 2)
	{

		xpow = power(x, exp);

		printf("%.3g to the power %d is %.5g.\n", x, exp, xpow);

		printf("Please enter the next numbers or q to quit.\n");

	}

	printf("Hope you nejoy this power trip --- bye!\b");



	return 0;

}

double power(double n, int p)
{

	double pow = 1;

	int i = 0;

	if (n == 0 && p == 0)
	{

		printf("The %g to the power %d is not define, return 1!\n", n, p);

		return 1;

	}//0的0次幂单独标注

	if (n == 0)
	{

		return 0;//0的任何次幂等于0

	}

	if (p == 0)
	{

		return 1;//除0之外，任何数的0次幂等于1

	}

	if (p > 0)//正整数次幂
	{

		for (i = 1; i <= p; i++)
		{

			pow *= n;

			//return pow;

		}

		return pow;

	}

	else//负整数次幂
	{

		for (i = 1; i <= -p; i++)
		{

			pow *= n;

			//return 1 / pow;

		}

		return 1 / pow;


	}
}

//output:
//Please enter a number and the integer power : to which
//the number will be raised.Enter q to  quit.
//3.3 - 2
//3.3 to the power - 2 is 0.30303.
//Please enter the next numbers or q to quit.

//output:
//Please enter a number and the integer power  to which
//the number will be raised.Enter q to  quit.
//0.5 2
//0.5 to the power 2 is 0.25.
//Please enter the next numbers or q to quit.
//0.5 3
//0.5 to the power 3 is 0.125.
//Please enter the next numbers or q to quit.
//0.5 - 2
//0.5 to the power - 2 is 4.
//Please enter the next numbers or q to quit.
//0.5 - 3
//0.5 to the power - 3 is 8.
//Please enter the next numbers or q to quit.
//q
//Hope you nejoy this power trip-- - bye