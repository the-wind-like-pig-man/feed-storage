//somedata.c---部分初始化数组

#include <stdio.h>

#define SIZE 4

int main()
{

	int some_data[SIZE] = { 1492, 1066 };

	int i = 0;

	//printf("%2s%14s\n", "i", "some_data");

	printf("%1s%19s\n", "i", "some_data");


	for (i = 0; i < SIZE; i++)
	{

		printf("%d%14d\n", i, some_data[i]);

	}


	return 0;

}

//output:
//i     some_data
//0          1492
//1          1066
//2             0
//3             0

//output:
//i          some_data
//0          1492
//1          1066
//2             0
//3             0