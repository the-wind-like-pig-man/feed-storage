//lethead1.c---创建在一行打印四十个*号的函数

#include <stdio.h>

#define NAME "GIGATHINK, INC"

#define ADDRESS "101 Megabuck Plaza"

#define PLACE "Megapolis, CA 94904"

#define WIDTH 40

void starbar(void);//函数原型

int main()
{

	starbar();

	printf("%s\n", NAME);

	printf("%s\n", ADDRESS);

	printf("%s\n", PLACE);

	starbar();//使用函数



	return 0;

}

void starbar()//定义函数
{

	int count = 0;

	for (count = 1; count <= WIDTH; count++)
	{

		putchar('*');

		//putchar('\n');

	}

	putchar('\n');

}

//output1:
//****************************************
//GIGATHINK, INC
//101 Megabuck Plaza
//Megapolis, CA 94904
//* ***************************************
//
// output2:
// 
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//GIGATHINK, INC
//101 Megabuck Plaza
//Megapolis, CA 94904
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*
//*