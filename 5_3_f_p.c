﻿
#include <stdio.h>

#include <string.h>

#include <stdlib.h>

//void bubble_sort(int arr[], int sz)
//{
//
//	int i = 0;
//
//	//冒泡的趟数
//
//	for (i = 0; i < sz - 1; i++)
//	{
//
//		int flag = 1;//假设数组是排序好的
//		//一趟冒泡排序的过程
//
//		int j = 0;
//
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//
//			if (arr[j] > arr[j + 1])
//			{
//
//				int tmp = arr[j];
//
//				arr[j] = arr[j + 1];
//
//				arr[j + 1] = tmp;
//
//				flag = 0;
//
//			}
//
//		}
//
//		if (flag == 1)//若是排序好的，不再进行冒泡排序
//		{
//
//			break;
//
//		}
//
//	}
//
//}
//
//int main()
//{
//
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//
//	//0,1,2,3,4,5,6,7,8,9---升序排列
//	//把数组按升序排列
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz);
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//
//	}
//
//
//
//	return 0;
//
//}

//qsort---库函数，使用快速排序的思想实现排序的一个函数，这个函数可以排序任意类型的数据

//不同数据的比较排序有差异，方法不同
//
//void qsort(void* base,//待排序的数据的起始位置
//	       
//	       size_t num,//待排序的数据元素的个数
//
//	       size_t width,//待排序的数据元素的大小（单位是字节）
//	      
//	       int(* cmp)(const void* e1, const void* e2)//函数指针---比较函数
//
//
//);

//_cadec1---函数调用约定


//比较两个整形元素
//e1指向一个整型
//e2指向另外一个整型

//int cmp_int(const void* e1, const void* e2)
//{
//
//	//if (*e1 > *e2)//void型指针不能直接解引用
//	if (*（int*）e1 > *(int*)e2)//void型指针不能直接解引用
// 
//
//		return 1;
//
//	//else if (*e1 == *e2)
//	else if (*(int*)e1 == *(int*)e2)
// 
//
//		return 0;
//
//	else
//
//		return -1;
//
//}

 
// int cmp_int(const void* e1, const void* e2)
//{
//
//	 //return (*(int*)e1 - *(int*)e2);//升序---两个return语句，谁在前面谁有效
//
//	 return (*(int*)e2 - *(int*)e1);//降序
//
//
//}
//
////用qsort函数实现排序
//int main()
//{
//
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//
//	//0,1,2,3,4,5,6,7,8,9---升序排列
//	//把数组按升序排列
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	//bubble_sort(arr, sz);
//
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);//qsort函数默认进行升序排列
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//
//	}
//
//
//
//	return 0;
//
//}
//
//int main()
//{
//
//	int a = 10;
//
//	//char* pv = &a;//a的地址是int*类型，不匹配
//
//	void* pv = &a;//void*是无具体类型的指针，可以接收任意类型的地址
//	//void*是无具体类型的指针，所以不能解引用操作，也不能加、减整数
//
//
//	return 0;
//
//}

//struct Stu//定义结构体数据
//{
//
//	char name[20];
//
//	int age;
//
//};
//
//int cmp_stu_by_name(const void* e1, const void* e2)
//{
//
////strcmp ---> >0  ==0  <0
//
//	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
//
//}
//
//int cmp_stu_by_age(const void* e1, const void* e2)
//{
//
//	return (((struct Stu*)e1)->age - ((struct Stu*)e2)->age);
//
//}
//
//void test2()
//{
//
////测试使用qsort来排序结构体数据
//
//	struct Stu s[] = {{"zhangsan", 20}, {"lisi", 30}, {"wangeu",25}};
//
//	int sz = sizeof(s) / sizeof(s[0]);
//
//	qsort(s, sz, sizeof(s[0]), cmp_stu_by_name);
//
//
//	//qsort(s, sz, sizeof(s[0]), cmp_stu_by_age);
//
//}

//int main()
//{
//
//	//test1();
//
//	test2();
//
//
//	return 0;
//
//}

void Swap(char* buf1, char* buf2, int width)
{

	int i = 0;

	for (i = 0; i < width; i++)
	{

		char tmp = *buf1;

		*buf1 = *buf2;

		*buf2 = tmp;

		buf1++;

		buf2++;

	}

}

//将冒泡排序改造成可以排序任意类型的排序函数
void bubble_sort(void* base, int sz, int width, int(*cmp)(const* e1, const* e2))
{

	int i = 0;

	//冒泡的趟数

	for (i = 0; i < sz - 1; i++)
	{

		int flag = 1;//假设数组是排序好的
		//一趟冒泡排序的过程

		int j = 0;

		for (j = 0; j < sz - 1 - i; j++)
		{

			if (cmp((char*)base + j*width, (char*)base + (j + 1)*width) > 0)
			{
				//进行交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);

				flag = 0;

			}

		}

		if (flag == 1)//若是排序好的，不再进行冒泡排序
		{

			break;

		}

	}

}

int main()
{

	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };

	//0,1,2,3,4,5,6,7,8,9---升序排列
	//把数组按升序排列

	int sz = sizeof(arr) / sizeof(arr[0]);

	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);

	int i = 0;

	for (i = 0; i < sz; i++)
	{

		printf("%d ", arr[i]);

	}



	return 0;

}
