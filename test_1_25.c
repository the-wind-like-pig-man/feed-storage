﻿#define _CRT_SECURE_NO_WARNINGS 0


#include <stdio.h>

//函数实现屏幕打印九九乘法口诀表

//void print_table(int n)
//{
//
//	int i = 0;
//
//	for (i = 1; i <= n; i++)//打印一行
//	{
//		int j = 0;
//
//		for (j = 1; j <= i; j++)//打印一列
//
//			printf("%d*%d=%-2d ", i, j, i * j);//%-2d后面应有一个空格
//
//		printf("\n");
//
//	}
//
//}
//
//int main()
//{
//
//	int n = 0;
//
//	scanf("%d", &n);//输入数字
//
//	print_table(n);
//
//	return 0;
//}

//int test()
//{
//	
//	//return 3, 4;//只能返回4
//
//	return (3, 4);//只能返回4
//
//
//}
//
//int main()
//{
//
//	int ret = test();
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}



//int test(int arr[])//可以返回3和4
//{
//	
//	arr[0] = 3;
//
//	arr[1] = 4;
//	//return 3, 4;//只能返回4
//
//	//return (3, 4);//只能返回4
//
//
//}
//
//int main()
//{
//
//	int arr[2] = { 0 };
//
//	int ret = test(arr);
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}

//int test(int* px,int* py)//可以返回3和4
//{
//	
//	*px = 3;
//
//	*py = 4;
//
//	//return 3, 4;//只能返回4
//
//	//return (3, 4);//只能返回4
//
//
//}
//
//int main()//定义两个指针，返回两个参数
//{
//	int a = 0;
//
//	int b = 0;
//
//
//    test(&a,&b);
//
//	printf("%d,%d\n", a,b);
//
//	return 0;
//
//}

//定义两个全局变量，返回两个参数
//int a = 0;
//
//int b = 0;
//
//int test(int* px,int* py)//可以返回3和4
//{
//	
//	a = 3;
//
//	b = 4;
//
//	//return 3, 4;//只能返回4
//
//	//return (3, 4);//只能返回4
//
//
//}
//
//int main()//定义两个全局变量，返回两个参数
//{
//	//int a = 0;
//
//	//int b = 0;
//
//
//    test(&a,&b);
//
//	printf("a=%d,b=%d\n", a,b);
//
//	return 0;
//
//}
//


//int main()
//{
//
//	/*int a = 1;
//
//	int b = 2;
//
//	int c = 3;*/
//
//	//若存放1到10个数，1到100个数？？
//
//	//数组---一组相同类型元素的集合。
//
//	int arr[10];
//
//	char ch[5];
//
//	double data1[20];
//
//	double data2[15 + 5];
//	
//	
//	//下面的代码只能在支持C99标准的编译器上编译---gcc的编译器支持C99
//	int n = 10;
//
//	int arr2[n];//这种数组不能初始化
//
//	//在C99标准之前，数组的大小必须是常量或者常量表达式
//	//在C99之后，数组的大小可以是变量，为了支持变长数组
//
//	return 0;
//}

//
//int main()
//{
//	//不完全初始化，剩余的元素默认初始化为0
//
//	int arr[10] = { 1,2,3 };
//
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,0 };
//
//	int arr2[] = { 1,2,3 };
//
//	char ch1[10] = { 'a','b','c' };//a b c 0 0 0 0 0 0 0
//
//	char ch2[10] = "abc";//a b c \0 0 0 0 0 0 0 0---与上面性质不同，结果相同。
//
//	char ch3[] = { 'a','b','c' };//[]---下标引用操作符
//
//	char ch4[] = "abc";
//
//
//	return 0;
//}
//
//
//int main()
//{
//
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//            0    ...           9
//	//[]下标引用操作符
//
//	//printf("%d\n", arr[4]);
//
//	int i = 0;
//
//	int sz = sizeof(arr) / sizeof(arr[0]);//10个元素
//
//	//打印数组每个元素的地址
//
//	for (i = 0; i <= sz; i++)
//	{
//
//		printf("&arr[%d] = %p\n", i, &arr[i]);
//
//	}
//
//	//for (i = 0; i <= sz; i++)//含等号，打印出未知数
//
//	//for (i = 0; i < sz; i++)//不含等号，不会打印出未知数
//
//	//{
//
//	//	printf("%d ", arr[i]);
//
//	//}
//
//	//for (i = sz-1; i >= 0; i--)//倒序打印
//
//	//	{
//
//	//		printf("%d ", arr[i]);
//
//	//	}
//
//	return 0;
//
//}