//pnt_add.c---指针地址

#include <stdio.h>

#define SIZE 4

int main()
{

	short dates[SIZE] = { 0 };

	short* pti;

	short index = 0;

	double bills[SIZE] = { 0 };

	double* ptf;

	pti = dates;

	ptf = bills;//把数组地址赋给指针

	printf("%23s %15s\n", "short", "double");

	for (index = 0; index < SIZE; index++)
	{

		printf("pointers + %d: %10p %10p\n", index, pti + index, ptf + index);

	}




	return 0;

}


//output:
//                   short          double
//pointers + 0: 000000430397F5F8 000000430397F658
//pointers + 1: 000000430397F5FA 000000430397F660
//pointers + 2: 000000430397F5FC 000000430397F668
//pointers + 3: 000000430397F5FE 000000430397F670