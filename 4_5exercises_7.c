//4_5exercises_7.c---分别以不同精度double和float类型计算1.0/3.0，并打印

#include <stdio.h>

#include <float.h>

int main()
{

	double d_third = 1.0 / 3.0;

	float f_third = 1.0 / 3.0;

	printf("float of one third(6) = %.6f\n", f_third);

	printf("float of one third(12) = %.12f\n", f_third);

	printf("float of one third(16) = %.16f\n", f_third);

	printf("double of one third(6) = %.6lf\n", d_third);

	printf("double of one third(12) = %.12lf\n", d_third);

	printf("double of one third(16) = %.16lf\n", d_third);

	printf("FLT_DIG in float.h is %d\n", FLT_DIG);//float类型的精度

	printf("DBL_DIG in flost.h is %d\n", DBL_DIG);//double类型的精度


	return 0;

}

//output:
//float of one third(6) = 0.333333
//float of one third(12) = 0.333333343267
//float of one third(16) = 0.3333333432674408
//double of one third(6) = 0.333333
//double of one third(12) = 0.333333333333
//double of one third(16) = 0.3333333333333333
//FLT_DIG in float.h is 6
//DBL_DIG in flost.h is 15
