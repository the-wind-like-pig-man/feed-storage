#define _CRT_SECURE_NO_WARNINGS 0

//2_5_exercises_2.c---打印姓名和地址

#include <stdio.h>
//
//#define NAME "Stephen Prata"
//
//#define ADDRESS "NO.11 Chengshou Street, Fengtai District, Beijing"//姓名、地址分别用预编译指令定义
//
//int main()
//{
//
//	printf("%s\n", NAME);
//
//	printf("%s\n", ADDRESS);
//
//
//	return 0;
//
//}

//output:
//Stephen Prata
//NO.11 Chengshou Street, Fengtai District, Beijing

int main()
{

	char NAME = { 0 };

	char ADDRESS = { 0 };

	//scanf("%s", NAME);

	printf("Please enter your name: ");

	while ((NAME = getchar()) != '\n')
	{


		 putchar(NAME);

		//printf("%c\n", NAME);

	}

	printf("\n");
	
	printf("Please input your ADDRESS: ");

	while ((ADDRESS = getchar()) != '\n')
	{

		putchar(ADDRESS);

	}

	/*ADDRESS = getchar();

	printf("%c\n", ADDRESS);*/

	return 0;

}

//ourput:
//Please enter your name : David Lei
//David LeiPlease input your ADDRESS : Lanxue Road, New Chuansha Town, Pudong New Area
//Lanxue Road, New Chuansha Town, Pudong New Area

////output:
//Please enter your name : David Lei
//David Lei
//Please input your ADDRESS : Lanxue Road, Chuansha New Town, Pudong New Area
//Lanxue Road, Chuansha New Town, Pudong New Area
