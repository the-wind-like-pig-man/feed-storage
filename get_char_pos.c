//get_char_pos.c---编写一个函数，从标准输入读取字符，直到文件结束
//判断每个字符的位置，并给出每个字符的位置。另编写一个函数，以字符为参数，若该字符时字母，则返回一个数值位置，否则返回-1

#include <stdio.h>

void get_char_pos(void);//函数读取标准输入，对于字符输入，打印其在字母表中的位置

//int position(char ch);//函数计算并返回字符在字母表中的位置

int position(char ch);//函数计算并返回字符在字母表中的位置


int main()
{

	get_char_pos();


	return 0;

}

void get_char_pos(void)
{

	//char ch = 0;

	char ch;


	printf("Please enter the chars(ended by EOF,not enter): ");

	while ((ch = getchar()) != EOF)//以文件结束尾为标志
	{
	
		if ((ch = getchar()) == '\n')
		{
		
			continue;//简单清除换行符的输入，对于其他特殊符号未处理
		
		}

		if (position(ch) != -1)
		{

			printf("The char %c's position in alphabet is %d.\n", ch, position(ch));

			putchar(ch);

			printf("\n");


		}

		else
		{

			printf("%c is not a alphabet.\n", ch);

		}
	
	}

}

int position(char ch)//一次仅能判断一个字符的位置
{

	if (ch >= 'A' && ch <= 'Z')
	{

		//return ch - 'A' + 1;

		return (ch - 'A' + 1);

		//putchar(ch);


	}

	if (ch >= 'a' && ch <= 'z')
	{

		//return ch - 'a' + 1;

		return (ch - 'a' + 1);
		
		//putchar(ch);



	}

	return -1;


}

//output:
//Please enter the chars(ended by EOF, not enter) : a bce 1 23
//is not a alphabet.
//The char c's position in alphabet is 3.
//is not a alphabet.
//is not a alphabet.
//3 is not a alphabet.
//
//EOF
//The char E's position in alphabet is 5.
//The char F's position in alphabet is 6.
//
//Q
//
//QUIT
//The char Q's position in alphabet is 17.
//The char I's position in alphabet is 9.
//
//\n
//\ is not a alphabet.
//'\n'
//\ is not a alphabet.
//' is not a alphabet.

//output2:
//Please enter the chars(ended by EOF, not enter) : abcADCB
//The char b's position in alphabet is 2.
//The char A's position in alphabet is 1.
//The char C's position in alphabet is 3.
//abcdefg
//The char b's position in alphabet is 2.
//The char d's position in alphabet is 4.
//The char f's position in alphabet is 6.

//output:
//Please enter the chars(ended by EOF, not enter) : ab
//The char b's position in alphabet is 2.
//b
//cb
//The char c's position in alphabet is 3.
//c
//cb
//The char b's position in alphabet is 2.
//b
//cb
//The char c's position in alphabet is 3.
//c
//cb
//The char b's position in alphabet is 2.
//b