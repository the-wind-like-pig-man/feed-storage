//double_two_array_copy.c---编写一个程序，初始化一个double类型的二位数组，使用编程练习2中的一个复制函数把该数组中的数据复制到另一个二位数组中（因为二位数组是数组的素组
//所以可以使用处理一维数组的复制函数来处理数组中的每个子函数）

#include <stdio.h>

#define ROWS 12

#define COLS 5

void copy_arr(double t[], double s[], int n);

void copy_ptr(double* t, double* s, int n);

void copy_ptrs(double* t, double* s_first, double* s_last);
//编程练习2的一维数组赋值函数的声明

void copy_2d_array(double t[][ROWS], double s[][ROWS], int n);

void copy_2d_ptr(double(*t)[ROWS], double(*s)[ROWS], int n);
//二位数组的复制函数的声明，只使用了指针和数组作为参数，首位指针作为参数的形式类似


void main()
{

	double target[COLS][ROWS], source[COLS][ROWS] = {

		{4.3, 4.3, 4.3, 3.0, 2.0, 1.2, 0.2, 0.2, 0.4, 2.4, 3.5, 6.6},

		{8.5, 8.2, 1.2, 1.6, 2.4, 0.0, 5.2, 0.9, 0.3, 0.9, 1.4, 1.3},

		{9.1, 8.5, 6.7, 4.3, 2.1, 0.8, 0.2, 0.2, 1.1, 2.3, 6.1, 8.4},

	    {7.2, 9.9, 8.4, 3.3, 1.2, 0.8, 0.4, 0.0, 0.6, 1.7, 4.3, 6.2},

		{7.6, 5.6, 3.8, 2.8, 3.8, 0.2, 0.0, 0.0, 0.0, 1.3, 2.6, 5.2},
	
	};

	copy_2d_ptr(target, source, COLS);

	for (int i = 0; i < COLS; i++)
	{

		for (int j = 0; j < ROWS; j++)
		{

			printf("%5.2f", target[i][j]);

		}

		printf("\n");

	}



	return 0;

}

void copy_arr(double t[], double s[], int n)
{

	for (int i = 0; i < n; i++)
	{

		t[i] = s[i];

	}

}

void copy_ptr(double* t, double* s, int n)
{

	for (int i = 0; i < n; i++)
	{

		*(t + i) = *(s + i);

	}

}

void copy_ptrs(double* t, double* s_first, double* s_last)
{

	for (int i = 0; (s_last - s_first) > i; i++)
	{
		//for(int i = 0; (s_last - s_first) > 0; i++, s_irst++)

		*(t + i) = *(s_first + i);

	}

}
//编程练习2的3个一维数组复试函数的实现

void copy_2d_array(double t[][ROWS], double s[][ROWS], int n)
{
//参数n表示列数

	for (int i = 0; i < n; i++)
	{

		copy_arr(t[i], s[i], ROWS);

	}


}
//以二维数组作为参数，参数列表内可以省略主数组的元素数，但其他子数组的元素数不能省略
//原一维数组的复制函数能够复制二维数组的一行，所以通过循环，逐行复制，在参数的调用中需要注意
//行数、列数在函数内使用的变量名，n表示列数

void copy_2d_ptr(double(*t)[ROWS], double(*s)[ROWS], int n)
{

	for (int i = 0; i < n; i++)
	{

		copy_ptr(*(t + i), *(s + i), ROWS);

	}

}
//以指向二维数组的指针作为参数，参数列表内要标识指针指向数组的元素数。
//原一维数组的复制函数能够复制二维数组的一行，所以通过循环逐行复制。
//在参数的调用中需要注意行数、列数在函数内使用的变量名，n表示列数

//output:
//4.30 4.30 4.30 3.00 2.00 1.20 0.20 0.20 0.40 2.40 3.50 6.60
//8.50 8.20 1.20 1.60 2.40 0.00 5.20 0.90 0.30 0.90 1.40 1.30
//9.10 8.50 6.70 4.30 2.10 0.80 0.20 0.20 1.10 2.30 6.10 8.40
//7.20 9.90 8.40 3.30 1.20 0.80 0.40 0.00 0.60 1.70 4.30 6.20
//7.60 5.60 3.80 2.80 3.80 0.20 0.00 0.00 0.00 1.30 2.60 5.20
