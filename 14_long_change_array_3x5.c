//13_3fuctions_3x5_array.c---编写一个程序，提示用户输入3组数据，每组数据包含5个
//double类型的数（假设用户正确地响应，不会输入非数值数据）。该程序完成下列任务：
//a.把用户输入的数据存储在3x5的数组中；
//b.计算每组数据的平均值；
//c.计算所有数据的平均值；
//d.找出这15个数据中的最大值；
//e.打印结果。
//每个任务都要用单独的函数来完成（使用传统C处理数组的方式）。为了完成任务b,要编写一个计算并返回一维数组
//平均值的函数，利用循环调用该函数3次。对于处理其他任务的函数，应该以整个数组作为参数，完成任务c和d的函数
//把结果返回主调函数。

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#define ROWS 3

#define COLS 5

void input_array(int rows, double arr[][COLS]);

double col_average(int cols, const double arr[]);

double array_average(int rows, const double arr[][COLS]);

double array_max_number(int rows, const double arr[][COLS]);

void show_result(int rows, const double arr[][COLS]);
//函数的定义使用传统数组形式来实现。数组的数组函数将会修改数组的元素值，因此不能
//使用const关键词，其他函数应当使用const来防止实参被误修改

int main()
{

	double array[ROWS][COLS];

	input_array(ROWS, array);

	show_result(ROWS, array);

	printf("\n");

	return 0;

}

void input_array(int rows, double arr[][COLS])
{

	printf("Please enter the array number: \n");

	for (int i = 0; i < rows; i++)
	{

		printf("Please enter five double number seprate by enter: \n");

		for (int j = 0; j < COLS; j++)
		{

			//scanf("%1f ", &arr[i][j]);//不是1f是lf,且后面没有空格

			scanf("%lf", &arr[i][j]);

		}

	}

}

double col_average(int cols, const double arr[])
{

	double sum = 0;

	for (int i = 0; i < cols; i++)
	{

		sum += arr[i];

	}

	return sum / cols;

}

double array_average(int rows, const double arr[][COLS])
{


	double sum = 0;

	for (int i = 0; i < rows; i++)
	{

		sum += col_average(COLS, arr[i]);

	}

	return sum / rows;

}

double array_max_number(int rows, const double arr[][COLS])
{

	double max = arr[0][0];

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < COLS; j++)
		{

			if (max < arr[i][j])
			{

				max = arr[i][j];

			}

		}

	}

	return max;


}


void show_result(int rows, const double arr[][COLS])
{

	printf("Now, let\'s check the array: \n");

	printf("The array you input is: \n");

	for (int i = 0; i < rows; i++)
	{

		for (int j = 0; j < COLS; j++)
		{

			printf("%5g ", arr[i][j]);

		}

		printf("\n");

	}

	printf("The average of every column is： \n");

	for (int i = 0; i < rows; i++)
	{

		printf("The %d column's average is %g.\n", i, col_average(COLS, arr[i]));

		/*printf("The array's data average is %g \n", array_average(ROWS, arr));

		printf("The max datum in the array is %g", array_max_number(ROWS, arr));*/

	}

	printf("The array's data average is %g \n", array_average(ROWS, arr));

	printf("The max datum in the array is %g", array_max_number(ROWS, arr));


}

//output:
//Please enter the array number :
//	Please enter five double number seprate by enter :
//1.0 2.0 30.0 4.2 5.6
//Please enter five double number seprate by enter :
//Please enter five double number seprate by enter :
//Now, let's check the array:
//The array you input is :
//-9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61
//- 9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61
//- 9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61 - 9.25596e+61
//The average of every column is：
//The 0 column's average is -9.25596e+61.
//The array's data average is -9.25596e+61
//The max datam in the array is - 9.25596e+61The 1 column's average is -9.25596e+61.
//The array's data average is -9.25596e+61
//The max datam in the array is - 9.25596e+61The 2 column's average is -9.25596e+61.
//The array's data average is -9.25596e+61
//The max datam in the array is - 9.25596e+61

//output2:
//Please enter the array number :
//	Please enter five double number seprate by enter :
//1.0 2.0 3.0 4.0 5.0
//3.0 2.0 1.0 8.0 7.0
//Please enter five double number seprate by enter :
//2.3 1.0 5.6 4.8 3.6
//Please enter five double number seprate by enter :
//2.1 3.2 8.8 9.9 7.7
//Now, let's check the array:
//The array you input is :
//1     2     3     4     5
//3     2     1     8     7
//2.3     1   5.6   4.8   3.6
//The average of every column is：
//The 0 column's average is 3.
//The array's data average is 3.55333
//The max datam in the array is 8The 1 column's average is 4.2.
//The array's data average is 3.55333
//The max datam in the array is 8The 2 column's average is 3.46.
//The array's data average is 3.55333
//The max datam in the array is 8
//
//output3:
//Please enter the array number :
//	Please enter five double number seprate by enter :
//1.0 2.0 3.0 4.0 5.0
//3.0 2.0 4.0 6.0 7.0
//Please enter five double number seprate by enter :
//3.0 4.0 5.0 6.0 7.2
//Please enter five double number seprate by enter :
//8.0 9.0 4.5 3.6 9.9
//Now, let's check the array:
//The array you input is :
//1     2     3     4     5
//3     2     4     6     7
//3     4     5     6   7.2
//The average of every column is：
//The 0 column's average is 3.
//The 1 column's average is 4.4.
//The 2 column's average is 5.04.
//The array's data average is 4.14667
//The max datum in the array is 7.2

//output4:
//Please enter the array number :
//	Please enter five double number seprate by enter :
//1.1 2.2 3.3 4.4 5.5
//Please enter five double number seprate by enter :
//2.2 3.3 4.4 6.6 7.7
//Please enter five double number seprate by enter :
//8.9 8.8 7.8 9.9 7.6
//Now, let's check the array:
//The array you input is :
//1.1   2.2   3.3   4.4   5.5
//2.2   3.3   4.4   6.6   7.7
//8.9   8.8   7.8   9.9   7.6
//The average of every column is：
//The 0 column's average is 3.3.
//The 1 column's average is 4.84.
//The 2 column's average is 8.6.
//The array's data average is 5.58
//The max datum in the array is 9.9
