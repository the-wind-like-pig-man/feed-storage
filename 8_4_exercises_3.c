//exercises_3.c---输入字符流，统计大小写字符的个数

#include <stdio.h>

#include <ctype.h>//islower()和isupper()函数的头文件

int main()
{

	int lowercase = 0;

	int uppercase = 0;

	char ch = 0;

	//while ((ch = getchar()) != EOF)
	
	while ((ch = getchar()) != '\n')

	{

		if (ch >= 'A' && ch <= 'Z')
		{

			uppercase++;

		}

		if (ch >= 'a' && ch <= 'z')
		{

			lowercase++;

		}
		//使用ASCII码表中连续字符的特性进行判断
		//if(islower(ch)) lowercase++;
		//if(isupper(ch)) uppercase++;
		//也可以使用ctype.h库中字符判断函数进行判断，需要添加头文件
		//
	}

	printf("There are %d uppercase, and %d lowercase in that file!\n", uppercase,
		lowercase);


	return 0;

}

//output:
//while ((ch = getchar()) != EOF)
//There are 3 uppercase, and 14 lowercase in that file!

//output:
//I come from China, I am a Chinese people.I love my country!
//There are 5 uppercase, and 40 lowercase in that file!
