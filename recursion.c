﻿
#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

#include <string.h>

#include <math.h>

//int Fun(int n)
//{
//
//	if (n == 5)
//
//		return 2;
//
//	else
//
//		return 2 * Fun(n + 1);//2*8<---2*4<---2*2递归
//
//}
//
//int main()
//{
//
//	int ret = Fun(2);
//
//	printf("%d\n", ret);
//
//	return 0;
//
//}


//逆序字符串（递归实现）

//int main()
//{
//	char arr[] = "abcedefg";//[a b c d e f g \0]
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	int left = 0;
//
//	//int right = sz - 2;//下标以0开始，且以\0结尾，故减2
//	
//	int right = strlen(arr) - 1;
//
//	while (left < right)//首尾两两交换位置
//	{
//
//		char tmp = arr[left];
//
//		arr[left] = arr[right];
//
//		arr[right] = tmp;
//
//		left++;
//
//		right--;
//	
//	}
//
//	//printf("%d\n", arr);//类型不对，打印未知数
//
//	printf("%s\n", arr);
//
//
//	return 0;
//
//}


//void reverse(char arr[])
//{
//
//
//	int left = 0;
//	
//		//int right = sz - 2;//下标以0开始，且以\0结尾，故减2
//		
//		int right = strlen(arr) - 1;
//	
//		while (left < right)//首尾两两交换位置
//		{
//	
//			char tmp = arr[left];
//	
//			arr[left] = arr[right];
//	
//			arr[right] = tmp;
//	
//			left++;
//	
//			right--;
//		
//		}
//
//
//}

//int my_strlen(char* str)//自定义函数求字符串长度
//{
//
//	int count = 0;
//
//	while (*str != '\0')
//	{
//
//		count++;
//
//		str++;
//
//	}
//
//	return count;
//
//}

//void reverse(char* str)
//{
//
//	char tmp = *str;//first
//
//	//int len = strlen(str);//调用库函数
//	int len = my_strlen(str);//不调用库函数
//
//
//	*str = *(str + len - 1);//sceond
//
//	*(str + len - 1) = '\0';//third
//
//	//strlen(str + 1);
//
//	//if (strlen(str + 1) >= 2);//多了分号，语句被结束
//	//if (strlen(str + 1) >= 2)
//		if (my_strlen(str + 1) >= 2)
//
//	     //reverse(str + 1);//fourth
//		 reverse(str+1 );//fourth
//
//	*(str + len - 1) = tmp;
//
//
//}

//int main()//用函数来实现
//{
//
//
//	char arr[] = "abcdefg";//[a b c d e f g \0]
//
//	reverse(arr);
//
//	printf("%s\n", arr);
//
//	return 0;
//
//
//}

//void reverse(char arr[], int left, int right)//函数多参时
//{
//
//	char tmp = arr[left];
//
//	arr[left] = arr[right];
//
//	arr[right] = tmp;
//
//	if (left < right)
//
//		reverse(arr, left + 1, right - 1);
//
//}
//
//int main()//用函数来实现
//{
//
//
//	char arr[] = "abcdefg";//[a b c d e f g \0]
//
//	int left = 0;
//
//	int right = my_strlen(arr) - 1;
//
//	reverse(arr, left, right);
//
//	//reverse(arr);
//
//	printf("%s\n", arr);
//
//	return 0;
//
//
//}
//
//int DigitSum(unsigned int n)//求一个无符号整数各个位数字之和，例如输入1729，结果为19
//{
//
//	if (n > 9)
//
//		return DigitSum(n / 10) + n % 10;
//
//	else
//
//		return n;
//
//}
//
//int main()
//{
//
//	unsigned int n = 0;
//
//	scanf("%u", &n);
//
//	int sum = DigitSum(n);
//
//	printf("%d", sum);
//
//	return 0;
//
//}


//编写一个函数实现n的k次方，用递归来实现

//Pow(n,k)--->n*Pow(n,k-1)
//k=0,Pow(n,k)=1
//k>0,Pow(n,k)=n*Pow(n,k-1)
//k<0,Pow(n,k)=1.0/Pow(n,-k)

//double Pow(int n, int k)
//{
//
//	if (k > 0)
//
//		return n * Pow(n, k - 1);
//
//	else if (k == 0)
//
//		return 1;
//
//	else
//
//		return 1.0 / Pow(n, -k);
//
//
//}
//int main()
//{
//
//	int n = 0;
//
//	int k = 0;
//
//	scanf("%d%d", &n, &k);
//
//	//int ret = Pow(n, k);//可以接收，但强制转换后精度丢失
//
//	double ret = Pow(n, k);
//
//
//	//printf("%d\n", ret);
//
//	printf("%lf\n", ret);
//
//
//	return 0;
////
//}


//int main()
//{
//	//逗号表达式，结果为逗号后面的值
//	int arr[] = { 1,2,(3,4),5 };//1 2 4 5
//
//	printf("%d\n", sizeof(arr));
//
//	return 0;
//
//}
//
//int main()
//{
//	int num = 0;
//
//	int arr[10] = { 0 };
//
//	printf("%d\n", sizeof(arr));//结果=40
//
//	printf("%d\n", sizeof(int [10]));//与上面的sizeof(arr)等价，结果=40
//
//	return 0;
//
//}

//sizeof--->它是一个操作符，用来计算变量（类型）所占空间的大小，不关注内存中存放的具体内容，但是是字节
//不关注内存中存放的具体内容，单位是字节
//strlen--->它是一个库函数，是专门求字符串长度的。只能针对字符串
//从参数给定的地址向后一直找\0，并统计\0之前出现字符的个数
//int main()
//{
//	char str[12] = "hello world";
//
//	char arr[12] = {0};
//
//	printf("%d %d\n", sizeof(str), strlen(str));//输出12 11
//
//
//	printf("%d %d\n", sizeof(arr), strlen(arr));//输出12 0,strlen无法求数组的大小
//
//
//	return 0;
//
//}

//将数组A中的内容和数组B中的内容进行交换，数组大小一样
//int main()
//{
//	int arr1[] = { 1,3,5,7,9 };
//
//	int arr2[] = { 2,4,6,8,0 };
//
//	int i = 0;
//
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	
//	//错误的示范
//	//int tmp[] = { 0 };
//
//	//tmp = arr1;
//
//	//arr1 = arr2;
//
//	//arr2 = tmp;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		int tmp = arr1[i];
//
//		arr1[i] = arr2[i];
//
//		arr2[i] = tmp;
//
//	}
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr1[i]);
//
//	}
//
//	printf("\n");
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr2[i]);
//
//	}
//
//	return 0;
//
//}

//创建一个数组，完成对数组的操作
//实现函数inint()---初始化数组全部为0
//实现print()函数---打印数组的每个元素
//实现reverse()函数---完成数组元素的逆置

//void reverse(int arr[], int sz)//逆序函数reverse()实现
//{
//
//	int left = 0;
//
//	int right = sz - 1;
//
//	while (left < right)
//	{
//
//		int tmp = arr[left];
//
//		arr[left] = arr[right];
//
//		arr[right] = tmp;
//
//		left++;
//
//		right--;
//
//	}
//
//}
//
//void init(int arr[], int sz)//将数组初始化为0函数inint()实现
//{
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		arr[i] = 0;
//
//	}
//
//}
//
//void print(int arr[], int sz)//print()函数实现
//{
//
//	int i = 0;
//
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//
//	}
//
//	printf("\n");
//
//}
//
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	print(arr, sz);//1 2 3 4 5 6 7 8 9 0
//
//	reverse(arr, sz);//将数组逆序
//
//	print(arr, sz);//0 9 8 7 6 5 4 3 2 1 
//
//	init(arr, sz);//将数组初始化为0
//
//	print(arr, sz);//0 0 0 0 0 0 0 0 0 0
//
//
//
//	return 0;
//
//}