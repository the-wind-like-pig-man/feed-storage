//flc.c---��Ȥ�ĳ���

#include <stdio.h>

#define COLS 4

int sum2d(const int ar[][COLS], int rows);

int sum(const int ar[], int n);

int main()
{

	int total11 = 0, total12 = 0, total13 = 0;

	int* pt1;

	int(*pt2)[COLS];

	pt1 = (int[2]) {10, 20};

	pt2 = (int[2][COLS]){ {1, 2, 3, -9},{4, 5, 6, -8} };

	total11 = sum(pt1, 2);

	total12 = sum2d(pt2, 2);

	total13 = sum((int[]) { 4, 4, 4, 5, 5, 5 }, 6);

	printf("total11 = %d\n", total11);

	printf("total12 = %d\n", total12);

	printf("total13 = %d\n", total13);


	return 0;

}

int sum(const int ar[], int n)
{

	int i = 0;

	int total = 0;

	for (i = 0; i < n; i++)
		{

			total += ar[i];

		}

	return total;


}

int sum2d(const int ar[][COLS], int rows)
{

	int r = 0;

	int c = 0;

	int tot = 0;

	for (r = 0; r < rows; r++)
	{

		for (c = 0; c < COLS; c++)
		{

			tot += ar[r][c];

		}

	}

	return tot;

}

//output:
//total11 = 30
//total12 = 4
//total13 = 27