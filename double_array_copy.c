//double_array_copy.c---初始化一个double数组，并用三种方式复制数组的值

#include <stdio.h>

void copy_arr(double t[], double s[], int n);

void copy_ptr(double* t, double* s, int n);

void copy_ptrs(double* t, double* s_first, double* s_last);
//依据题目要求的函数调用格式还原的函数声明
//void print_target1(target1, 5);

int main()
{

	int i = 0;

	double source[5] = { 1.1, 2.2, 3.3, 4.4, 5.5 };

	double target1[5];

	double target2[5];

	double target3[5];

	copy_arr(target1, source, 5);

	for (i = 0; i < 5; i++)
	{

		printf("target1[i] = %.1f ", target1[i]);

	}
	printf("\n");

	copy_ptr(target2, source, 5);

	printf("\n");

	copy_ptrs(target3, source, 5);


	return 0;

}

void copy_arr(double t[], double s[], int n)
{

	for (int i = 0; i < n; i++)
	{

		t[i] = s[i];
		//使用数组作为函数的参数，同时需要数组的下标值，只需要下标访问数组

		printf("%.1f ", t[i]);

	}

}

void copy_ptr(double* t, double* s, int n)
{

	for (int i = 0; i < n; i++)
	{

		*(t + i) = *(s + i);
		//使用指针作为函数的参数，需要标明指针的访问范围，不能越界
		//并使用*号形式进行数据的赋值

		printf("*(t + i) = %.1f ", *(t + i));

	}

}

void copy_ptrs(double* t, double* s_first, double* s_last)
{

	for (int i = 0; (s_last - s_first) > i; i++)
	{
		//for(int i = 0; (s_last _ s_first) >0; i++, s_first++)
		//也可以移动首指针，当尾指针相等时即停止循环，只是该方法的赋值语句 
		//略有不同

		*(t + i) = *(s_first + i);
		//使用指针作为函数的参数，也可以通过首、尾两个指针来表示
		//指针允许访问的数据地址范围

		printf("%.1f ", *(t + i));

	}

	//printf("*(t + i) = %.1f ", *(t + i));


}

//ouput:
//t[i] = 1.1 t[i] = 2.2 t[i] = 3.3 t[i] = 4.4 t[i] = 5.5
//* (t + i) = 1.1 * (t + i) = 2.2 * (t + i) = 3.3 * (t + i) = 4.4 * (t + i) = 5.5

//output:
//1.1 2.2 3.3 4.4 5.5 target1[i] = 1.1 target1[i] = 2.2 target1[i] = 3.3 target1[i] = 4.4 target1[i] = 5.5
//* (t + i) = 1.1 * (t + i) = 2.2 * (t + i) = 3.3 * (t + i) = 4.4 * (t + i) = 5.5