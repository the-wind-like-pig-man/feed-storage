﻿
#include <stdio.h>


#include "mineclearance_game.h"

void InitBoard(char board[ROWS][COLS], int rows, int cols,char set)
{

	int i = 0;

	int j = 0;

	for (i = 0; i < rows; i++)
	{
		
		for (j = 0; j < cols; j++)
		{

			board[i][j] = set;

		}
	}


}

void DisplayBoard(char board[ROWS][COLS], int row, int col)//
{

	int i = 0;

	int j = 0;

	//printf("----------------mineclearance---------------");//没换行符，输出很惊喜

	printf("-----mineclearance-----\n");

	//for (j = 1; j <= col; j++)//j=1的时候，列号对不齐

	for (j = 0; j <= col; j++)

	{

		//printf("%d", j);//没有空格，会很不理想

		//printf(" %d", j);//前面有一个空格，会比较理想

		printf("%d ", j);//前面有一个空格，会比较理想


		//printf("%d  ", j);//有两个空格，会超出理想


	}

	printf("\n");

	for (i = 1; i <= row; i++)
	{
		
		//printf("%d", i);//没有空格，会很不理想

		printf("%d ", i);//有空格，会很理想


		for (j = 1; j <= col; j++)
		{

			printf("%c ", board[i][j]);

		}

		printf("\n");

	}

	//printf("----------------mineclearance---------------");//没有换行符，输出很惊喜
	//printf("----------------mineclearance---------------");//字符过长时，输出很惊喜

	printf("-----mineclearance-----\n");//字符数量适中，输出正常


}


void SetMine(char board[ROWS][COLS], int row, int col)
{

	int count = EASY_COUNT;

	//横坐标1~9
	//纵坐标1~9

	while (count)//生成随机坐标
	{

		int x = rand() % row + 1;

		int y = rand() % col + 1;

		if (board[x][y] == '0')
		{

			board[x][y] = '1';

			count--;

		}

	}


}
