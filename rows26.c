//rows26.c---打印26个英文字母

#include <stdio.h>

#define ROWS 26

//int main()
//{
//
//	int row = 0;
//
//	int i = 0;
//
//	char arry[26] = { 0 };
//
//	char c = 'a';
//
//	for (row = 0; row < ROWS; row++, c++)
//	{
//		
//		arry[i] = c;
//
//		for (arry[0] = 'a'; arry[i] < 'z'; i++)
//		{
//
//			printf("%c ", arry[i]);
//
//			arry[i] = 'a' + 1;
//
//		}
//
//		printf("\n");
//
//	}
//
//	return 0;
//
//}


int main()
{

	int i = 0;

	int j = 0;

	char arry[26] = { 0 };

	char c = 'a';//此句放到循环外部无法实现

	for (j = 0; j < ROWS; j++)
	{

		char c = 'a';

		for (i = 0; i < ROWS; i++, c++)
		{

			arry[i] = c;

		}

		for (i = 0; i < ROWS; i++)
		{

			printf("%c ", arry[i]);

		}

		printf("\n");

	}
		
	

	return 0;

}


//output:
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z
//a b c d e f g h i j k l m n o p q r s t u v w x y z