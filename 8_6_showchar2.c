//showchar2.c---按指定的行列打印字符

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

void display(char cr, int lines, int width);

int main()
{

	int ch = 0;//待打印字符

	int rows = 0, cols = 0;//待打印行和列

	printf("Please enter a character and two integers: \n");

	while ((ch = getchar()) != '\n')
	{

		if (scanf("%d %d", &rows, &cols) != 2)
		{

			break;

		}

		display(ch, rows, cols);

		while (getchar() != '\n')
		{

			continue;

		}

		printf("Please enter another character and two integers;\n");

		printf("Please enter a newline to quit.\n");

	}

	printf("Bye!\n");



	return 0;

}

void display(char cr, int lines, int width)
{

	int row = 0, col = 0;

	//for (row = 1; row <= lines; col++)

		for (row = 1; row <= lines; row++)

	{

		for (col = 1; col <= width; col++)
		{

			putchar(cr);


		}

		putchar('\n');//结束一行并开始新的一行

	}

}
//
//output:
//Please enter a character and two integers :
//c 1 2
//cc
//Please enter another character and two integers;
//Please enter a newline to quit.
//!3 6
//!!!!!!
//!!!!!!
//!!!!!!

//
//output:
//Please enter a character and two integers :
//@ 3 7
//@@@@@@@
//@@@@@@@
//@@@@@@@