﻿

#include <stdio.h>

//int main()//打印1到10
//{
//	int i = 1;//先初始化
//
//	while (i <= 10)//进而进行判断
//	{
//		if (i == 5)
//
//			continue;//到i=5后进入死循环
//
//		printf("%d ", i);
//
//		i++;//最后进行调整
//
//	}
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//
//	for (i = 1; i <= 10; i++)//for循环用的最普遍，因其将初始化、判断、调整放在一起，便于控制
//	{
//		if (i == 5)
//
//			//break;//退出
//
//			continue;//把5跳过去，跳到调整部分去
//
//		printf("%d ", i);
//
//		
//		//printf("hello world!\n");
//
//	}
//
//	return 0;
//}

int main()
{
	//int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	////下标是0~9

	//int i = 0;

	//for (i = 0; i < 10; i++)
	//	//for (i = 0; i <= 9; i++)//也可以打印1~10，但不建议用此法

	//{
	//	printf("%d ", arr[i]);

	//}

	//int i = 0;

	//for (i = 100; i <= 200; i++)//打印100~200
	//{
	//	printf("%d ", i);

	//}

	//for循环的判断部分省略，意味着判断会恒成立
	//for ( ; ; )
	//{
	//	printf("hello world!\n");
	//}

	//int i = 0;

	//int j = 0;

	////for (i = 0; i < 3; i++)
	//	for (; i < 3; i++) // 不要随意省略初始化

	//{
	//	//for (j = 0; j < 3; j++)
	//		for (; j < 3; j++)//不要随意省略初始化,否则只打印3次


	//	{
	//		printf("hello world!\n");

	//	}
	//}

	/*int x, y;
	
	for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)

	{
		printf("hello world!\n");

	}*/

	//int i = 0;

	//int k = 0;

	//for (i = 0, k = 0; k = 0; i++, k++)//k=0条件为假，不循环

	//	k++;

	int i = 1;

	do
	{
		i++;


		if (i == 5)

			continue;

			//break;//打印2，3，4

		printf("%d ", i);

		//i++;//打印1，2，3，4死循环
	}

	while (i <= 10);


	return 0;
}