//exercises_8.c---求模运算

#define _CRT_SECURE_NO_WARNINGS 0

#include <stdio.h>

int main(int argc, char* argv[])
{

	int first = 0, second = 0;

	printf("This program computes moguli.\n");

	printf("Please enter an integer to server as the second operand: ");

	scanf("%d", &second);//用户输入的second数据保持不变

	printf("Now enter the first operand: ");

	scanf("%d", &first);

	while (first > 0)
	{

		printf("%d %% %d is %d\n", first, second, (first % second));

		printf("Please enter next number for first operand (<= 0 to quit): ");

		scanf("%d", &first);

	}

	//循环读取用户的输入，计算并打印结果

	printf("Done!\n");



	return 0;

}

//output:
//This program computes moguli.
//Please enter an integer to server as the second operand : 256
//Now enter the first operand : 438
//438 % 256 is 182
//Please enter next number for first operand(<= 0 to quit) : 1234567
//1234567 % 256 is 135
//Please enter next number for first operand(<= 0 to quit) : 256
//256 % 256 is 0
//Please enter next number for first operand(<= 0 to quit) : 0
//Done!
